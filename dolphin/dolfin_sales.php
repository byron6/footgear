<?php

$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => 'https://ftgsemuatlab.mychain.co.za:8888/DolfinInterface.Webservices.FTG/EnquiryInterface.svc',
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => '',
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => 'POST',
  CURLOPT_POSTFIELDS =>'<?xml version="1.0" encoding="UTF-8"?>
<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://www.w3.org/2003/05/soap-envelope" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <SOAP-ENV:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">
    <wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">
      <wsse:UsernameToken>
        <wsse:Username>SEMANTICA</wsse:Username>
        <wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">k4tVwN7R34s8XMmx</wsse:Password>
      </wsse:UsernameToken>
    </wsse:Security>
    <wsa:Action>http://tempuri.org/IEnquiryInterface/SalesEnquiry</wsa:Action>
    <wsa:To>https://ftgsemuatlab.mychain.co.za:8888/DolfinInterface.Webservices.FTG/EnquiryInterface.svc</wsa:To>
  </SOAP-ENV:Header>
  <SOAP-ENV:Body>
    <SalesEnquiry xmlns="http://tempuri.org/">
    	 <CompanyID xmlns="http://tempuri.org/">FTG</CompanyID>
    	 <StoreNumber xmlns="http://tempuri.org/">9997</StoreNumber>
    <TerminalNumber xmlns="http://tempuri.org/">991</TerminalNumber>
        <TransactionNumber xmlns="http://tempuri.org/">20011</TransactionNumber>
    </SalesEnquiry>
  
  </SOAP-ENV:Body>
</SOAP-ENV:Envelope>
',
  CURLOPT_HTTPHEADER => array(
    'Content-Type: application/soap+xml; charset=utf-8'
  ),
));

$response = curl_exec($curl); // Set the response as $response //
file_put_contents('dolfin_sales.xml', $response); // Save the response in a xml document //

$xml = file_get_contents("dolfin_sales.xml");
$xml = str_replace('<s:', '<', $xml);
$xml = str_replace('</s:', '</', $xml);
$xml = str_replace('<b:', '<', $xml);
$xml = str_replace('</b:', '</', $xml);
file_put_contents("dolfin_sales.xml", $xml);

curl_close($curl);
echo $response;