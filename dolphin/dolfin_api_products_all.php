<?php

$curl = curl_init();

curl_setopt_array($curl, array(
    CURLOPT_URL => 'https://ftgsem.mychain.co.za/Argility.DolfinInterface.Webservices.FTG/EnquiryInterface.svc',
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => '',
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => 'POST',
    CURLOPT_POSTFIELDS => '<?xml version="1.0" encoding="UTF-8"?>
<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://www.w3.org/2003/05/soap-envelope" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <SOAP-ENV:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">
    <wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">
      <wsse:UsernameToken>
        <wsse:Username>SEMANTICA</wsse:Username>
        <wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">PuU#x2V8#6$9weDB</wsse:Password>
      </wsse:UsernameToken>
    </wsse:Security>
    <wsa:Action>http://tempuri.org/IEnquiryInterface/ProcessXML</wsa:Action>
    <wsa:To>https://ftgsem.mychain.co.za/Argility.DolfinInterface.Webservices.FTG/EnquiryInterface.svc</wsa:To>
  </SOAP-ENV:Header>
  <SOAP-ENV:Body>
    <ProcessXML xmlns="http://tempuri.org/">
    		<query xmlns="http://tempuri.org/">
    			<Process xmlns="">
                    <TranHdr>
                        <CompanyID>FTG</CompanyID>
                        <StoreNumber>9990</StoreNumber>
                        <UserName>Sagie</UserName>
                        <TranDate>2021-08-12</TranDate>
                        <TranTime>12:15:24</TranTime>
                        <TranGUID>229EDCC7-9DDC-4CF6-A0AC-E977212E010F</TranGUID>
                    </TranHdr>
                    <DataRequest>
                        <RequestType>S2S_Web_Products_v4</RequestType>
                        <RequestDetails>
                        <ProductList></ProductList>
                        <StoreList>9990</StoreList>
                        <Rows>10000000</Rows> 
                        <WebEnabled>1</WebEnabled> 
                        </RequestDetails>
                    </DataRequest>                    
                </Process>
		    </query>
    </ProcessXML>
  </SOAP-ENV:Body>
</SOAP-ENV:Envelope>
',
    CURLOPT_HTTPHEADER => array(
        'Content-Type: application/soap+xml; charset=utf-8'
    ),
));

$response = curl_exec($curl); // Set the response as $response //
file_put_contents('dolfin_product_all.xml', $response); // Save the response in a xml document //

$xml = file_get_contents("dolfin_product_all.xml");
$xml = str_replace('<s:', '<', $xml);
$xml = str_replace('</s:', '</', $xml);
$xml = str_replace('<b:', '<', $xml);
$xml = str_replace('</b:', '</', $xml);
$xml = str_replace('Mens Footwear', 'Men', $xml);
$xml = str_replace('Mens Apparel', 'Men', $xml);
$xml = str_replace('Ladies Footwear', 'Women', $xml);
$xml = str_replace('Ladies Apparel', 'Women', $xml);
$xml = str_replace('Juniors Footwear', 'Kids', $xml);
$xml = str_replace('Infants Footwear', 'Kids', $xml);
$xml = str_replace('Childrens Footwear', 'Kids', $xml);
$xml = str_replace('<PromoPrice>0.0000</PromoPrice>', '<PromoPrice/>', $xml);
$xml = str_replace('<Size>', '<Size>UK ', $xml);
$xml = str_replace('<SP4>0.0000</SP4>', '<SP4/>', $xml);
$xml = str_replace('<SP4>0.00000.0000</SP4>', '<SP4/>', $xml);
$xml = str_replace('<SP3>0.0000</SP3>', '<SP3/>', $xml);
$xml = str_replace('<SP3>0.00000.0000</SP3>', '<SP3/>', $xml);
$xml = str_replace('<SP3>', '<Combo_Cat>combo</Combo_Cat><SP3>', $xml);
$xml = str_replace('<PromoPrice>', '<Promo_Cat>274</Promo_Cat><PromoPrice>', $xml);
$xml = str_replace('<PromoPrice/>', '<Promo_Cat></Promo_Cat><PromoPrice/>', $xml);
$xml = str_replace('00</SP>', '</SP>', $xml);
$xml = str_replace('00</SP3>', '</SP3>', $xml);
file_put_contents("dolfin_product_all.xml", $xml);



/*
echo "Success";



$curl = curl_init();

curl_setopt_array($curl, array(
    CURLOPT_URL => 'https://ftgsemuatlab.mychain.co.za:8888/DolfinInterface.Webservices.FTG/EnquiryInterface.svc',
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => '',
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => 'POST',
    CURLOPT_POSTFIELDS => '<?xml version="1.0" encoding="UTF-8"?>
<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://www.w3.org/2003/05/soap-envelope" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <SOAP-ENV:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">
    <wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">
      <wsse:UsernameToken>
        <wsse:Username>SEMANTICA</wsse:Username>
        <wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">k4tVwN7R34s8XMmx</wsse:Password>
      </wsse:UsernameToken>
    </wsse:Security>
    <wsa:Action>http://tempuri.org/IEnquiryInterface/ProcessXML</wsa:Action>
    <wsa:To>https://ftgsemuatlab.mychain.co.za:8888/DolfinInterface.Webservices.FTG/EnquiryInterface.svc</wsa:To>
  </SOAP-ENV:Header>
  <SOAP-ENV:Body>
    <ProcessXML xmlns="http://tempuri.org/">
    		<query xmlns="http://tempuri.org/">
    			<Process xmlns="">
                    <TranHdr>
                        <CompanyID>FTG</CompanyID>
                        <StoreNumber>9990</StoreNumber>
                        <UserName>Sagie</UserName>
                        <TranDate>2021-08-12</TranDate>
                        <TranTime>12:15:24</TranTime>
                        <TranGUID>229EDCC7-9DDC-4CF6-A0AC-E977212E010F</TranGUID>
                    </TranHdr>
                    <DataRequest>
                        <RequestType>S2S_Web_Products_v4</RequestType>
                        <RequestDetails>
                        <ProductList></ProductList>
                        <StoreList>9990</StoreList>
                        <Rows>10000000</Rows> 
                        <WebEnabled>1</WebEnabled> 
                        </RequestDetails>
                    </DataRequest>                    
                </Process>
		    </query>
    </ProcessXML>
  </SOAP-ENV:Body>
</SOAP-ENV:Envelope>
',
    CURLOPT_HTTPHEADER => array(
        'Content-Type: application/soap+xml; charset=utf-8'
    ),
));

$response = curl_exec($curl); // Set the response as $response //
file_put_contents('dolfin_product_all.xml', $response); // Save the response in a xml document //

$xml = file_get_contents("dolfin_product_all.xml");
$xml = str_replace('<s:', '<', $xml);
$xml = str_replace('</s:', '</', $xml);
$xml = str_replace('<b:', '<', $xml);
$xml = str_replace('</b:', '</', $xml);
$xml = str_replace('Mens Footwear', 'Men', $xml);
$xml = str_replace('Mens Apparel', 'Men', $xml);
$xml = str_replace('Ladies Footwear', 'Women', $xml);
$xml = str_replace('Ladies Apparel', 'Women', $xml);
$xml = str_replace('Juniors Footwear', 'Kids', $xml);
$xml = str_replace('Infants Footwear', 'Kids', $xml);
$xml = str_replace('Childrens Footwear', 'Kids', $xml);
$xml = str_replace('<PromoPrice>0.0000</PromoPrice>', '<PromoPrice/>', $xml);
$xml = str_replace('<Size>', '<Size>UK ', $xml);
$xml = str_replace('<SP4>0.0000</SP4>', '<SP4/>', $xml);
$xml = str_replace('<SP4>0.00000.0000</SP4>', '<SP4/>', $xml);
$xml = str_replace('<SP3>0.0000</SP3>', '<SP3/>', $xml);
$xml = str_replace('<SP3>0.00000.0000</SP3>', '<SP3/>', $xml);
$xml = str_replace('<SP3>', '<Combo_Cat>combo</Combo_Cat><SP3>', $xml);
$xml = str_replace('<PromoPrice>', '<Promo_Cat>274</Promo_Cat><PromoPrice>', $xml);
$xml = str_replace('<PromoPrice/>', '<Promo_Cat></Promo_Cat><PromoPrice/>', $xml);

file_put_contents("dolfin_product_all.xml", $xml);


/* End SKU's */ /* WORKING */
echo "Success";
