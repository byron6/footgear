<?php

/**
 * @link			  https://www.semantica.co.za/
 * @since			  1.0.0
 * @package			  Semantica Core
 *
 * @wordpress-plugin
 * Plugin Name:		  Dolphin Refunds API
 * Plugin URI:		  https://www.semantica.co.za/
 * Description:		  Semantica Core Integrations
 * Version:			  1.0.0
 * Author:			  Semantica
 * Author URI:		  https://www.semantica.co.za/
 * License:			  GPL-2.0+
 * License URI:		  http://www.gnu.org/licenses/gpl-2.0.txt
 */

if (!defined('ABSPATH')) exit; // Exit if accessed directly
add_action('woocommerce_order_status_refunded', 'woocommerce_order_status_refunded', 999, 1);
function woocommerce_order_status_refunded($order_id)
{
    $order = wc_get_order($order_id);
    $date_created = $order->get_date_created();
    $order_date_created_date = $date_created->date("Y-m-d");
    $time_created = $order->get_date_created();
    $order_time_created_date = $time_created->date("H:i:s");
    $order_id = $order->get_id();
    $order_number = $order->get_order_number();
    $order_billing_first_name = $order->get_billing_first_name();
    $order_billing_last_name = $order->get_billing_last_name();
    $order_billing_email = $order->get_billing_email();
    $order_billing_phone = $order->get_billing_phone();
    $order_shipping_first_name = $order->get_shipping_first_name();
    $order_shipping_last_name = $order->get_shipping_last_name();
    $address1 = $order->get_shipping_address_1();
    $address2 = $order->get_shipping_address_2();
    $address_city = $order->get_shipping_city();
    $address_postcode = $order->get_shipping_postcode();
    $address_country = $order->get_shipping_country();
    $order_quanity = $order->get_item_count();
    $order_not_rounded = $order->get_total();
    $order_shipping = $order->get_shipping_total();
    $order_final = $order_total - $order_shipping;
    $product_details = array();
	$order_total = round($order_not_rounded, 2); 
	$item_total_refunded = $order->get_total_refunded_for_item( $item_id );
	$time2 = time();
	$time = substr($time2, -3);
	$order_number_timestamp = $order_number . "" . $time;
    $num = 0;
    $i = 1;
    $j = 1;
    foreach ($order->get_items() as $item_id => $item) {
        $product = $item->get_product();
        $sku = $product->get_sku();
        $quantity = $item->get_quantity();
        $total = $item->get_total(); // Total without tax (non discounted)
        $total_tax = $item->get_subtotal_tax();
        $item_total_not_rounded = $total + $total_tax;
        $item_total = round($item_total_not_rounded, 2); 
        $delivery_date = date('Y-m-d', strtotime('+3 days'));
        
        $product_details[] = "
            <SaleLineData>
                <LineNumber>" . $order_number . "0" . $i++ . "</LineNumber>
                <SKUCode>" . $sku . "</SKUCode>
                <SaleQty>" . $quantity . "</SaleQty>
                <SaleLineDirection>ReturnOfSale</SaleLineDirection>
                <TransactionSubType>NLI</TransactionSubType>
                <NetAmtStoreInc>" . $item_total . "</NetAmtStoreInc>
                <TaxAmtStore>" . $total_tax . "</TaxAmtStore>
                <DiscAmtStoreInc>0.0000</DiscAmtStoreInc>
                <DiscountType>NA</DiscountType>
                <DiscountReasonCode />
                <ConditionalPromotionID>0</ConditionalPromotionID>
                <UnitPriceStoreInc>" . $item_total . "</UnitPriceStoreInc>
                <OverridePrice>0</OverridePrice>
                <ExchangeInd>0</ExchangeInd>
        		<CSType>NA</CSType>
        		<OrigStoreNumber>9990</OrigStoreNumber>
        		<OrigTerminal>991</OrigTerminal>
        		<OrigTransactionNumber>" . $order_number . "</OrigTransactionNumber>
        		<OrigTransactionSeqNumber>" . $order_number . "0" . $j++ . "</OrigTransactionSeqNumber>
        		<AdjustmentValue>0</AdjustmentValue>
        		<AdjustmentType>Cm</AdjustmentType>
        		<ReferenceLineNumber>0</ReferenceLineNumber>
        		<ReferenceLineType>NA</ReferenceLineType>
       			<VoidReferenceLine>0</VoidReferenceLine>
        		<JavaReferenceLineNumber>0</JavaReferenceLineNumber>
        		<AltReferenceLineNumber>0</AltReferenceLineNumber>
        		<HireRefundReason>0</HireRefundReason>
        		<ForeignCurrencyPrice>0</ForeignCurrencyPrice>
        		<NoOfDaysReturnPolicy>0</NoOfDaysReturnPolicy>
        		<TransactionDiscountAmtStoreInc>0</TransactionDiscountAmtStoreInc>
        		<LineDiscountAmtStoreInc>0</LineDiscountAmtStoreInc>
        		<OffLineInd>0</OffLineInd>
        		<SecurityDepositAmount>0</SecurityDepositAmount>
        		<CollectedHireInd>0</CollectedHireInd>
                    <DeliveryDetails>
                    <DeliveryType>Courier</DeliveryType>
                    <DeliveryStoreNumber />
                    <ExpectedDeliveryDate>" . $delivery_date . "</ExpectedDeliveryDate>
                    <IsPreOrderInd>0</IsPreOrderInd>
                </DeliveryDetails>
                <GiftVoucher />
            </SaleLineData>";
    }
    $product_list = implode('', $product_details);
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://ftgsem.mychain.co.za/Argility.DolfinInterface.Webservices.FTG/DolfinMessagingInterface.svc',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => '<?xml version="1.0" encoding="UTF-8"?>
            <SOAP-ENV:Envelope xmlns:tem="http://tempuri.org/" xmlns:SOAP-ENV="http://www.w3.org/2003/05/soap-envelope" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
            <SOAP-ENV:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">
                <wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">
                    <wsse:UsernameToken>
                    <wsse:Username>SEMANTICA</wsse:Username>
                    <wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">PuU#x2V8#6$9weDB</wsse:Password>
                    </wsse:UsernameToken>
                </wsse:Security>
                <wsa:Action>http://tempuri.org/IDolfinMessagingInterface/ActionDolfinTransaction</wsa:Action>
                <wsa:To>https://ftgsem.mychain.co.za/Argility.DolfinInterface.Webservices.FTG/DolfinMessagingInterface.svc</wsa:To>
            </SOAP-ENV:Header>
            <SOAP-ENV:Body>
                <tem:ActionDolfinTransaction>
                <tem:companyId>FTG</tem:companyId>
                <tem:source>DolfinTranSourceWS</tem:source>
                <tem:xmlDoc>
                    <![CDATA[<CashSaleReturn>
                        <Hdr>
                            <TranHdr>
                                <StoreNumber>9990</StoreNumber>
                                <Terminal>992</Terminal>
                                <TransactionNumber>' . $order_number . "" . $time . '</TransactionNumber>
                                <UserName>mychain\ftg9997a</UserName>
                                <OnlineAuthorisationUserName />
                                <TranDate>' . $order_date_created_date . '</TranDate>
                                <TranTime>' . $order_date_created_time . '</TranTime>
                            </TranHdr>
                            <TransactionType>CSR</TransactionType>
                            <CashierID>Web Sales</CashierID>
                          	<EmployeeNumber />
							<LoyaltyCustomerNo />
							<RefundNote>01</RefundNote>
							<PostalCode />
							<SchoolClub />
							<MySchool />
							<DiscoveryMemNum />
							<DiscAuthCode />
							<ReasonName />
							<ReasonTel />
							<ReasonInfoID />
							<ReasonInfoVoucherNum />
							<ReasonInfoAdd1 />
							<ReasonInfoAdd2 />
							<ReasonInfoAdd3 />
							<ReasonInfoAdd4 />
							<ReasonInfoAdd5 />
							<OverrideUser></OverrideUser>
							<DiscoveryHealthNumber />
							<DiscoveryInputType>0</DiscoveryInputType>
							<RefundSurname />
							<RefundContactNumber />
							<RefundEmailAddress />
							<DemographicHdr>
							<Sex />
							<Age />
							<Race />
							</DemographicHdr>
							<CSCustomerCellPhone>' . $order_billing_phone . '</CSCustomerCellPhone>
							<CSCustomerEmail>' . $order_billing_email . '</CSCustomerEmail>
							<CSCustomerName>' . $order_billing_first_name . '</CSCustomerName>
							<CSCustomerSurname>' . $order_billing_last_name . '</CSCustomerSurname>
							<CSCustomerAccountNumber></CSCustomerAccountNumber>
							<GiftReceiptNumber></GiftReceiptNumber>
							</Hdr>
							<ACSA>
							<SaleACSAData>
							<SaleACSASex />
							<SaleACSAAirline />
							<SaleACSADestination />
							<SaleACSAFlight />
							<SaleACSAClass />
							<SaleACSASeat />
							</SaleACSAData>
							</ACSA>
                        <Lines>' . $product_list . '
                        </Lines>
                        <Tenders>
                            <TenderData>
                                <TenderTypeNo>DirectDeposit</TenderTypeNo>
                                <TenderSubType>DDEP</TenderSubType>
                                <Reference />
                                <TenderDirection>Tender</TenderDirection>
                                <TenderCurrency>RANDS</TenderCurrency>
                                <TenderAmountCurr>' . $order_total . '</TenderAmountCurr>
                                <TenderAmountStore>' . $order_total . '</TenderAmountStore>
                            </TenderData>
                        </Tenders>
                        <Totals>
                            <TranDetailTotal>
                                <LinesCount>' . $order_quanity . '</LinesCount>
                                <LinesQty>' . $order_quanity . '</LinesQty>
                                <LinesValueInc>' . $order_total . '</LinesValueInc>
                            </TranDetailTotal>
                        </Totals>
                    </CashSaleReturn>
                ]]> </tem:xmlDoc>
                </tem:ActionDolfinTransaction>
            </SOAP-ENV:Body>
            </SOAP-ENV:Envelope>
		',
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/soap+xml; charset=utf-8'
        ),
    ));
    $response = curl_exec($curl);
    update_post_meta($order_id, 'dolphin_api_refund_response', $response);
    update_post_meta($order_id, 'dolphin_refund_order_id', $order_number_timestamp);
    update_post_meta($order_id, 'dolphin_order_time_created_date', $order_time_created_date);
    update_post_meta($order_id, 'dolphin_order_date_created_date', $order_date_created_date);
    update_post_meta($order_id, 'dolphin_order_billing_first_name', $order_billing_first_name);
    update_post_meta($order_id, 'dolphin_order_billing_last_name', $order_billing_last_name);
    update_post_meta($order_id, 'dolphin_order_billing_email', $order_billing_email);
    update_post_meta($order_id, 'dolphin_order_billing_phone', $order_billing_phone);
    update_post_meta($order_id, 'dolphin_order_quanity', $order_quanity);
    update_post_meta($order_id, 'dolphin_order_total', $item_total_refunded);
    update_post_meta($order_id, 'dolphin_order_total2', $order_total);
    update_post_meta($order_id, 'dolphin_order_total3', $order_not_rounded);
    update_post_meta($order_id, 'dolphin_refund_salelinedata', $product_list);
    curl_close($curl);
}