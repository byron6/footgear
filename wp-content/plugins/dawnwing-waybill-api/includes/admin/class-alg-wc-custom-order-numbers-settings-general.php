<?php
/**
 * Dawnwing Custom Order Numbers for WooCommerce - General Section Settings
 *
 * @version 1.2.3
 * @since   1.0.0
 * @author  Tyche Softwares
 * @package Custom-Order-Numbers-Lite
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

if ( ! class_exists( 'Alg_WC_Custom_Order_Numbers_Settings_General' ) ) :

	/**
	 * General Settings.
	 */
	class Alg_WC_Custom_Order_Numbers_Settings_General extends Alg_WC_Custom_Order_Numbers_Settings_Section {

		/**
		 * Constructor.
		 *
		 * @version 1.0.0
		 * @since   1.0.
		 */
		public function __construct() {
			$this->id   = '';
			$this->desc = __( 'General', 'custom-order-numbers-for-woocommerce' );
			parent::__construct();
			add_action( 'admin_head', array( $this, 'add_tool_button_class_style' ) );
		}

		/**
		 * Add_tool_button_class_style.
		 *
		 * @version 1.0.0
		 * @since   1.0.0
		 */
		public function add_tool_button_class_style() {
			echo '<style type="text/css">';
			echo '#alg-tool-button { ';
			echo 'background: #ba0000; border-color: #aa0000; text-shadow: 0 -1px 1px #990000,1px 0 1px #990000,0 1px 1px #990000,-1px 0 1px #990000; box-shadow: 0 1px 0 #990000;';
			echo ' }';
			echo '</style>';
		}

		/**
		 * Add_settings.
		 *
		 * @param array $settings - Settings to be displayed.
		 *
		 * @version 1.2.3
		 * @since   1.0.0
		 * @todo    [dev] add `alg_wc_custom_order_numbers_counter_previous_order_date` as `hidden` field (for proper settings reset)
		 */
		public function add_settings( $settings ) {

			global $wp_roles;
			$user_roles          = array();
			$user_roles_no_admin = array();
			foreach ( apply_filters( 'editable_roles', ( isset( $wp_roles->roles ) ? $wp_roles->roles : array() ) ) as $role_key => $role ) {
				$user_roles[ $role_key ] = $role['name'];
				if ( ! in_array( $role_key, array( 'administrator', 'super_admin' ), true ) ) {
					$user_roles_no_admin[ $role_key ] = $role['name'];
				}
			}

			$settings = array_merge(
				array(
					array(
						'title' => __( 'Dawnwing Custom Order Numbers Options', 'custom-order-numbers-for-woocommerce' ),
						'type'  => 'title',
						'desc'  => __( 'Enable sequential order numbering, set custom number prefix and number width.', 'custom-order-numbers-for-woocommerce' ),
						'id'    => 'alg_wc_custom_order_numbers_options',
					),
					array(
						'title'    => __( 'WooCommerce Dawnwing Custom Order Numbers', 'custom-order-numbers-for-woocommerce' ),
						'desc'     => '<strong>' . __( 'Enable plugin', 'custom-order-numbers-for-woocommerce' ) . '</strong>',
						'desc_tip' => __( 'Dawnwing Custom Order Numbers for WooCommerce.', 'custom-order-numbers-for-woocommerce' ),
						'id'       => 'alg_wc_custom_order_numbers_enabled',
						'default'  => 'yes',
						'type'     => 'checkbox',
					),
					array(
						'title'   => __( 'Order numbers counter', 'custom-order-numbers-for-woocommerce' ),
						'id'      => 'alg_wc_custom_order_numbers_counter_type',
						'default' => 'sequential',
						'type'    => 'select',
						'options' => array(
							'sequential' => __( 'Sequential', 'custom-order-numbers-for-woocommerce' ),
							'order_id'   => __( 'Order ID', 'custom-order-numbers-for-woocommerce' ),
							'hash_crc32' => __( 'Pseudorandom - crc32 Hash (max 10 digits)', 'custom-order-numbers-for-woocommerce' ),
						),
					),
					array(
						'title'    => __( 'Sequential: Next order number', 'custom-order-numbers-for-woocommerce' ),
						'desc_tip' => __( 'Next new order will be given this number.', 'custom-order-numbers-for-woocommerce' ) . ' ' .
							__( 'Use "Renumerate Orders tool" for existing orders.', 'custom-order-numbers-for-woocommerce' ) . ' ' .
							__( 'This will be ignored if sequential order numbering is disabled.', 'custom-order-numbers-for-woocommerce' ),
						'id'       => 'alg_wc_custom_order_numbers_counter',
						'default'  => 1,
						'type'     => 'number',
					),
					array(
						'title'    => __( 'Sequential: Reset counter', 'custom-order-numbers-for-woocommerce' ),
						'desc_tip' => __( 'This will be ignored if sequential order numbering is disabled.', 'custom-order-numbers-for-woocommerce' ),
						'id'       => 'alg_wc_custom_order_numbers_counter_reset_enabled',
						'default'  => 'no',
						'type'     => 'select',
						'options'  => array(
							'no'      => __( 'Disabled', 'custom-order-numbers-for-woocommerce' ),
							'daily'   => __( 'Daily', 'custom-order-numbers-for-woocommerce' ),
							'monthly' => __( 'Monthly', 'custom-order-numbers-for-woocommerce' ),
							'yearly'  => __( 'Yearly', 'custom-order-numbers-for-woocommerce' ),
						),
					),
					array(
						'desc'     => '<br>' . __( 'Reset counter value.', 'custom-order-numbers-for-woocommerce' ),
						'desc_tip' => __( 'Counter value to reset to.', 'custom-order-numbers-for-woocommerce' ) . ' ' .
							__( 'This will be ignored if "Sequential: Reset counter" option is set to "Disabled".', 'custom-order-numbers-for-woocommerce' ),
						'id'       => 'alg_wc_custom_order_numbers_counter_reset_counter_value',
						'default'  => 1,
						'type'     => 'number',
					),
					array(
						'title'    => __( 'Order number custom prefix', 'custom-order-numbers-for-woocommerce' ),
						'desc_tip' => __( 'Prefix before order number (optional). This will change the prefixes for all existing orders.', 'custom-order-numbers-for-woocommerce' ),
						'id'       => 'alg_wc_custom_order_numbers_prefix',
						'default'  => '',
						'type'     => 'text',
					),
					array(
						'type' => 'sectionend',
						'id'   => 'alg_wc_custom_order_numbers_options',
					),
				),
				$settings
			);
			return $settings;
		}

	}

endif;

return new Alg_WC_Custom_Order_Numbers_Settings_General();
