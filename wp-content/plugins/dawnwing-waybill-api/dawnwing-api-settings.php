<?php

class DawnwingGenerateWaybill
{
    private $dawnwing_generate_waybill_options;

    public function __construct()
    {
        add_action('admin_menu', array($this, 'dawnwing_generate_waybill_add_plugin_page'));
        add_action('admin_init', array($this, 'dawnwing_generate_waybill_page_init'));
    }

    public function dawnwing_generate_waybill_add_plugin_page()
    {
        add_submenu_page(
            'woocommerce',
            'Dawnwing Generate Waybill', // page_title
            'Dawnwing Generate Waybill', // menu_title
            'manage_woocommerce', // capability
            'dawnwing-generate-waybill', // menu_slug
            array($this, 'dawnwing_generate_waybill_create_admin_page'),
            1
        );
    }

    public function dawnwing_generate_waybill_create_admin_page()
    {
        $this->dawnwing_generate_waybill_options = get_option('dawnwing_generate_waybill_option_name');
        $dawnwing_generate_waybill_options = get_option('dawnwing_generate_waybill_option_name');
        $dawnwing_username_0 = $dawnwing_generate_waybill_options['dawnwing_username_0']; // Dawnwing Username
        $dawnwing_password_1 = $dawnwing_generate_waybill_options['dawnwing_password_1']; // Dawnwing Password


        $generatetoken = [
           
        ];
        $data = array("userName" => $dawnwing_username_0, "password" => $dawnwing_password_1);
        $data_string = json_encode($data);


        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'http://swatws.dawnwing.co.za/dwwebservices/v2/uat/api/Token',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => $data_string,

            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        $response_array = json_decode($response, true);
        update_option('dawnwing_api_token', $response_array['token']);
        $token = get_option('dawnwing_api_token');

?>
        <div class="wrap">
            <h2>Dawnwing Generate Waybill</h2>
            <p>Settings to populate the fields to generate and send Waybills to Dawnwing</p>
            <p>Click <a href='/wp-admin/admin.php?page=wc-settings&tab=alg_wc_custom_order_numbers'>here</a> to change your order number structure and sequence</p>
            <?php
            $dawnwing_generate_waybill_options = get_option('dawnwing_generate_waybill_option_name');
            if ($token) {
                echo "Token Succesfully Generated";
            } else "Enter your username and password to generate your token";
            ?>
            <?php settings_errors(); ?>



            <form method="post" action="options.php">
                <?php
                settings_fields('dawnwing_generate_waybill_option_group');
                do_settings_sections('dawnwing-generate-waybill-admin');
                submit_button();
                ?>
            </form>
        </div>
<?php }

    public function dawnwing_generate_waybill_page_init()
    {
        register_setting(
            'dawnwing_generate_waybill_option_group', // option_group
            'dawnwing_generate_waybill_option_name', // option_name
            array($this, 'dawnwing_generate_waybill_sanitize') // sanitize_callback
        );

        add_settings_section(
            'dawnwing_generate_waybill_setting_section', // id
            'Settings', // title
            array($this, 'dawnwing_generate_waybill_section_info'), // callback
            'dawnwing-generate-waybill-admin' // page
        );

        add_settings_field(
            'dawnwing_username_0', // id
            'Dawnwing Username', // title
            array($this, 'dawnwing_username_0_callback'), // callback
            'dawnwing-generate-waybill-admin', // page
            'dawnwing_generate_waybill_setting_section' // section
        );

        add_settings_field(
            'dawnwing_password_1', // id
            'Dawnwing Password', // title
            array($this, 'dawnwing_password_1_callback'), // callback
            'dawnwing-generate-waybill-admin', // page
            'dawnwing_generate_waybill_setting_section' // section
        );

        add_settings_field(
            'account_number_2', // id
            'Account Number', // title
            array($this, 'account_number_2_callback'), // callback
            'dawnwing-generate-waybill-admin', // page
            'dawnwing_generate_waybill_setting_section' // section
        );

        add_settings_field(
            'send_company_3', // id
            'Send Company', // title
            array($this, 'send_company_3_callback'), // callback
            'dawnwing-generate-waybill-admin', // page
            'dawnwing_generate_waybill_setting_section' // section
        );

        add_settings_field(
            'send_address_1_4', // id
            'Send Address 1', // title
            array($this, 'send_address_1_4_callback'), // callback
            'dawnwing-generate-waybill-admin', // page
            'dawnwing_generate_waybill_setting_section' // section
        );

        add_settings_field(
            'send_address_2_5', // id
            'Send Address 2', // title
            array($this, 'send_address_2_5_callback'), // callback
            'dawnwing-generate-waybill-admin', // page
            'dawnwing_generate_waybill_setting_section' // section
        );

        add_settings_field(
            'send_address_3_6', // id
            'Send Address 3', // title
            array($this, 'send_address_3_6_callback'), // callback
            'dawnwing-generate-waybill-admin', // page
            'dawnwing_generate_waybill_setting_section' // section
        );

        add_settings_field(
            'send_address_4_7', // id
            'Send Address 4', // title
            array($this, 'send_address_4_7_callback'), // callback
            'dawnwing-generate-waybill-admin', // page
            'dawnwing_generate_waybill_setting_section' // section
        );

        add_settings_field(
            'send_address_5_8', // id
            'Send Address 5', // title
            array($this, 'send_address_5_8_callback'), // callback
            'dawnwing-generate-waybill-admin', // page
            'dawnwing_generate_waybill_setting_section' // section
        );

        add_settings_field(
            'send_contact_person_9', // id
            'Send Contact Person', // title
            array($this, 'send_contact_person_9_callback'), // callback
            'dawnwing-generate-waybill-admin', // page
            'dawnwing_generate_waybill_setting_section' // section
        );

        add_settings_field(
            'send_work_tel_10', // id
            'Send Work Tel', // title
            array($this, 'send_work_tel_10_callback'), // callback
            'dawnwing-generate-waybill-admin', // page
            'dawnwing_generate_waybill_setting_section' // section
        );

        add_settings_field(
            'parcel_description_11', // id
            'Parcel Description', // title
            array($this, 'parcel_description_11_callback'), // callback
            'dawnwing-generate-waybill-admin', // page
            'dawnwing_generate_waybill_setting_section' // section
        );
    }

    public function dawnwing_generate_waybill_sanitize($input)
    {
        $sanitary_values = array();
        if (isset($input['dawnwing_username_0'])) {
            $sanitary_values['dawnwing_username_0'] = sanitize_text_field($input['dawnwing_username_0']);
        }

        if (isset($input['dawnwing_password_1'])) {
            $sanitary_values['dawnwing_password_1'] = sanitize_text_field($input['dawnwing_password_1']);
        }

        if (isset($input['account_number_2'])) {
            $sanitary_values['account_number_2'] = sanitize_text_field($input['account_number_2']);
        }

        if (isset($input['send_company_3'])) {
            $sanitary_values['send_company_3'] = sanitize_text_field($input['send_company_3']);
        }

        if (isset($input['send_address_1_4'])) {
            $sanitary_values['send_address_1_4'] = sanitize_text_field($input['send_address_1_4']);
        }

        if (isset($input['send_address_2_5'])) {
            $sanitary_values['send_address_2_5'] = sanitize_text_field($input['send_address_2_5']);
        }

        if (isset($input['send_address_3_6'])) {
            $sanitary_values['send_address_3_6'] = sanitize_text_field($input['send_address_3_6']);
        }

        if (isset($input['send_address_4_7'])) {
            $sanitary_values['send_address_4_7'] = sanitize_text_field($input['send_address_4_7']);
        }

        if (isset($input['send_address_5_8'])) {
            $sanitary_values['send_address_5_8'] = sanitize_text_field($input['send_address_5_8']);
        }

        if (isset($input['send_contact_person_9'])) {
            $sanitary_values['send_contact_person_9'] = sanitize_text_field($input['send_contact_person_9']);
        }

        if (isset($input['send_work_tel_10'])) {
            $sanitary_values['send_work_tel_10'] = sanitize_text_field($input['send_work_tel_10']);
        }

        if (isset($input['parcel_description_11'])) {
            $sanitary_values['parcel_description_11'] = sanitize_text_field($input['parcel_description_11']);
        }

        return $sanitary_values;
    }

    public function dawnwing_generate_waybill_section_info()
    {
    }

    public function dawnwing_username_0_callback()
    {
        printf(
            '<input class="regular-text" type="text" name="dawnwing_generate_waybill_option_name[dawnwing_username_0]" id="dawnwing_username_0" value="%s">',
            isset($this->dawnwing_generate_waybill_options['dawnwing_username_0']) ? esc_attr($this->dawnwing_generate_waybill_options['dawnwing_username_0']) : ''
        );
    }

    public function dawnwing_password_1_callback()
    {
        printf(
            '<input class="regular-text" type="text" name="dawnwing_generate_waybill_option_name[dawnwing_password_1]" id="dawnwing_password_1" value="%s">',
            isset($this->dawnwing_generate_waybill_options['dawnwing_password_1']) ? esc_attr($this->dawnwing_generate_waybill_options['dawnwing_password_1']) : ''
        );
    }

    public function account_number_2_callback()
    {
        printf(
            '<input class="regular-text" type="text" name="dawnwing_generate_waybill_option_name[account_number_2]" id="account_number_2" value="%s">',
            isset($this->dawnwing_generate_waybill_options['account_number_2']) ? esc_attr($this->dawnwing_generate_waybill_options['account_number_2']) : ''
        );
    }

    public function send_company_3_callback()
    {
        printf(
            '<input class="regular-text" type="text" name="dawnwing_generate_waybill_option_name[send_company_3]" id="send_company_3" value="%s">',
            isset($this->dawnwing_generate_waybill_options['send_company_3']) ? esc_attr($this->dawnwing_generate_waybill_options['send_company_3']) : ''
        );
    }

    public function send_address_1_4_callback()
    {
        printf(
            '<input class="regular-text" type="text" name="dawnwing_generate_waybill_option_name[send_address_1_4]" id="send_address_1_4" value="%s">',
            isset($this->dawnwing_generate_waybill_options['send_address_1_4']) ? esc_attr($this->dawnwing_generate_waybill_options['send_address_1_4']) : ''
        );
    }

    public function send_address_2_5_callback()
    {
        printf(
            '<input class="regular-text" type="text" name="dawnwing_generate_waybill_option_name[send_address_2_5]" id="send_address_2_5" value="%s">',
            isset($this->dawnwing_generate_waybill_options['send_address_2_5']) ? esc_attr($this->dawnwing_generate_waybill_options['send_address_2_5']) : ''
        );
    }

    public function send_address_3_6_callback()
    {
        printf(
            '<input class="regular-text" type="text" name="dawnwing_generate_waybill_option_name[send_address_3_6]" id="send_address_3_6" value="%s">',
            isset($this->dawnwing_generate_waybill_options['send_address_3_6']) ? esc_attr($this->dawnwing_generate_waybill_options['send_address_3_6']) : ''
        );
    }

    public function send_address_4_7_callback()
    {
        printf(
            '<input class="regular-text" type="text" name="dawnwing_generate_waybill_option_name[send_address_4_7]" id="send_address_4_7" value="%s">',
            isset($this->dawnwing_generate_waybill_options['send_address_4_7']) ? esc_attr($this->dawnwing_generate_waybill_options['send_address_4_7']) : ''
        );
    }

    public function send_address_5_8_callback()
    {
        printf(
            '<input class="regular-text" type="text" name="dawnwing_generate_waybill_option_name[send_address_5_8]" id="send_address_5_8" value="%s">',
            isset($this->dawnwing_generate_waybill_options['send_address_5_8']) ? esc_attr($this->dawnwing_generate_waybill_options['send_address_5_8']) : ''
        );
    }

    public function send_contact_person_9_callback()
    {
        printf(
            '<input class="regular-text" type="text" name="dawnwing_generate_waybill_option_name[send_contact_person_9]" id="send_contact_person_9" value="%s">',
            isset($this->dawnwing_generate_waybill_options['send_contact_person_9']) ? esc_attr($this->dawnwing_generate_waybill_options['send_contact_person_9']) : ''
        );
    }

    public function send_work_tel_10_callback()
    {
        printf(
            '<input class="regular-text" type="text" name="dawnwing_generate_waybill_option_name[send_work_tel_10]" id="send_work_tel_10" value="%s">',
            isset($this->dawnwing_generate_waybill_options['send_work_tel_10']) ? esc_attr($this->dawnwing_generate_waybill_options['send_work_tel_10']) : ''
        );
    }

    public function parcel_description_11_callback()
    {
        printf(
            '<input class="regular-text" type="text" name="dawnwing_generate_waybill_option_name[parcel_description_11]" id="parcel_description_11" value="%s">',
            isset($this->dawnwing_generate_waybill_options['parcel_description_11']) ? esc_attr($this->dawnwing_generate_waybill_options['parcel_description_11']) : ''
        );
    }
}
if (is_admin())
    $dawnwing_generate_waybill = new DawnwingGenerateWaybill();

/*
* Retrieve this value with:
* $dawnwing_generate_waybill_options = get_option( 'dawnwing_generate_waybill_option_name' ); // Array of All Options
* $dawnwing_username_0 = $dawnwing_generate_waybill_options['dawnwing_username_0']; // Dawnwing Username
* $dawnwing_password_1 = $dawnwing_generate_waybill_options['dawnwing_password_1']; // Dawnwing Password
* $account_number_2 = $dawnwing_generate_waybill_options['account_number_2']; // Account Number
* $send_company_3 = $dawnwing_generate_waybill_options['send_company_3']; // Send Company
* $send_address_1_4 = $dawnwing_generate_waybill_options['send_address_1_4']; // Send Address 1
* $send_address_2_5 = $dawnwing_generate_waybill_options['send_address_2_5']; // Send Address 2
* $send_address_3_6 = $dawnwing_generate_waybill_options['send_address_3_6']; // Send Address 3
* $send_address_4_7 = $dawnwing_generate_waybill_options['send_address_4_7']; // Send Address 4
* $send_address_5_8 = $dawnwing_generate_waybill_options['send_address_5_8']; // Send Address 5
* $send_contact_person_9 = $dawnwing_generate_waybill_options['send_contact_person_9']; // Send Contact Person
* $send_work_tel_10 = $dawnwing_generate_waybill_options['send_work_tel_10']; // Send Work Tel
* $parcel_description_11 = $dawnwing_generate_waybill_options['parcel_description_11']; // Parcel Description
*/
