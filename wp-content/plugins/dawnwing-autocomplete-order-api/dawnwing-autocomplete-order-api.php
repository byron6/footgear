<?php

/**
 * @link			  https://www.semantica.co.za/
 * @since			  1.0.0
 * @package			  Semantica Core
 *
 * @wordpress-plugin
 * Plugin Name:		  Dawnwing Auto Complete Order APi
 * Plugin URI:		  https://www.semantica.co.za/
 * Description:		  Semantica Core Integrations
 * Version:			  1.0.0
 * Author:			  Semantica
 * Author URI:		  https://www.semantica.co.za/
 * License:			  GPL-2.0+
 * License URI:		  http://www.gnu.org/licenses/gpl-2.0.txt
 */


require_once($_SERVER['DOCUMENT_ROOT'].'/wp-load.php' );

$orders = wc_get_orders(
    array(
        'limit'=>-1,
        'status'=> array( 'wc-shipped') //when live change this to 'wp-shipping'
    )
);


foreach($orders as $order){
    $orderID = $order->id;
    $orderNum = $order->get_meta('_order_number');


    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => 'http://swatws.dawnwing.co.za/dwwebservices/v2/live/api/trackandtrace?WaybillNo=FTGC' .$orderNum,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        CURLOPT_HTTPHEADER => array(
            'Authorization: Bearer eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJJZCI6IjExMCIsImV4cCI6MTk1MjYwMjY5NSwiaXNzIjoiaHR0cDovLzQxLjAuNjkuMTk3LyIsImF1ZCI6Imh0dHA6Ly80MS4wLjY5LjE5Ny8ifQ.FtovsNtqjCf_sgPsuFeKcUb6ai6Jt_upw5oyRnaqcglAC4uKDBPIAfsacGTEnnZx_GOWWMHatg8LOn1sNazbkw'
        ),
    ));

    $response = curl_exec($curl);

    curl_close($curl);

    $decoded = (array) json_decode($response, true);

    $trackCode = $decoded['trackEvents'][0]['trackEventCode'];

    //Ensure to review correct track code to use on live.
    if($trackCode == 4 or 8 or 6){
        $order->update_status( 'completed' );
        echo '<pre>'.print_r('Order #'.$orderID.' is now completed.', true).'</pre>';
    }


}


