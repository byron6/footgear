<?php
if ( !defined( 'ABSPATH' ) || !defined( 'YITH_WCBM_PREMIUM' ) ) {
    exit; // Exit if accessed directly
}

/**
 * Implements features of FREE version of YITH WooCommerce Badge Management
 *
 * @class   YITH_WCBM_Premium
 * @package YITH WooCommerce Badge Management
 * @since   1.0.0
 * @author  Yithemes
 */

if ( !class_exists( 'YITH_WCBM_Premium' ) ) {
    /**
     * YITH WooCommerce Badge Management
     *
     * @since 1.0.0
     */
    class YITH_WCBM_Premium extends YITH_WCBM {
		/**
		 * Single instance of the class
		 *
		 * @var YITH_WCBM_Premium
		 */
		protected static $_instance;

		/**
		 * Constructor
		 *
		 * @since 1.0.0
		 */
		public function __construct() {
			parent::__construct();

			// register plugin to licence/update system
			add_action( 'wp_loaded', array( $this, 'register_plugin_for_activation' ), 99 );
			add_action( 'admin_init', array( $this, 'register_plugin_for_updates' ) );
		}

		/**
		 * Register plugins for activation tab
		 *
		 * @return void
		 */
		public function register_plugin_for_activation() {
			if ( function_exists( 'YIT_Plugin_Licence' ) ) {
				YIT_Plugin_Licence()->register( YITH_WCBM_INIT, YITH_WCBM_SECRET_KEY, YITH_WCBM_SLUG );
			}
		}

		/**
		 * Register plugins for update tab
		 *
		 * @return void
		 */
		public function register_plugin_for_updates() {
			if ( function_exists( 'YIT_Upgrade' ) ) {
				YIT_Upgrade()->register( YITH_WCBM_SLUG, YITH_WCBM_INIT );
			}
		}
    }
}

/**
 * Unique access to instance of YITH_WCBM_Premium class
 *
 * @return YITH_WCBM_Premium
 * @deprecated since 1.3.0 use YITH_WCBM() instead
 * @since 1.0.0
 */
function YITH_WCBM_Premium() {
    return YITH_WCBM();
}