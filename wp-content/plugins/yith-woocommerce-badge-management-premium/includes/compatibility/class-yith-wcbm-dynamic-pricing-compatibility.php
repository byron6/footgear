<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

/**
 * Dynamic Pricing Compatibility Class
 *
 * @class   YITH_WCBM_Dynamic_Pricing_Compatibility
 * @package Yithemes
 * @since   1.2.8
 * @author  Yithemes
 */
class YITH_WCBM_Dynamic_Pricing_Compatibility {

	/**
	 * Single instance of the class
	 *
	 * @var YITH_WCBM_Dynamic_Pricing_Compatibility
	 * @since 1.0.0
	 */
	protected static $_instance;


	/**
	 * @var array
	 */
	private $dynamic_rules;

	/**
	 * @var array
	 */
	private $valid_dynamic_rules;


	/**
	 * Returns single instance of the class
	 *
	 * @return YITH_WCBM_Dynamic_Pricing_Compatibility
	 * @since 1.0.0
	 */
	public static function get_instance() {
		return ! is_null( self::$_instance ) ? self::$_instance : self::$_instance = new self();
	}

	/**
	 * Constructor
	 *
	 * @access public
	 * @since  1.0.0
	 */
	public function __construct() {
		add_filter( 'yith_wcbm_settings_admin_tabs', array( $this, 'add_admin_tabs' ) );

		add_filter( 'yith_wcmb_get_badges_premium', array( $this, 'add_dynamic_pricing_badges' ), 10, 2 );

		add_filter( 'yith_wcbm_advanced_badge_product_price', array( $this, 'get_discounted_price' ), 10, 2 );
	}

	/**
	 * Retrieve the discounted price
	 *
	 * @param float      $price   The price.
	 * @param WC_Product $product The product.
	 *
	 * @return float
	 * @since 1.4.9
	 */
	public function get_discounted_price( $price, $product ) {
		$price = (float) YITH_WC_Dynamic_Pricing()->get_discount_price( $product->get_price( 'edit' ), $product );

		return $price;
	}

	/**
	 * get dynamic Pricing Rules
	 *
	 * @return array
	 */
	public function get_rules() {
		if ( isset( $this->dynamic_rules ) ) {
			return $this->dynamic_rules;
		}

		if ( is_callable( array( YITH_WC_Dynamic_Pricing(), 'recover_pricing_rules' ) ) ) {
			$this->dynamic_rules = YITH_WC_Dynamic_Pricing()->recover_pricing_rules();
		} else {
			$this->dynamic_rules = YITH_WC_Dynamic_Pricing()->get_option( 'pricing-rules' );
		}

		return $this->dynamic_rules;
	}

	/**
	 * get dynamic Pricing Rules
	 *
	 * @return array
	 */
	public function get_valid_rules() {
		if ( isset( $this->valid_dynamic_rules ) ) {
			return $this->valid_dynamic_rules;
		}

		if ( is_callable( array( YITH_WC_Dynamic_Pricing(), 'get_pricing_rules' ) ) ) {
			$this->valid_dynamic_rules = YITH_WC_Dynamic_Pricing()->get_pricing_rules();
		} else {
			$this->valid_dynamic_rules = YITH_WC_Dynamic_Pricing()->get_option( 'pricing-rules' );
		}

		return $this->valid_dynamic_rules;
	}

	/**
	 * @param string     $badge_html
	 * @param WC_Product $product
	 *
	 * @return string
	 */
	public function add_dynamic_pricing_badges( $badge_html, $product ) {
		$rules   = $this->get_valid_rules();
		$product = wc_get_product( $product );

		if ( $product && ! empty( $rules ) ) {
			$apply_bulk = false;
			foreach ( $rules as $rule_id => $rule ) {
				$rule_badge         = get_option( 'yith-wcbm-dynamic-pricing-badge-' . $rule_id );
				$product_is_in_rule = $this->product_is_in_rule( $product->get_id(), $rule );

				if ( ! empty( $rule_badge ) && $rule_badge !== 'none' && $product_is_in_rule && ( ! $apply_bulk || 'bulk' !== $rule['discount_mode'] ) ) {
					$badge_html .= yith_wcbm_get_badge_premium( $rule_badge, $product->get_id() );
					if ( 'bulk' === $rule['discount_mode'] ) {
						$apply_bulk = true;
					}
				}
			}
		}

		return $badge_html;
	}

	/**
	 * Add Admin Setting Tabs
	 *
	 * @param $admin_tabs_free
	 *
	 * @return mixed
	 */
	public function add_admin_tabs( $admin_tabs_free ) {
		$admin_tabs_free['dynamic-pricing'] = __( 'Dynamic Pricing', 'yith-woocommerce-badges-management' );

		return $admin_tabs_free;
	}

	/**
	 * check if a product is in one rule
	 *
	 * @param $product_id
	 * @param $rule
	 *
	 * @return bool
	 */
	public function product_is_in_rule( $product_id, $rule ) {

		$product = wc_get_product( $product_id );
		if ( ! $product ) {
			return false;
		}

		$is_in_rule          = YITH_WC_Dynamic_Pricing_Helper()->valid_product_to_apply( $rule, $product );
		$apply_adjustment_to = isset( $rule['apply_adjustment'] ) ? $rule['apply_adjustment'] : false;

		if ( $apply_adjustment_to ) {
			switch ( $apply_adjustment_to ) {

				case 'categories_list_excluded':
				case 'tags_list_excluded':
				case 'products_list_excluded':
				case 'brand_list_excluded':
					$is_in_rule = YITH_WC_Dynamic_Pricing_Helper()->valid_product_to_adjustment( $rule, $product );
					break;
			}
		}

		return $is_in_rule;
	}
}
