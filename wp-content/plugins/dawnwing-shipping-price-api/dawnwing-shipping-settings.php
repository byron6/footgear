<?php

class DawnwingLiveShippingQuote {
	private $dawnwing_live_shipping_quote_options;

	public function __construct() {
		add_action( 'admin_menu', array( $this, 'dawnwing_live_shipping_quote_add_plugin_page' ) );
		add_action( 'admin_init', array( $this, 'dawnwing_live_shipping_quote_page_init' ) );
	}

	public function dawnwing_live_shipping_quote_add_plugin_page() {
	
		add_submenu_page(
			'woocommerce',
			'Dawnwing Live Shipping Quote', // page_title
			'Dawnwing Live Shipping Quote', // menu_title
			'manage_woocommerce', // capability
			'dawnwing-live-shipping-quote', // menu_slug
			array( $this, 'dawnwing_live_shipping_quote_create_admin_page' ), // function
			1 // position
		);
	}

	public function dawnwing_live_shipping_quote_create_admin_page() {
		$this->dawnwing_live_shipping_quote_options = get_option( 'dawnwing_live_shipping_quote_option_name' ); 
        $dawnwing_live_shipping_quote_options = get_option( 'dawnwing_live_shipping_quote_option_name' );
        $dawnwing_username_0 = $dawnwing_live_shipping_quote_options['dawnwing_username_0']; // Dawnwing Username
        $dawnwing_password_1 = $dawnwing_live_shipping_quote_options['dawnwing_password_1']; // Dawnwing Password


        $generatetoken = [
            "userName" => $dawnwing_username_0,
            "password" => $dawnwing_password_1
        ];
        $data = array("userName" => $dawnwing_username_0, "password" => $dawnwing_password_1);
        $data_string = json_encode($data);


        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'http://swatws.dawnwing.co.za/dwwebservices/v2/uat/api/Token',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => $data_string,

            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        $response_array = json_decode($response, true);
        update_option('dawnwing_api_token', $response_array['token']);
        $token = get_option('dawnwing_api_token');

?>
		<div class="wrap">
			<h2>Dawnwing Live Shipping Quote</h2>
			<p>Settings to populate the fields of the Shipping Notices and general settings</p>
			<?php
            $token = get_option('dawnwing_api_token');
            if ($token) {
                echo "Token Succesfully Generated";
            } else {
            echo "Enter your username and password to generate your token";
            }
            ?>
			<?php settings_errors(); ?>

			<form method="post" action="options.php">
				<?php
					settings_fields( 'dawnwing_live_shipping_quote_option_group' );
					do_settings_sections( 'dawnwing-live-shipping-quote-admin' );
					submit_button();
				?>
			</form>
		</div>
	<?php }

	public function dawnwing_live_shipping_quote_page_init() {
		register_setting(
			'dawnwing_live_shipping_quote_option_group', // option_group
			'dawnwing_live_shipping_quote_option_name', // option_name
			array( $this, 'dawnwing_live_shipping_quote_sanitize' ) // sanitize_callback
		);

		add_settings_section(
			'dawnwing_live_shipping_quote_setting_section', // id
			'Settings', // title
			array( $this, 'dawnwing_live_shipping_quote_section_info' ), // callback
			'dawnwing-live-shipping-quote-admin' // page
		);

		add_settings_field(
			'dawnwing_username_0', // id
			'Dawnwing Username', // title
			array( $this, 'dawnwing_username_0_callback' ), // callback
			'dawnwing-live-shipping-quote-admin', // page
			'dawnwing_live_shipping_quote_setting_section' // section
		);

		add_settings_field(
			'dawnwing_password_1', // id
			'Dawnwing Password', // title
			array( $this, 'dawnwing_password_1_callback' ), // callback
			'dawnwing-live-shipping-quote-admin', // page
			'dawnwing_live_shipping_quote_setting_section' // section
		);

		add_settings_field(
			'_onx_failed_2', // id
			' ONX Failed', // title
			array( $this, '_onx_failed_2_callback' ), // callback
			'dawnwing-live-shipping-quote-admin', // page
			'dawnwing_live_shipping_quote_setting_section' // section
		);

		add_settings_field(
			'overnight_express_3', // id
			'Overnight Express', // title
			array( $this, 'overnight_express_3_callback' ), // callback
			'dawnwing-live-shipping-quote-admin', // page
			'dawnwing_live_shipping_quote_setting_section' // section
		);

		add_settings_field(
			'economy_4', // id
			'Economy', // title
			array( $this, 'economy_4_callback' ), // callback
			'dawnwing-live-shipping-quote-admin', // page
			'dawnwing_live_shipping_quote_setting_section' // section
		);

		add_settings_field(
			'economy_free_shipping_5', // id
			'Economy - Free Shipping', // title
			array( $this, 'economy_free_shipping_5_callback' ), // callback
			'dawnwing-live-shipping-quote-admin', // page
			'dawnwing_live_shipping_quote_setting_section' // section
		);

		add_settings_field(
			'fallback_shipping_6', // id
			'Fallback Shipping', // title
			array( $this, 'fallback_shipping_6_callback' ), // callback
			'dawnwing-live-shipping-quote-admin', // page
			'dawnwing_live_shipping_quote_setting_section' // section
		);

		add_settings_field(
			'express_after_free_shipping_7', // id
			'Express After Free Shipping', // title
			array( $this, 'express_after_free_shipping_7_callback' ), // callback
			'dawnwing-live-shipping-quote-admin', // page
			'dawnwing_live_shipping_quote_setting_section' // section
		);

		add_settings_field(
			'no_postcode_error_8', // id
			'No Postcode Error', // title
			array( $this, 'no_postcode_error_8_callback' ), // callback
			'dawnwing-live-shipping-quote-admin', // page
			'dawnwing_live_shipping_quote_setting_section' // section
		);
	}

	public function dawnwing_live_shipping_quote_sanitize($input) {
		$sanitary_values = array();
		if ( isset( $input['dawnwing_username_0'] ) ) {
			$sanitary_values['dawnwing_username_0'] = sanitize_text_field( $input['dawnwing_username_0'] );
		}

		if ( isset( $input['dawnwing_password_1'] ) ) {
			$sanitary_values['dawnwing_password_1'] = sanitize_text_field( $input['dawnwing_password_1'] );
		}

		if ( isset( $input['_onx_failed_2'] ) ) {
			$sanitary_values['_onx_failed_2'] = sanitize_text_field( $input['_onx_failed_2'] );
		}

		if ( isset( $input['overnight_express_3'] ) ) {
			$sanitary_values['overnight_express_3'] = sanitize_text_field( $input['overnight_express_3'] );
		}

		if ( isset( $input['economy_4'] ) ) {
			$sanitary_values['economy_4'] = sanitize_text_field( $input['economy_4'] );
		}

		if ( isset( $input['economy_free_shipping_5'] ) ) {
			$sanitary_values['economy_free_shipping_5'] = sanitize_text_field( $input['economy_free_shipping_5'] );
		}

		if ( isset( $input['fallback_shipping_6'] ) ) {
			$sanitary_values['fallback_shipping_6'] = sanitize_text_field( $input['fallback_shipping_6'] );
		}

		if ( isset( $input['express_after_free_shipping_7'] ) ) {
			$sanitary_values['express_after_free_shipping_7'] = sanitize_text_field( $input['express_after_free_shipping_7'] );
		}

		if ( isset( $input['no_postcode_error_8'] ) ) {
			$sanitary_values['no_postcode_error_8'] = sanitize_text_field( $input['no_postcode_error_8'] );
		}

		return $sanitary_values;
	}

	public function dawnwing_live_shipping_quote_section_info() {
		
	}

	public function dawnwing_username_0_callback() {
		printf(
			'<input class="regular-text" type="text" name="dawnwing_live_shipping_quote_option_name[dawnwing_username_0]" id="dawnwing_username_0" value="%s">',
			isset( $this->dawnwing_live_shipping_quote_options['dawnwing_username_0'] ) ? esc_attr( $this->dawnwing_live_shipping_quote_options['dawnwing_username_0']) : ''
		);
	}

	public function dawnwing_password_1_callback() {
		printf(
			'<input class="regular-text" type="text" name="dawnwing_live_shipping_quote_option_name[dawnwing_password_1]" id="dawnwing_password_1" value="%s">',
			isset( $this->dawnwing_live_shipping_quote_options['dawnwing_password_1'] ) ? esc_attr( $this->dawnwing_live_shipping_quote_options['dawnwing_password_1']) : ''
		);
	}

	public function _onx_failed_2_callback() {
		printf(
			'<input class="regular-text" type="text" name="dawnwing_live_shipping_quote_option_name[_onx_failed_2]" id="_onx_failed_2" value="%s">',
			isset( $this->dawnwing_live_shipping_quote_options['_onx_failed_2'] ) ? esc_attr( $this->dawnwing_live_shipping_quote_options['_onx_failed_2']) : ''
		);
	}

	public function overnight_express_3_callback() {
		printf(
			'<input class="regular-text" type="text" name="dawnwing_live_shipping_quote_option_name[overnight_express_3]" id="overnight_express_3" value="%s">',
			isset( $this->dawnwing_live_shipping_quote_options['overnight_express_3'] ) ? esc_attr( $this->dawnwing_live_shipping_quote_options['overnight_express_3']) : ''
		);
	}

	public function economy_4_callback() {
		printf(
			'<input class="regular-text" type="text" name="dawnwing_live_shipping_quote_option_name[economy_4]" id="economy_4" value="%s">',
			isset( $this->dawnwing_live_shipping_quote_options['economy_4'] ) ? esc_attr( $this->dawnwing_live_shipping_quote_options['economy_4']) : ''
		);
	}

	public function economy_free_shipping_5_callback() {
		printf(
			'<input class="regular-text" type="text" name="dawnwing_live_shipping_quote_option_name[economy_free_shipping_5]" id="economy_free_shipping_5" value="%s">',
			isset( $this->dawnwing_live_shipping_quote_options['economy_free_shipping_5'] ) ? esc_attr( $this->dawnwing_live_shipping_quote_options['economy_free_shipping_5']) : ''
		);
	}

	public function fallback_shipping_6_callback() {
		printf(
			'<input class="regular-text" type="text" name="dawnwing_live_shipping_quote_option_name[fallback_shipping_6]" id="fallback_shipping_6" value="%s">',
			isset( $this->dawnwing_live_shipping_quote_options['fallback_shipping_6'] ) ? esc_attr( $this->dawnwing_live_shipping_quote_options['fallback_shipping_6']) : ''
		);
	}

	public function express_after_free_shipping_7_callback() {
		printf(
			'<input class="regular-text" type="text" name="dawnwing_live_shipping_quote_option_name[express_after_free_shipping_7]" id="express_after_free_shipping_7" value="%s">',
			isset( $this->dawnwing_live_shipping_quote_options['express_after_free_shipping_7'] ) ? esc_attr( $this->dawnwing_live_shipping_quote_options['express_after_free_shipping_7']) : ''
		);
	}

	public function no_postcode_error_8_callback() {
		printf(
			'<input class="regular-text" type="text" name="dawnwing_live_shipping_quote_option_name[no_postcode_error_8]" id="no_postcode_error_8" value="%s">',
			isset( $this->dawnwing_live_shipping_quote_options['no_postcode_error_8'] ) ? esc_attr( $this->dawnwing_live_shipping_quote_options['no_postcode_error_8']) : ''
		);
	}

}
if ( is_admin() )
	$dawnwing_live_shipping_quote = new DawnwingLiveShippingQuote();

/* 
 * Retrieve this value with:
 * $dawnwing_live_shipping_quote_options = get_option( 'dawnwing_live_shipping_quote_option_name' ); // Array of All Options
 * $dawnwing_username_0 = $dawnwing_live_shipping_quote_options['dawnwing_username_0']; // Dawnwing Username
 * $dawnwing_password_1 = $dawnwing_live_shipping_quote_options['dawnwing_password_1']; // Dawnwing Password
 * $_onx_failed_2 = $dawnwing_live_shipping_quote_options['_onx_failed_2']; //  ONX Failed
 * $overnight_express_3 = $dawnwing_live_shipping_quote_options['overnight_express_3']; // Overnight Express
 * $economy_4 = $dawnwing_live_shipping_quote_options['economy_4']; // Economy
 * $economy_free_shipping_5 = $dawnwing_live_shipping_quote_options['economy_free_shipping_5']; // Economy - Free Shipping
 * $fallback_shipping_6 = $dawnwing_live_shipping_quote_options['fallback_shipping_6']; // Fallback Shipping
 * $express_after_free_shipping_7 = $dawnwing_live_shipping_quote_options['express_after_free_shipping_7']; // Express After Free Shipping
 * $no_postcode_error_8 = $dawnwing_live_shipping_quote_options['no_postcode_error_8']; // No Postcode Error
 */
