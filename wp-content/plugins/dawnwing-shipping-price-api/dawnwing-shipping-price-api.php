<?php

/**
 * @link			  https://www.semantica.co.za/
 * @since			  1.0.0
 * @package			  Semantica Core
 *
 * @wordpress-plugin
 * Plugin Name:		  Dawnwing Shipping Price API
 * Plugin URI:		  https://www.semantica.co.za/
 * Description:		  Semantica Core Integrations
 * Version:			  1.0.0
 * Author:			  Semantica
 * Author URI:		  https://www.semantica.co.za/
 * License:			  GPL-2.0+
 * License URI:		  http://www.gnu.org/licenses/gpl-2.0.txt
 */
// Include Admin Page



add_filter( 'woocommerce_cart_no_shipping_available_html', 'change_no_shipping_text' ); // Alters message on Cart page
add_filter( 'woocommerce_no_shipping_available_html', 'change_no_shipping_text' ); // Alters message on Checkout page
function change_no_shipping_text() {
 
    return "No Postal Code found, enter your Postal Code to calculate shipping costs.";
}



$plugin_dir = ABSPATH . 'wp-content/plugins/dawnwing-shipping-price-api/';
include $plugin_dir . '/dawnwing-shipping-settings.php';
// Register Shipping Methods
if (in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins')))) {
    add_filter('woocommerce_shipping_methods', 'register_dawnwing_onx');
    function register_dawnwing_onx($methods)
    {
        $methods['dawnwing_onx'] = 'WC_Shipping_Onx';
        $methods['dawnwing_econ'] = 'WC_Shipping_Econ';
        return $methods;
    }
    function your_shipping_method_init()
    {
        // if (!class_exists('WC_Shipping_Onx')) {
//             class WC_Shipping_Onx extends WC_Shipping_Method
//             {
//                 public function __construct($instance_id = 80)
//                 {
//                     $this->id = 'dawnwing_onx';
//                     $this->instance_id = absint($instance_id);
//                     $this->method_title = __('Dawnwing Express');
//                     $this->method_description = __('Dawnwing Shipping method for live quite API - Overnight Express');
//                     $this->supports = array(
//                         'shipping-zones',
//                         'instance-settings',
//                     );
//                     $this->instance_form_fields = array(
//                         'enabled' => array(
//                             'title' => __('Enable/Disable'),
//                             'type' => 'checkbox',
//                             'label' => __('Enable this shipping method'),
//                             'default' => 'yes',
//                         ),
//                         'title' => array(
//                             'title' => __('Express'),
//                             'type' => 'text',
//                             'description' => __('This controls the title which the user sees during checkout.'),
//                             'default' => __('Express'),
//                             'desc_tip' => true,
//                         ),
//                     );
//                     $this->enabled = $this->get_option('enabled');
//                     $this->title = $this->get_option('title');
//                     add_action('woocommerce_update_options_shipping_' . $this->id, array($this, 'process_admin_options'));
//                 }
//                 public function calculate_shipping($package = array())
//                 {
//                     $plugin_dir = ABSPATH . 'wp-content/plugins/dawnwing-shipping-price-api/';
//                     include $plugin_dir . '/multiapi.php';
//                     $sum_total = $onxcheck - $econ;
//                     $cart_total_price = wc_prices_include_tax() ? WC()->cart->get_cart_contents_total() + WC()->cart->get_cart_contents_tax() : WC()->cart->get_cart_contents_total();
//                     if ($onx == '200') {
//                     } elseif ($cart_total_price <= 650) {
//                         $this->add_rate(
//                             array(
//                                 'id' => 'dawnwing_onx',
//                                 'label' => 'Express',
//                                 'cost' => $onx,
//                             )
//                         );
//                     } elseif ($cart_total_price >= 651) {
//                         $this->add_rate(
//                             array(
//                                 'id' => 'dawnwing_onx',
//                                 'label' => 'Express',
//                                 'cost' => $sum_total,
//                             )
//                         );
//                     }
//                 }
//             }
//         }
        if (!class_exists('WC_Shipping_Econ')) {
            class WC_Shipping_Econ extends WC_Shipping_Method
            {
                public function __construct($instance_id = 90)
                {
                    $plugin_dir = ABSPATH . 'wp-content/plugins/dawnwing-shipping-price-api/';
                    include $plugin_dir . '/multiapi.php';

                    if ($econ == '100') {
                        $this->id = 'dawnwing_shipping';
                    } else {
                        $this->id = 'dawnwing_econ';
                    }
                    $this->instance_id = absint($instance_id);
                    $this->method_title = __('Dawnwing Economy');
                    $this->method_description = __('Dawnwing Shipping method for live quite API - Economy.');
                    $this->supports = array(
                        'shipping-zones',
                        'instance-settings',
                    );
                    $this->instance_form_fields = array(
                        'enabled' => array(
                            'title' => __('Enable/Disable'),
                            'type' => 'checkbox',
                            'label' => __('Enable this shipping method'),
                            'default' => 'yes',
                        ),
                        'title' => array(
                            'title' => __('Economy'),
                            'type' => 'text',
                            'description' => __('This controls the title which the user sees during checkout.'),
                            'default' => __('Economy'),
                            'desc_tip' => true,
                        ),
                    );
                    $this->enabled = $this->get_option('enabled');
                    $this->title = $this->get_option('title');
                    add_action('woocommerce_update_options_shipping_' . $this->id, array($this, 'process_admin_options'));
                }
                public function calculate_shipping($package = array())
                {
                    $plugin_dir = ABSPATH . 'wp-content/plugins/dawnwing-shipping-price-api/';
                    include $plugin_dir . '/multiapi.php';
                    $cart_total_price = wc_prices_include_tax() ? WC()->cart->get_cart_contents_total() + WC()->cart->get_cart_contents_tax() : WC()->cart->get_cart_contents_total();
                    if ($postcode == '') {
                    } 
                    
                    elseif ($econerrorcheck == "1") {
                    }
                     elseif ($econstatus == "400") {
                    }
                    elseif ($econ == '100') {
                        $this->add_rate(
                            array(
                                'id' => 'dawnwing_shipping',
                                'label' => 'Shipping',
                                'cost' => '100',
                            )
                        );
                    } elseif ($cart_total_price <= 650) {
                        $this->add_rate(
                            array(
                                'id' => 'dawnwing_econ',
                                // 'label' => 'Standard (3-5 working days)',
								'label' => 'Standard',
                                'cost' => $econ,
                            )
                        );
                    } elseif ($cart_total_price >= 651) {
                        $this->add_rate(
                            array(
                                'id' => 'dawnwing_econ',
                                'label' => 'Free Shipping',
                                'cost' => '0',
                            )
                        );
                    }
                }
            }
        }
    }
    add_action('woocommerce_shipping_init', 'your_shipping_method_init');
}

// Disable Shipping if no postcode added or shipping zone selected
/* function disable_checkout_button_no_shipping()
{
    $package_counts = array();
    // get shipping packages and their rate counts
    $packages = WC()->shipping->get_packages();
    foreach ($packages as $key => $pkg)
        $package_counts[$key] = count($pkg['rates']);
    // remove button if any packages are missing shipping options
    if (in_array(0, $package_counts))
        remove_action('woocommerce_proceed_to_checkout', 'woocommerce_button_proceed_to_checkout', 20);
}
add_action('woocommerce_proceed_to_checkout', 'disable_checkout_button_no_shipping', 1);
*/
// Shipping Method Descriptions
add_action('woocommerce_after_shipping_rate', 'action_after_shipping_rate', 20, 2);
function action_after_shipping_rate($method, $index)
{
    $plugin_dir = ABSPATH . 'wp-content/plugins/dawnwing-shipping-price-api/';
    include $plugin_dir . '/multiapi.php';
    $onx_fail = get_option( 'onx_fail' );
    $onx_main = get_option( 'onx_main' );
    $econ_main = get_option( 'econ_main' );
    $econ_free = get_option( 'econ_free' );
    $fallback_shipping = get_option( 'fallback_shipping' );
    $onx_extra = get_option( 'onx_extra' );

    $cart_total_price = wc_prices_include_tax() ? WC()->cart->get_cart_contents_total() + WC()->cart->get_cart_contents_tax() : WC()->cart->get_cart_contents_total();
    if ($postcode == "") {
        add_action('woocommerce_cart_totals_after_shipping', 'shipping_zone_targeted_postcodes_custom_notice');
        function shipping_zone_targeted_postcodes_custom_notice()
        {
            echo '<tr class="shipping">
            <td colspan="2" style="text-align:left">' . sprintf(
                __("Please add your postcode to calculate shipping costs", "woocommerce"),
                '<strong>10%</strong>',

            ) . '</td>
        </tr>';
        }
    }
    if ($econerrorcheck == "1") {
        add_action('woocommerce_cart_totals_after_shipping', 'shipping_zone_targeted_postcodes_custom_notice');
        function shipping_zone_targeted_postcodes_custom_notice()
        {
            echo '<tr class="shipping">
            <td colspan="2" style="text-align:left">' . sprintf(
                __("Postcode does not exist", "woocommerce"),
                '<strong>10%</strong>',

            ) . '</td>
        </tr>';
        }
    }
    elseif ('dawnwing_shipping' === $method->id) {
        echo __("<div id='shipping_description' style='text-indent: 0px;font-size:12px;line-height: 1.5;'>" . $fallback_shipping . " <a style='text-decoration: underline;' href='/customer-care/refunds-exchange-policy/'>Read More</a></div>");
    }
    if ($onx = '200' & 'dawnwing_onx' === $method->id) {
        echo __("<div id='shipping_description' style='text-indent: 0px;font-size:12px;line-height: 1.5;'>" . $onx_fail . "<a style='text-decoration: underline;' href='/customer-care/refunds-exchange-policy/'>Read More</a></div>");
    }
    if ($cart_total_price <= 650 & 'dawnwing_onx' === $method->id) {
        echo __("<div id='shipping_description' style='text-indent: 0px;font-size:12px;line-height: 1.5;'>" . $onx_main . " <a style='text-decoration: underline;' href='/customer-care/refunds-exchange-policy/'>Read More</a></div>");
    }
 //    if ($cart_total_price <= 650 & 'dawnwing_econ' === $method->id) {
//         echo __("<div id='shipping_description' style='text-indent: 0px;font-size:12px;line-height: 1.5;'>" . $econ_main . " <a style='text-decoration: underline;' href='/customer-care/refunds-exchange-policy/'>Read More</a></div>");
//     }
//     if ($cart_total_price >= 651 & 'dawnwing_econ' === $method->id) {
//         echo __("<div id='shipping_description' style='text-indent: 0px;font-size:12px;line-height: 1.5;'>" . $econ_free . $econ . "<br/>" . $econ_main . " <a style='text-decoration: underline;' href='/customer-care/refunds-exchange-policy/'>Read More</a></div>");
//     }
//     
    if ($cart_total_price >= 651 & 'dawnwing_onx' === $method->id) {
        echo __("<div id='shipping_description' style='text-indent: 0px;font-size:12px;line-height: 1.5;'>" . $onx_extra . " <span class='discounted' style='text-decoration: line-through;'>R" . $onxcheck . "</span><br/>Next Day delivery to major centres (order before 13:30) <a style='text-decoration: underline;' href='/customer-care/refunds-exchange-policy/'>Read More</a></div>");
    }
}


if ( ! class_exists( 'Dawnwing_Live_Quote_API_Settings_Section' ) ) :

	/**
	 * Settings class.
	 */
	class Dawnwing_Live_Quote_API_Settings_Section {

		/**
		 * Constructor.
		 *
		 * @version 1.0.0
		 * @since   1.0.0
		 */
		public function __construct() {
			add_filter( 'woocommerce_get_sections_alg_wc_custom_order_numbers', array( $this, 'settings_section' ) );
		//	add_filter( 'woocommerce_get_settings_alg_wc_custom_order_numbers_' . $this->id, array( $this, 'get_settings' ), PHP_INT_MAX );
			add_action( 'init', array( $this, 'add_settings_hook' ) );
		}

		/**
		 * Add_settings_hook.
		 *
		 * @version 1.0.0
		 * @since   1.0.0
		 */
		public function add_settings_hook() {
			add_filter( 'alg_custom_order_numbers_settings_' . $this->id, array( $this, 'add_settings' ) );
		}

		/**
		 * Get_settings.
		 *
		 * @version 1.2.0
		 * @since   1.0.0
		 */
		/*
		public function get_settings() {
			return array_merge(
				apply_filters( 'alg_custom_order_numbers_settings_' . $this->id, array() ),
				array(
					array(
						'title' => __( 'Reset Settings', 'dawnwing-live-quote-api' ),
						'type'  => 'title',
						'id'    => 'alg_wc_custom_order_numbers_' . $this->id . '_reset_options',
					),
					array(
						'title'   => __( 'Reset section settings', 'dawnwing-live-quote-api' ),
						'desc'    => '<strong>' . __( 'Reset', 'dawnwing-live-quote-api' ) . '</strong>',
						'id'      => 'alg_wc_custom_order_numbers_' . $this->id . '_reset',
						'default' => 'no',
						'type'    => 'checkbox',
					),
					array(
						'type' => 'sectionend',
						'id'   => 'alg_wc_custom_order_numbers_' . $this->id . '_reset_options',
					),
				)
			);
		}

		/**
		 * Settings_section.
		 *
		 * @param array $sections - List of sections (ID & Desc).
		 * @version 1.0.0
		 * @since   1.0.0
		 */
		public function settings_section( $sections ) {
			$sections[ $this->id ] = $this->desc;
			return $sections;
		}

	}

endif;

/**
 * Dawnwing Live Quote API for WooCommerce - General Section Settings
 *
 * @version 1.2.3
 * @since   1.0.0
 * @author  Tyche Softwares
 * @package Custom-Order-Numbers-Lite
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

if ( ! class_exists( 'Dawnwing_Live_Quote_API_Settings_General' ) ) :

	/**
	 * General Settings.
	 */
	class Dawnwing_Live_Quote_API_Settings_General extends Dawnwing_Live_Quote_API_Settings_Section {

		/**
		 * Constructor.
		 *
		 * @version 1.0.0
		 * @since   1.0.
		 */
		public function __construct() {
			$this->id   = '';
			$this->desc = __( 'General', 'dawnwing-live-quote-api' );
			parent::__construct();
			add_action( 'admin_head', array( $this, 'add_tool_button_class_style' ) );
		}

		/**
		 * Add_tool_button_class_style.
		 *
		 * @version 1.0.0
		 * @since   1.0.0
		 */
		public function add_tool_button_class_style() {
			echo '<style type="text/css">';
			echo '#alg-tool-button { ';
			echo 'background: #ba0000; border-color: #aa0000; text-shadow: 0 -1px 1px #990000,1px 0 1px #990000,0 1px 1px #990000,-1px 0 1px #990000; box-shadow: 0 1px 0 #990000;';
			echo ' }';
			echo '</style>';
		}

		/**
		 * Add_settings.
		 *
		 * @param array $settings - Settings to be displayed.
		 *
		 * @version 1.2.3
		 * @since   1.0.0
		 * @todo    [dev] add `alg_wc_custom_order_numbers_counter_previous_order_date` as `hidden` field (for proper settings reset)
		 */
		public function add_settings( $settings ) {

			global $wp_roles;
			$user_roles          = array();
			$user_roles_no_admin = array();
			foreach ( apply_filters( 'editable_roles', ( isset( $wp_roles->roles ) ? $wp_roles->roles : array() ) ) as $role_key => $role ) {
				$user_roles[ $role_key ] = $role['name'];
				if ( ! in_array( $role_key, array( 'administrator', 'super_admin' ), true ) ) {
					$user_roles_no_admin[ $role_key ] = $role['name'];
				}
			}

			$settings = array_merge(
				array(
					array(
						'title' => __( 'Dawnwing Live Quote API Options', 'dawnwing-live-quote-api' ),
						'type'  => 'title',
						'desc'  => __( 'Settings for Dawnwing Live Quote API', 'dawnwing-live-quote-api' ),
						'id'    => 'alg_wc_custom_order_numbers_options',
					),
					array(
						'title'    => __( 'ONX Failed', 'dawnwing-live-quote-api' ),
						'desc_tip' => __( 'When there is no response from the API for Overnight Express this message will show', 'dawnwing-live-quote-api' ),
						'id'       => 'onx_fail',
						'default'  => '',
						'type'     => 'text',
					),
					array(
						'title'    => __( 'Overnight Express', 'dawnwing-live-quote-api' ),
						'desc_tip' => __( 'When there is no response from the API for Overnight Express this message will show', 'dawnwing-live-quote-api' ),
						'id'       => 'onx_main',
						'default'  => '',
						'type'     => 'text',
					),
					array(
						'title'    => __( 'Economy', 'dawnwing-live-quote-api' ),
						'desc_tip' => __( 'When there is no response from the API for Overnight Express this message will show', 'dawnwing-live-quote-api' ),
						'id'       => 'econ_main',
						'default'  => '',
						'type'     => 'text',
					),
					array(
						'title'    => __( 'Economy - Free Shipping', 'dawnwing-live-quote-api' ),
						'desc_tip' => __( 'When there is no response from the API for Overnight Express this message will show', 'dawnwing-live-quote-api' ),
						'id'       => 'econ_free',
						'default'  => '',
						'type'     => 'text',
					),
					array(
						'title'    => __( 'Fallback Shipping', 'dawnwing-live-quote-api' ),
						'desc_tip' => __( 'When there is no response from the API for shipping this message will show', 'dawnwing-live-quote-api' ),
						'id'       => 'fallback_shipping',
						'default'  => '',
						'type'     => 'text',
					),
					array(
						'title'    => __( 'Express After Free Shipping', 'dawnwing-live-quote-api' ),
						'desc_tip' => __( 'When there is no response from the API for Overnight Express this message will show', 'dawnwing-live-quote-api' ),
						'id'       => 'onx_extra',
						'default'  => '',
						'type'     => 'text',
					),
					array(
						'title'    => __( 'No Postcode Error', 'dawnwing-live-quote-api' ),
						'desc_tip' => __( 'When there is no response from the API this message will show', 'dawnwing-live-quote-api' ),
						'id'       => 'no_postcode',
						'default'  => '',
						'type'     => 'text',
					),
					
				),
				$settings
			);
			return $settings;
		}

	}
endif;
return new Dawnwing_Live_Quote_API_Settings_General();
