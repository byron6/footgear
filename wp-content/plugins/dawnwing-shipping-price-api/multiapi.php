<?php
global $pagenow, $current_page, $post;
if ($pagenow == 'post.php' && !empty($_GET['action']) && $_GET['action'] == 'edit' && $post->post_type == 'shop_order') {
} 
elseif ($pagenow == 'admin.php') {

}
elseif ($pagenow == 'post.php') {

}
elseif ($post->post_type == 'shop_coupon') {
}
else {
// Get an array of the current customer data stored in WC session
  $customer_data = (array) WC()->session->get('customer');
}
// Get the billing postcode
if (isset($customer_data['postcode'])) {
    $postcode = $customer_data['postcode'];
}

// Get the shipping postcode
if (isset($customer_data['shipping_postcode'])) {
    $postcode = $customer_data['shipping_postcode'];
}

$pickup_date_onx = date('Y-m-d', strtotime('+1 days'));
$pickup_date_econ = date('Y-m-d', strtotime('+2 days'));
$parcels = [
    "mass" => '2',
    "length"  => '10',
    "breadth"  => '10',
    "height" => '10',
    "units" => '1'
];


$onxapi = [
    "receiverSite" => "",
    "receiverProvince" => "Gauteng",
    "senderUnit" => "",
    "senderStreet" => "107 Andre Greyvenstein",
    "senderSite" => "",
    "senderProvince" => "Gauteng",
    "senderSuburb" => "JNB",
    "senderCity" => "Johannesburg",
    "senderPostCode" => "1619",
    "senderContact" => "Paul",
    "sender" => "Dawnwning",
    "senderCell" => "0871234567",
    "senderTel" => "000000000",
    "senderEmail" => "receiver@mail.co.za",
    "timeReady" => "08:00:00",
    "timeClosed" => "17:00:00",
    "receiverEmail" => "me@mail.co.za",
    "receiverTel" => "0987654321",
    "accountName" => "CPT4260",
    "accountNo" => "CPT4260",
    "collectionDate" => $pickup_date_onx,
    "surcharges" => null,
    "mass" => 10,
    "parcels" => [
        [
            "mass" => 10,
            "length" => 10,
            "breadth" => 10,
            "height" => 10,
            "units" => 1
        ]
    ],
    "service" => "ONX1",
    "insuranceRequired" => false,
    "insuranceValue" => 0,
    "cod" => 0,
    "deliveryType" => "D/D",
    "invoiceValue" => 0,
    "receiverUnit" => "1",
    "receiverStreet" => "12 State street",
    "receiverSuburb" => "Edenvale",
    "receiverCity" => "Johannesburg",
    "receiverPostCode" => $postcode,
    "receiverContact" => "Receiver Contact",
    "receiver" => "Peter",
    "receiverCell" => "0831234567",
];

$econapi = [
    "receiverSite" => "",
    "receiverProvince" => "Gauteng",
    "senderUnit" => "",
    "senderStreet" => "14 Fabriek Street",
    "senderSite" => "",
    "senderProvince" => "Western Cape",
    "senderSuburb" => "Industrail",
    "senderCity" => "Cape Town",
    "senderPostCode" => "7580",
    "senderContact" => "Paul",
    "sender" => "Dawnwning",
    "senderCell" => "0871234567",
    "senderTel" => "000000000",
    "senderEmail" => "receiver@mail.co.za",
    "timeReady" => "08:00:00",
    "timeClosed" => "17:00:00",
    "receiverEmail" => "me@mail.co.za",
    "receiverTel" => "0987654321",
    "accountName" => "CPT4260",
    "accountNo" => "CPT4260",
    "collectionDate" => $pickup_date_econ,
    "surcharges" => null,
    "mass" => 10,
    "parcels" => [
        [
            "mass" => 2,
            "length" => 14,
            "breadth" => 9,
            "height" => 5,
            "units" => 1
        ]
    ],
    "service" => "ECON",
    "insuranceRequired" => false,
    "insuranceValue" => 0,
    "cod" => 0,
    "deliveryType" => "D/D",
    "invoiceValue" => 0,
    "receiverUnit" => "1",
    "receiverStreet" => "12 State street",
    "receiverSuburb" => "Edenvale",
    "receiverCity" => "Johannesburg",
    "receiverPostCode" => $postcode,
    "receiverContact" => "Receiver Contact",
    "receiver" => "Peter",
    "receiverCell" => "0831234567",
];

$ch1 = curl_init();
$ch2 = curl_init();

curl_setopt_array($ch1, array(
    CURLOPT_URL => "https://swatws.dawnwing.co.za/dwwebservices/v2/live/api/Quotes/GetQuotes",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => '',
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => 'POST',
    CURLOPT_POSTFIELDS => json_encode($onxapi),
    CURLOPT_HTTPHEADER => array(
        'Content-Type: application/json',
        'Authorization: Bearer eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJJZCI6IjExMCIsImV4cCI6MTk1MjYwMjY5NSwiaXNzIjoiaHR0cDovLzQxLjAuNjkuMTk3LyIsImF1ZCI6Imh0dHA6Ly80MS4wLjY5LjE5Ny8ifQ.FtovsNtqjCf_sgPsuFeKcUb6ai6Jt_upw5oyRnaqcglAC4uKDBPIAfsacGTEnnZx_GOWWMHatg8LOn1sNazbkw',
    )
));

curl_setopt_array($ch2, array(
    CURLOPT_URL => "https://swatws.dawnwing.co.za/dwwebservices/v2/live/api/Quotes/GetQuotes",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => '',
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => 'POST',
    CURLOPT_POSTFIELDS => json_encode($econapi),
    CURLOPT_HTTPHEADER => array(
        'Content-Type: application/json',
        'Authorization: Bearer eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJJZCI6IjExMCIsImV4cCI6MTk1MjYwMjY5NSwiaXNzIjoiaHR0cDovLzQxLjAuNjkuMTk3LyIsImF1ZCI6Imh0dHA6Ly80MS4wLjY5LjE5Ny8ifQ.FtovsNtqjCf_sgPsuFeKcUb6ai6Jt_upw5oyRnaqcglAC4uKDBPIAfsacGTEnnZx_GOWWMHatg8LOn1sNazbkw',
    )
));

$mh = curl_multi_init();

//add the two handles
curl_multi_add_handle($mh, $ch1);
curl_multi_add_handle($mh, $ch2);

//execute the multi handle
do {
    $status = curl_multi_exec($mh, $active);
    if ($active) {
        curl_multi_select($mh);
    }
} while ($active && $status == CURLM_OK);

//close the handles
curl_multi_remove_handle($mh, $ch1);
curl_multi_remove_handle($mh, $ch2);
curl_multi_close($mh);

// all of our requests are done, we can now access the results
$response_1 = curl_multi_getcontent($ch1);
$response_2 = curl_multi_getcontent($ch2);

$arrayonx = json_decode($response_1, true);

$onxcheck = $arrayonx['response']["data"][0]["total"];
$onxerrorcheck = $arrayeonx["hasError"];
if ($onxcheck == "") {
    $onx = '200';
} else {
    $onx = $arrayonx['response']["data"][0]["total"];
}
$arrayecon = json_decode($response_2, true);
$econcheck = $arrayecon['response']["data"][0]["total"];
$econerrorcheck = $arrayecon["hasError"];
$econstatus = $arrayecon["status"];
if ($econcheck == "") {
    $econ = '100';
} 

elseif ($econerrorcheck == "1") {
$econ = '0';
}
elseif ($econstatus == "400") {
$econ = '0';
}
else {
    $econ = $arrayecon['response']["data"][0]["total"];
}

if ($econ == '100') {
    //$econdescription = "<br/>Fallback <a href='/customer-care/refunds-exchange-policy/'>Read More</a>";
} else {
  //  $econdescription = "<br/><a href='/customer-care/refunds-exchange-policy/'>Read More</a>";
}


if ($onx == '200') {
    $onxdescription = "<br/>Fallback <a href='/customer-care/refunds-exchange-policy/'>Read More</a>";
} else {
    $onxdescription = "<br/>Next Day delivery to major centres (order before 13:30) <a href='/customer-care/refunds-exchange-policy/'>Read More</a>";
}