<?php
/**
 * @link              https://www.semantica.co.za/
 * @since             1.0.0
 * @package           Footgear Product Galleries
 *
 * @wordpress-plugin
 * Plugin Name:       Footgear Product Galleries
 * Plugin URI:        https://www.semantica.co.za/
 * Description:       Custom product galleries for simple and variable products.
 * Version:           1.0.0
 * Author:            Semantica
 * Author URI:        https://www.semantica.co.za/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 */

define( 'FGGAL_PATH', plugin_dir_path( __FILE__ ) );

//Enque Scripts
function fg_gal_required_scripts() { 
    wp_register_style( 'fg_gal_css', plugins_url('css/fg-galleries.css', __FILE__) );
    wp_enqueue_style( 'fg_gal_css' );
	
	wp_register_script('fg_gal_jquery', plugins_url('js/fg-galleries.js', __FILE__),array('jquery'),'', true);
    wp_enqueue_script('fg_gal_jquery');

    wp_localize_script( 'fg_gal_jquery', 'fggal_ajax_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
}
add_action( 'wp_enqueue_scripts', 'fg_gal_required_scripts' );

//Change Default WC Gallery Template Path
function gerrg_get_template($located, $template_name)
{
	if( is_plugin_active( 'woo-variation-gallery/woo-variation-gallery.php' ) ) {
		if ('single-product/product-image.php' === $template_name)
		{
			return get_stylesheet_directory() . '/woocommerce/single-product/product-image.php';
		}
		
		return $located;
	}
}
add_filter('wc_get_template', 'gerrg_get_template', 99999, 2);

//Ajax Function to update gallery on variation select
function fg_variation_gal_funct(){

	echo json_encode(
		generate_prod_gallery(
			$_REQUEST['prodID'],
			$_REQUEST['fgColor'],
			$_REQUEST['fgSize'],
			$_REQUEST['SizeDefault']
		)
	);
	die();
}
add_action('wp_ajax_nopriv_fg_variation_gal', 'fg_variation_gal_funct');
add_action('wp_ajax_fg_variation_gal', 'fg_variation_gal_funct');

//Ajax Function to populate quick view
function fg_view_pop_funct(){
	
	$prodID = $_REQUEST['prodID'];

	echo '<div class="fg-quick-cont">'.do_shortcode('[product_page id="'.$prodID.'"]').'</div>';

	die();
}
add_action('wp_ajax_nopriv_fg_quick_view_pop', 'fg_view_pop_funct');
add_action('wp_ajax_fg_quick_view_pop', 'fg_view_pop_funct');

//Generate Product Gallery Output
function generate_prod_gallery($prodID,$prodColor,$prodSize,$sizeDefault){
	
	$html = '';
	$post_thumb_gal = array();
	$gal_classes = '';
	$gal_text = '';
	$cart_id = $prodID;
	
	$post_vid = get_post_meta($prodID, 'fg_vid_url');
	if(is_array($post_vid)){
		$post_vid = $post_vid[0];
	}
	$show_vid = false;
	
	$post_thumbs = get_post_meta($prodID, '_product_image_gallery');
	if(!empty($post_thumbs)){
		foreach($post_thumbs as $post_thumb){
			if(!empty($post_thumb)){
				$post_thumb_gal[0] = explode(',', $post_thumb);
			}
		}
	}
	
	$post_thumb_id = get_post_meta($prodID, '_thumbnail_id');
	
	//$html .= '<pre>'.print_r($galleryDefault, true).'</pre>';
	
	$atts_vals = array(
		'attribute_pa_color' => $prodColor,
		'attribute_pa_size' => $sizeDefault,
	);
	
	$selected = fg_find_variation($prodID, $atts_vals);
	
	if($selected != 0){
		$cart_id = $selected;
		$var_gallery = get_post_meta($selected, 'woo_variation_gallery_images');
		$var_thumb_id = get_post_meta($selected, '_thumbnail_id');
		$post_vidVar = get_post_meta($selected, 'fg_vid_url');
		$post_vidVar = $post_vidVar[0];
		if(isset($post_vidVar) && $post_vidVar !== ""){
			$post_vid = $post_vidVar;
		}
		if(!empty($var_gallery)){
			$post_thumb_gal = $var_gallery;
		}elseif(!empty($var_thumb_id)){
			foreach($var_thumb_id as $thumb_id){
				if($thumb_id != 0){
					$post_thumb_id = $var_thumb_id;
				}
			}
		}	
	}
	
	if(!empty($post_thumb_gal)){
		$cntr = 0;
		if(!empty($post_thumb_id)){
			$urlFeatured = wp_get_attachment_image_src( $post_thumb_id[0], 'full' );
			$gal_text .= '<div class="img"><a class="group" data-fancybox="gallery-'.$prodID.'" href="'.$urlFeatured[0].'" data-gal-id="'.$prodID.'" ><img name="" src="'.$urlFeatured[0].'" alt="" /></a></div>';
			$cntr = 1;
		}
		foreach($post_thumb_gal[0] as $img){
			if($cntr == 1){
				if(isset($post_vid) && !is_array($post_vid) && $post_vid != ''){
					$gal_text .= fg_add_vid($post_vid);
				}
			}
			$url = wp_get_attachment_image_src( $img, 'full' );
			$gal_text .= '<div class="img"><a class="group" data-fancybox="gallery-'.$prodID.'" href="'.$url[0].'" data-gal-id="'.$prodID.'" ><img name="" src="'.$url[0].'" alt="" /></a></div>';
			$cntr++;
		}
	}else if(!empty($post_thumb_id)){
		$url = wp_get_attachment_image_src( $post_thumb_id[0], 'full' );
		if(isset($post_vid) && !is_array($post_vid) && $post_vid != ''){
			$gal_text .= fg_add_vid($post_vid);
		}
		$gal_text .= '<div class="img"><a class="group" data-fancybox="gallery-'.$prodID.'" href="'.$url[0].'" data-gal-id="'.$prodID.'" ><img name="" src="'.$url[0].'" alt="" /></a></div>';
		$gal_classes = 'full';
	}else{
		$gal_text  .= '<div class="woocommerce-product-gallery__image--placeholder full">';
		if(isset($post_vid) && !is_array($post_vid) && $post_vid != ''){
			$gal_text .= fg_add_vid($post_vid);
		}else{
			$gal_text .= sprintf( '<img src="%s" alt="%s" class="wp-post-image" />', esc_url( wc_placeholder_img_src( 'woocommerce_single' ) ), esc_html__( 'Awaiting product image', 'woocommerce' ) );
			$gal_text .= '</div>';
		}
	}
	
	//$html .= '<pre>'.print_r($post_vid, true).'</pre>';
	
	$html .= '<div class="fg-custom-gal '.$gal_classes.'">';
	$html .= $gal_text;
	$html .= '</div>';
	
	$atts_vals = array(
		'attribute_pa_color' => $prodColor,
		'attribute_pa_size' => $prodSize,
	);
	
	$selected = fg_find_variation($prodID, $atts_vals);
	
	return array($html, $selected);
}

function fg_add_vid($post_vid){
	$ext = array_pop(explode('.', $post_vid));
	return '
	<div class="img vid">
		<video autoplay="" loop="" playsinline="" class="u-full-width u-full-height" tabindex="-1" muted="" >
			<source src="'.$post_vid.'" type="video/'.$ext.'">
		</video>
	</div>
	';
}

function fg_find_variation($product_id, $attributes) {
    $product_data_store = new WC_Product_Data_Store_CPT();
    $product = new WC_Product($product_id);

    return $product_data_store->find_matching_product_variation($product, $attributes);
}

/*Custom Video URL*/ 
function fg_vid_group() {
	
	echo '<div class="option_group">';
	
	woocommerce_wp_text_input( array(
		'id'                => 'fg_vid_url',
		'value'             => get_post_meta( get_the_ID(), 'fg_vid_url', true ),
		'label'             => 'Video URL',
		'desc_tip'    		=> true,
		'description'       => 'Inser either Youtube, Vimeo or Library URL.'
	) );
	
	echo '</div>';
}
add_action( 'woocommerce_product_options_general_product_data', 'fg_vid_group' );

function fg_save_video( $id, $post ){
 
	if( !empty( $_POST['fg_vid_url'] ) ) {
		update_post_meta( $id, 'fg_vid_url', $_POST['fg_vid_url'] );
	} else {
		delete_post_meta( $id, 'fg_vid_url' );
	}
 
}
add_action( 'woocommerce_process_product_meta', 'fg_save_video', 10, 2 );

/*Custom Video URL - Variations*/
function variation_video_field( $loop, $variation_data, $variation ) {
	
	woocommerce_wp_text_input( 
		array( 
			'id'          => 'fg_vid_url[' . $variation->ID . ']', 
			'label'       => __( 'Video URL', 'woocommerce' ),
			'value'       => get_post_meta( $variation->ID, 'fg_vid_url', true ),
			'desc_tip'    => true,
			'description' => 'Inser either Youtube, Vimeo or Library URL' 
		)
	);

}

function save_variation_video_field( $post_id ) {
	
	if( !empty( $_POST['fg_vid_url'][ $post_id ] ) ) {
		update_post_meta( $post_id, 'fg_vid_url', $_POST['fg_vid_url'][ $post_id ] );
	} else {
		delete_post_meta( $post_id, 'fg_vid_url' );
	}

}
//add_action( 'woocommerce_product_after_variable_attributes', 'variation_video_field', 10, 3 );
//add_action( 'woocommerce_save_product_variation', 'save_variation_video_field', 10, 2 );

function fg_variation_select(){
	global $product;
	
	if( $product->is_type('variable') ){
		$prodID = $product->id;
		
		$default_color = false;
		$default_size = false;
		$default_attributes = $product->get_default_attributes();
		if(!empty($default_attributes)){
			if(isset($default_attributes['pa_color'])){
				$default_color = $default_attributes['pa_color'];
			}
			if(isset($default_attributes['pa_size'])){
				$default_size = $default_attributes['pa_size'];
			}
		}
		
		$variations = $product->get_available_variations();
		$colors = array();
		$sizes = array();
		$check = array();
		$sizes_check = array();
		$color_txt = '<div class="fg-swatch">';
		$sizes_txt = '<div class="size-cont">';
		$attSizeName = $attSlug = ''; 
		
		if(current_user_can( 'administrator')){
			//echo '<pre>'.print_r($variations, true).'</pre>';
		}
		
		foreach ( $variations as $variation ){

			foreach($variation['attributes'] as $index => $value){
				$attSizeName = $index;
			}
			
			$attSlug = str_replace('attribute_', '',$attSizeName);
			
			$color = $variation['attributes']['attribute_pa_color'];
			$size = $variation['attributes'][$attSizeName];
			$gallery = $variation['variation_gallery_images'];
			$avail = $variation['availability_html'];
			
			if($gallery){
				$img = $gallery[0]['gallery_thumbnail_src'];
			}
			
			if(!in_array($color, $check)){
				if(isset($img)){
					$colors[] = array(
						'color' => $color,
						'img' => $img
					);
				}
				$check[] = $color;
			}
			
			if(!in_array($size, $sizes_check)){
				$sizes[] = array(
					'size' => $size,
					'avail' => $avail
				);
				$sizes_check[] = $size;
			}else{
				foreach($sizes as $key => $value)
				{ 
					if($value['size'] == $size){
						$value['avail'] = $avail;
					}
				}
			}
		}
		
		$colorDefault = false;
		
		if(!empty($colors)){
			$colors_cnt = 0;
			$checked = '';
			foreach($colors as $color){
				if($default_color){
					if($color['color'] == $default_color){
						$checked = ' checked="checked"';
					}
					$colorDefault = $default_color;
				}else{
					if($colors_cnt == 0){
						$checked = ' checked="checked"';
						$colorDefault = $color['color'];
					}
				}
				if($color['color'] != ''){
					$color_txt .= '<input type="radio" id="pa_color_'.$color['color'].'" name="attribute_pa_color" data-attribute_name="'.$attSizeName.'" value="'.$color['color'].'"'.$checked.'><label for="pa_color_'.$color['color'].'"><img class="fg-arch-thumb" data-arch-thumb="0" name="" src="'.$color['img'].'" alt="" /></label>';
				}
				$colors_cnt++;
				$checked = '';
			}
		}
		
		$returnVals = get_selected_sizes($product, $colorDefault);
		$sizes_txt .= $returnVals[0];
		
		$color_txt .= '</div>';
		$sizes_txt .= '</div>';
		
		echo '<div class="fg-variations">';
		echo $color_txt;
		echo $sizes_txt;
		echo '<div class="fg-stock-cont">'.$returnVals[2].'</div>';
		echo '</div>';
	}

}
add_action('woocommerce_before_variations_form', 'fg_variation_select', 10);

function get_selected_sizes($product, $colorDefault){
	$sizes_txt = '';
	$minStock = get_option('yith-wcbm-low-stock-qty');
	$default_size = false;
	$default_attributes = $product->get_default_attributes();
	if(!empty($default_attributes)){
		if(isset($default_attributes['pa_size'])){
			$default_size = $default_attributes['pa_size'];
		}
	}
	$variations = $product->get_available_variations();
	$sizes_check = array();
	$sizes = array();
	$sizeNames = array();
	$returnQty = 0;
	$checked = '';
	$attSizeName = $attSlug = '';
	
	foreach ( $variations as $variation ){
		
		foreach($variation['attributes'] as $index => $value){
			$attSizeName = $index;
		}
		
		$attSlug = str_replace('attribute_', '',$attSizeName);
		
		$term = get_term_by('slug', $variation['attributes'][$attSizeName], $attSlug);
		$avail = 1;
		$color = $variation['attributes']['attribute_pa_color'];
		$size = $variation['attributes'][$attSizeName];
		$qty = $variation['max_qty'];
		
		if(current_user_can( 'administrator')){
			//echo '<pre>'.print_r($variation, true).'</pre>';
		}
		
		if(preg_match("~\b0 in stock\b~",$variation['availability_html'])){
			$avail = 0;
		}else{
			if(preg_match("~\bOut of stock\b~",$variation['availability_html'])){
				$avail = 0;
			}
		}
		
		if($color == $colorDefault || !$colorDefault || !$color){
			if(!in_array($size, $sizes_check)){
				$sizes[] = array(
					'id' => $term->term_id,
					'size' => $size,
					'avail' => $avail,
					'name' => $term->name,
					'qty' => $qty
				);
				$sizes_check[] = $size;
			}else{
				foreach($sizes as $key => $value)
				{ 
					if($value['size'] == $size){
						$value['avail'] = $avail;
					}
				}
			}
		}
	}
	
	$checked = false;
	
	if(!empty($sizes)){
		foreach ($sizes as $key => $row)
		{
			$termSize = str_replace('uk-', '', $row['size']);
			$sizeNames[$key] = (float)$termSize;
		}
	}
	
	array_multisort($sizeNames, SORT_ASC, $sizes);
	foreach($sizes as $key=>$size){
		if(isset($size['qty']) && (float)$size['qty'] > 0){
			$sizes[$key]['checked'] = true;
			break;
		}
	}
	
	//echo '<pre>'.print_r($sizes, true).'</pre>';
	
	if(!empty($sizes)){
		$colors_cnt = 0;
		foreach($sizes as $size){
			$disabled = '';
			if($default_size){
				if($size['size'] == $default_size && $size['avail'] != 0){
					$checked = ' checked="checked"';
					if($returnQty == 0){
						$returnQty = (float)$size['qty'];
					}
				}
			}else{
				if($size['avail'] != 0 && isset($size['checked'])){
					$checked = ' checked="checked"';
					if($returnQty == 0){
						$returnQty = (float)$size['qty'];
					}
				}
			}

			if($size['avail'] == 0){
				$disabled = 'disabled';
			}
			
			$sizes_txt .= '<input type="radio" data-qty="'.$size['qty'].'" id="'.$attSlug.'_'.$size['size'].'" name="'.$attSizeName.'" data-min-stock="'.$minStock.'" value="'.$size['size'].'"'.$checked.'><label for="'.$attSlug.'_'.$size['size'].'" class="'.$disabled.'">'.$size['name'].'</label>';
			$colors_cnt++;
			$checked = '';
		}
	}
	
	//echo '<pre>'.print_r($returnQty, true).'</pre>';
	
	if($returnQty == 0){
		$retunTxt = '<div class="fg-stock-count loading">Checking Stock</div>';
	}else{
		if((int)$returnQty <= (int)$minStock){
			$returnQty = $returnQty.' ';
		}else{
			$returnQty = '';
		}
		$retunTxt = '<div class="fg-stock-count">'.$returnQty.'In Stock.</div>';
	}
	
	return array($sizes_txt, $returnQty, $retunTxt);
}