jQuery( document ).ready(function($) {
	
	$('.fg-custom-gal').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		adaptiveHeight: false,
		mobileFirst: true,
		responsive: [
			{
				  breakpoint: 921,
				  settings: 'unslick'
			}
		]
	});
	
	$('.cross-sells .products').slick({
		slidesToShow: 4,
		slidesToScroll: 1,
		infinite: true,
		autoplay: false,
		dots: false
	});
	
	if($('.fg-widget-product-slider').length){
		var $current = $(this).closest('.elementor-element');
		var $elems = $('.elementor-element');
		
		var $previous = $elems.eq($elems.index($current) - 1);
		var myElement = $previous.children('.elementor-heading-title h2');
		
		console.log(myElement);
		
		$('.fg-widget-product-slider ul').each(function( index ) {
			//var title = $(this).closest('.elementor-element').find('.elementor-heading-title');
			//console.log(title.html());
			console.log('Slides: ' + $(this).children('li').length);
			var slidesToShow = 4;
			if($(this).children('li').length < 4){
				slidesToShow = $(this).children('li').length;
			}
			if($(this).children('li').length > 0){
				$(this).slick({
					slidesToShow: slidesToShow,
					slidesToScroll: 1,
					arrows: true,
					dots: false,
					autoplay: false,
					infinite: true,
					prevArrow: '<div class="fg-prev"><i class="fas fa-chevron-left"></i></div>',
					nextArrow: '<div class="fg-next"><i class="fas fa-chevron-right"></i></div>',
					responsive: [
					{
					  breakpoint: 980,
					  settings: {
						slidesToShow: 3,
						slidesToScroll: 1,
						infinite: true,
						dots: false,
						arrows: true,
						autoplay: false,
					  }
					},
					{
					  breakpoint: 767,
					  settings: {
						slidesToShow: 1,
						slidesToScroll: 1,
						infinite: true,
						dots: false,
						arrows: true,
						autoplay: false,
					  }
					}
					// You can unslick at a given breakpoint now by adding:
					// settings: "unslick"
					// instead of a settings object
				  ]
				});
			}
		});
	}
	
	$(document).on('click',  '.fg-prod-quick .single_add_to_cart_button' , function(e) {
		$('.fg-prod-quick').addClass('loading-gallery');
	});	
	
	$(document).on('click',  '.fg-pop-trig' , function(e) {
		var id = $(this).data('prod-id');
		if($('.fg-prod-quick').length){
			jQuery.ajax({
				url:fggal_ajax_object.ajax_url,
				data:{ 
				  action: 'fg_quick_view_pop',
				  prodID: id
				},
				beforeSend:function(){
					$('.fg-prod-quick').addClass('loading-gallery');
				},
				success:function(data){
					//var ouput = JSON.parse(data);
					$('.fg-prod-quick').html(data);
					
					$('.fg-custom-gal').slick({
						slidesToShow: 1,
						slidesToScroll: 1,
					});
					
					$('.fg-prod-quick').removeClass('loading-gallery');
					
					fgCheckVariations();
					
					//$variation_form = $('.fg-prod-quick form.variations_form');
					//$('.fg-prod-quick form.variations_form input[name="attribute_pa_size"]').trigger('change');
					
					//fgDropSelect('#pa_size', 'yes');
					//fgDropSelect('#pa_color', 'yes');
				}
			});
		}
	});
	
	if($("#pa_mens").length || $("#pa_womens").length || $("#pa_kids").length){
		
		var attName = $(this).attr('id');
		
		var value = '';
		$("#" + attName + " > option").each(function() {
			var opt = $(this).attr('data-in-stock');
			if(opt == '1'){
				value = $(this).val();
				return false;
			}
		});
		if(value != ''){
			$('#' + attName).val(value);
			$('#' + attName).trigger('change');
			fgCheckVariations();
		}
	}
	
	if($("#pa_color").length){
		var opt = $("#pa_color").val();
		
		if(opt == ''){
			var values = $("#pa_color>option").map(function() { return $(this).val(); });
			$('#pa_color').val(values[1]);
			$('#pa_color').trigger('change');
		}
	}
	
	function fgDropSelect(dropdown, modal){
		if($(dropdown).length){
			var opt = $(dropdown).val();
			
			if(opt == '' || modal == 'yes'){
				var values = $(dropdown+">option").map(function() { return $(this).val(); });
				$(dropdown).trigger('change');
			}
		}
	}
	
	$(document).on('change',  '.fg-swatch input[name=attribute_pa_color]' , function(e) {
		$('#pa_color').val($(this).val());
		$('#pa_color').val($(this).val()).trigger('change');
		//$('#pa_color').trigger('change');
		
		var prodID = $('.variations_form').attr('data-product_id');
		var prodColor = $(this).val();
		var prodSize = 0;
		var fgSizeDefault = 0;
		var attClick = 0;
		
		var values = $("#pa_size>option").map(function() { return $(this).val(); });
		
		prodSize = fgSizeDefault = values[1];
		
		gal_ajax(prodID, prodColor, prodSize, attClick, fgSizeDefault);
	});
	
	$(document).on('change',  '.size-cont input[name*="attribute_"]' , function(e) {
		var attInput = $(this).attr('name').replace('attribute_','');
		console.log(attInput);
		$('.fg-stock-count').html('Checking Stock');
		var id = $(this).attr('id');
		var minStock = $(this).attr('data-min-stock');
		var stock = $(this).attr('data-qty');
		if($("label[for='"+id+"']").hasClass('disabled')){
			$('.single_add_to_cart_button').addClass('disabled');
		}else{
			$('.single_add_to_cart_button').removeClass('disabled');
		}
		
		$('#' + attInput).val($(this).val());
		$('#' + attInput).val($(this).val()).trigger('change');
		
		$('.single_add_to_cart_button').addClass('loading');
		$('.fg-stock-count').addClass('loading');
		setTimeout(function(){
			$('.single_add_to_cart_button').removeClass('loading');
			$('.fg-stock-count').removeClass('loading');
			fg_stock_update(stock, minStock);
		}, 2500);
		//$('#pa_size').trigger('change');
	});
	
	function gal_ajax(prodID, prodColor, prodSize, attClick, fgSizeDefault){
		jQuery.ajax({
			url:fggal_ajax_object.ajax_url,
			data:{ 
			  action: 'fg_variation_gal',
			  prodID: prodID,
			  fgColor: prodColor,
			  fgSize: prodSize,
			  SizeDefault: fgSizeDefault
			},
			beforeSend:function(){
				$('.woo-variation-gallery-wrapper').addClass('loading-gallery');
			},
			success:function(data){
				var ouput = JSON.parse(data);
				
				if($('.woocommerce-product-gallery__wrapper').length){
					$('.woocommerce-product-gallery__wrapper').html(ouput[0]);
					
					if($('.fg-prod-quick').length){
						$('.fg-custom-gal').slick({
							slidesToShow: 1,
							slidesToScroll: 1,
						});
					}
					
					$('.woo-variation-gallery-wrapper').removeClass('loading-gallery');
				}
	
			}
		});
	}
	
	function fg_stock_update(stock, minStock){
		var stockTxt = '';
		if(stock && stock != 0){
			if(stock <= minStock){
				stockTxt = stock + ' In Stock.';
			}else{
				stockTxt = 'In Stock.';
			}
		}else{
			stockTxt = '<span>Out of stock.</span>';
		}
		$('.fg-stock-count').html(stockTxt);
	}
	
	function fgCheckVariations(){
		if($('.fg-stock-count').length){
			var stock = 0;
			var attName = '';
			
			if($('input[name="attribute_pa_mens"]').length){
				attName = 'pa_mens';
			}else if($('input[name="attribute_pa_womens"]').length){
				attName = 'pa_womens';
			}else if($('input[name="attribute_pa_kids"]').length){
				attName = 'pa_kids';
			}
			
			$('input[name="attribute_'+attName+'"]').each(function( index ) {
				var minStock = $(this).attr('data-min-stock');
				if($(this).is(':checked')){
					stock = $(this).attr('data-qty');
				}
			});
			setTimeout(function(){
				$('.fg-stock-count').removeClass('loading');
				fg_stock_update(stock, minStock);
			}, 2500);
		}
	}
	
});