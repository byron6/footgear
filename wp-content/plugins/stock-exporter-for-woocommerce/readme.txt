=== Stock Exporter for WooCommerce ===
Contributors: webdados
Tags: woocommerce, ecommerce, e-commerce, stock, webdados
Author URI: https://www.webdados.pt
Plugin URI: https://www.webdados.pt/wordpress/plugins/exportacao-stock-woocommerce-wordpress/
Requires at least: 4.4
Tested up to: 5.7
Requires PHP: 5.6
Stable tag: 0.8.2

Export a simple CSV file report with the current WooCommerce products stock.

== Description ==

This plugin allows you to export a simple report, in a CSV file, with all the products, or only the ones where stock is managed, and its current stock on WooCommerce.
The file is UTF-16, comma-separated and the values are enclosed in double quotes.

= Features: =

* Generates a simple CSV file report with the current WooCommerce products stock;
* It's also possible to see the report as a HTML table directly on the plugin's admin page;
* WPML compatible;

== Installation ==

* Use the included automatic install feature on your WordPress admin panel and search for “WooCommerce Stock Exporter”.
* Go to WooCoomerce > Stock Exporter and click on “Export WooCommerce Stock” to generate the report.

== Frequently Asked Questions ==

= I need help, can I get technical support? =

This is a free plugin. It’s our way of giving back to the wonderful WordPress community.

There’s a support tab on the top of this page, where you can ask the community for help. We’ll try to keep an eye on the forums but we cannot promise to answer support tickets.

If you reach us by email or any other direct contact means, we’ll assume you are in need of urgent, premium, and of course, paid-for support.

= When using the "HTML table on new window" output option how can I format the table? =

Use the `wse_screen_new_header` action, with a priority higher than 10, to add your own CSS. Example here: https://gist.github.com/webdados/574704ecf877a18a8923af78e61f821b

You can also add any HTML using the `wse_screen_new_header` and `wse_screen_new_footer` filters. We are using priority 10 to add the HTML file structure, so you should use a value higher than 10 for the header and lower than 10 for the footer.

= Can this plugin also...? =

Nop! WooCoomerce Stock Report on a CSV file or HTML table. That's it.

== Changelog ==

= 0.8.2 - 2021-03-10 =
* Tested with WordPress 5.8-alpha-50516 and WooCommerce 5.1.0

= 0.8.1 =
* Bugfix: show categories for variations
* Technical support clarification
* Tested with WordPress 5.5-beta4-48649 and WooCommerce 4.3.1

= 0.8.0 =
* New `wse_sort_field` filter to set the sort field for the exported products (defaults to "product" title)
* Tested with WordPress 5.5-alpha-47923 and WooCommerce 4.3.0-rc.2

= 0.7.2 =
* Tested with WooCommerce 4.0.1

= 0.7.1 =
* Changes on the InvoiceXpress banner
* Tested with WordPress 5.3.3-alpha-46995 and WooCommerce 3.9.0-rc.2

= 0.7 =
* Hide the InvoiceXpress nag if the invoicing is already installed and active
* Tested with WordPress 5.3.1-alpha-46798 and WooCommerce 3.8.1

= 0.6 =
* Change InvoiceXpress nag interval from 30 to 90 days
* Tested with WordPress 5.2.4-alpha-46074 and WooCommerce 3.8.0-beta.1
* Requires PHP 5.6

= 0.5 =
* New output option to be able to export as an HTML table on a new clean window
* Exclude products by stock (more and less than value)
* Exclude products by meta key (equal to value)
* ID, SKU and product type fields are now optional
* Better performance when choosing to export only products with managed stock
* New field: image (thumbnail)
* Better coding standards
* Tested with WooCommerce 3.5.2

= 0.4.2.1 =
* Tested with WooCommerce 3.3
* Bumped `Tested up to` tag

= 0.4.2 =
* Fixed a PHP notice due to WooCommerce 3.0 changes
* Removed the translation files from the plugin `lang` folder (the translations are now managed on WordPress.org's GlotPress tool and will be automatically downloaded from there)
* Tested with WooCommerce 3.2
* Added `WC tested up to` tag on the plugin main file
* Bumped `Tested up to` tag

= 0.4.1 =
* Fixes a bug introduced on 0.4 that wouldn't allow to export on WooCommerce older than 3.0

= 0.4 =
* Tested and adapted to work with WooCommerce 3.0.0-rc.2
* New WC_Product_Stock_Exporter and WC_Product_Variation_Stock_Exporter classes (extends WC_Product and WC_Product_Variation) to be used by the plugin to get and product details
* Added ID to the list of fixed fields to export
* Changed all product/variation and categories separator to `|`
* Bumped `Tested up to` tag

= 0.3.1 =
* Bumped `Tested up to` and `Requires at least` tags

= 0.3 =
* Release date: 2016-05-11
* You can now choose aditional fields to include on the report: Categories, Regular Price and any custom field from any plugin
* The options are saved to be used as default next time
* Update sponsored by ideiahomedesign.pt


= 0.2 =
* Release date: 2016-04-05
* Added the product price to the report
* readme.txt fixes

= 0.1.1 =
* Release date: 2015-11-19
* Fix: Translations were not loaded correctly
* Plugin URI added to readme.txt

= 0.1 =
* Release date: 2015-11-19
* Initial release, sponsored by ideiahomedesign.pt