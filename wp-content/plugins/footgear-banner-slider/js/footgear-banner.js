jQuery( document ).ready(function() {
	
	if(jQuery('#ribbon-clock').length){	
		jQuery('.fg-ribbon').addClass('mobi-counter');
	}

	jQuery('.fg-ribbon').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
  		arrows: true,
		dots: false,
		autoplay: true,
		autoplaySpeed: 4000,
		infinite: true,
		prevArrow: '<div class="fg-prev"><i class="fas fa-chevron-left"></i></div>',
		nextArrow: '<div class="fg-next"><i class="fas fa-chevron-right"></i></div>'
	});
	
	jQuery('.fg-ribbon').css({
		height: 'auto',
		visibility: 'visible'
	});
	
	if(jQuery('.ribbon-entry').length){			  
		jQuery( '.ribbon-entry' ).each(function( index ) {
			var clock = '';
			if(jQuery(this).find('#ribbon-clock').length !== 0){
				console.log(jQuery('#ribbon-clock', this).attr('data-id'));
				clock = jQuery('#ribbon-clock', this);

				var endDate = clock.attr('data-end');
				var convert = endDate.substring(5, 7) + '/' + endDate.substring(8, 10) + '/' + endDate.substring(0, 4) + ' ' + endDate.substring(11);

				// Set the date we're counting down to
				var countDownDate = new Date(convert).getTime();
				console.log(countDownDate);
			
				// Update the count down every 1 second
				var x = setInterval(function() {
				
				  // Get today's date and time
				  var now = new Date().getTime();
					
				  // Find the distance between now and the count down date
				  var distance = countDownDate - now;
					
				  // Time calculations for days, hours, minutes and seconds
				  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
				  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
				  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
				  var seconds = Math.floor((distance % (1000 * 60)) / 1000);
					
				  // Output the result in an element with id="demo"
				  //clock.html(days + "d " + hours + "h " + minutes + "m " + seconds + "s ");
				  clock.html("<div class='digit'><span>DAYS</span><span class='numbers'>" + days + "</span></div><div class='digit'><span>HRS</span><span class='numbers'>" + hours + "</span></div><div class='digit'><span>MIN</span><span class='numbers'>" + minutes + "</span></div><div class='digit'><span>SEC</span><span class='numbers'>" + seconds + "</span></div>");
				  //document.getElementById("ribbon-clock").innerHTML = days + "d " + hours + "h "
				  //+ minutes + "m " + seconds + "s ";
					
				  // If the count down is over, write some text 
				  if (distance < 0) {
					clearInterval(x);
					clock.html("EXPIRED");
				  }
				}, 1000);
			}
		
		});	
	}
	
});