<?php
/**
 * @link              https://www.semantica.co.za/
 * @since             1.0.0
 * @package           Footgear Banner Slider
 *
 * @wordpress-plugin
 * Plugin Name:       Footgear Banner Slider
 * Plugin URI:        https://www.semantica.co.za/
 * Description:       Header advert banner slider with countdown timer.
 * Version:           1.0.0
 * Author:            Semantica
 * Author URI:        https://www.semantica.co.za/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 */

define( 'FGBANNER_PATH', plugin_dir_path( __FILE__ ) );

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@ Enque Scripts
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
function fg_banner_required_scripts() { 
    wp_register_style( 'fg_banner_css', plugins_url('css/footgear-banner.css', __FILE__) );
    wp_enqueue_style( 'fg_banner_css' );
	
	wp_register_script('fg_banner_jquery', plugins_url('js/footgear-banner.js', __FILE__),array('jquery'),'', true);
    wp_enqueue_script('fg_banner_jquery');
	
	$handle = 'slick.min.js';
	$list = 'enqueued';
	if (wp_script_is( $handle, $list )) {
		return;
	} else {
		wp_register_script( 'fg_banner_slick', plugin_dir_url(__FILE__).'js/slick.min.js');
		wp_enqueue_script( 'fg_banner_slick' );
	}
	
	//wp_register_script('fg_banner_countdown_jquery', plugins_url('js/countdown.js', __FILE__),array('jquery'),'', false);
    //wp_enqueue_script('fg_banner_countdown_jquery');
	
	//wp_register_script('fg_banner_countdown_min_jquery', plugins_url('js/jquery.countdown.min.js', __FILE__),array('jquery'),'', false);
    //wp_enqueue_script('fg_banner_countdown_min_jquery');

    wp_localize_script( 'fg_banner_jquery', 'fg_banner_ajax_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
}
add_action( 'wp_enqueue_scripts', 'fg_banner_required_scripts' );

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@ Includes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
require_once ( FGBANNER_PATH . '/admin/postTypes.php');

//Footgear Ribbon - Ad Slider
function fg_ribbon_funct($atts){
	$html = '';
	
	$options = shortcode_atts( array(
		'count' => -1
	), $atts );
	
	$count = $options['count'];
	
	$args = array(
        'post_type'   => 'ribbon-ads',
        'post_status' => 'publish',
		'order'   => 'ASC',
        'posts_per_page' => $count
    );
	
    $the_query = new WP_Query( $args );	
	
	if( $the_query->have_posts() ){
		
		$now = date("Y-m-d H:i:s", strtotime('+2 hours'));
		
		$html .= '<div class="fg-ribbon">';
		
		foreach($the_query->posts as $portfolio) {
			$id = $portfolio->ID;
			$label = get_field('adv_label', $id);
			$start = get_field('adv_start', $id);
			$end = get_field('adv_end', $id);
			$countdown = get_field('adv_countdown', $id);
			$countdown_pos = get_field('adv_countdown_pos', $id);
			$link = get_field('adv_link', $id);
			$link_style = get_field('adv_link_style', $id);
			$link_pos = get_field('adv_link_pos', $id);
			
			$clock = '';
			$eLink = '';
			$bLink = '';
			$eLinkEnds = '';
			$class = '';
			
			if($now < $end && $start <= $now){
			
				if($countdown == 'yes'){
					$clock = '<div id="ribbon-clock" class="'.$countdown_pos.'" data-end="'.$end.'" data-id="'.$id.'"></div>';
				}
				
				if(isset($link) && $link != ''){
					if($link_style == 'element'){
						$eLink = '<a href="'.$link.'">';
						$eLinkEnds = '</a>';
					}else{
						if($link_style == 'button'){
							$class = 'class="button"';
						}
						$bLink = '<div class="ribbon-link '.$link_pos.'"><a href="'.$link.'" '.$class.'>Learn More</a></div>';
					}
				}
				
				$html .= '<div class="ribbon-entry"><div class="ribbon-content">'.$eLink.$clock.'<div class="ribbon-txt">'.$label.'</div>'.$bLink.$eLinkEnds.'</div></div>';		
			}
		}
		
		$html .= '</div>';
		
	}
	
	return $html;
}
add_shortcode('fg-ribbon', 'fg_ribbon_funct');