<?php
function fg_cpt_funct() {
	
	register_post_type( 'ribbon-ads',
		array(
			'label' => __('Ribbon Ads'),
			'singular_label' => __('Ribbon Ad'),
			'exclude_from_search' => false,
			'public' => true,
			'show_ui' => true,
			'menu_icon' => 'dashicons-admin-post',
			'has_archive' => true,
			'hierarchical' => true,
			'show_in_nav_menus' => true,
			'rewrite' => array('slug' => 'ribbon-ads'),
			'supports' => array(
				'title',
				'revisions',
				'page-attributes',
				'custom-fields'
			),
        	'show_in_rest' => true
		)
	);
	
}
add_action('init', 'fg_cpt_funct');
?>