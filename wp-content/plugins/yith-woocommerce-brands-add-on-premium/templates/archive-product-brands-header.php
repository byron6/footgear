<?php
/**
 * Brand header banner.
 *
 * @author  Your Inspiration Themes
 *
 * @package YITH WooCommerce Brands
 * @version 1.0.0
 */

if ( ! defined( 'YITH_WCBR' ) ) {
	exit;
} // Exit if accessed directly

global $product;
?>

<?php if ( ! empty( $banner ) ) : ?>

<div class="yith-wcbr-brands-header-wrapper">
	<?php echo $banner; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
</div>

<?php endif; ?>
