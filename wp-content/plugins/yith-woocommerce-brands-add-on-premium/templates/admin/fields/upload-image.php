<?php
/**
 * Field to upload image.
 *
 * @author  Your Inspiration Themes
 *
 * @package YITH WooCommerce Brands
 * @version 1.0.0
 */

if ( ! defined( 'YITH_WCBR' ) ) {
	exit;
} // Exit if accessed directly
?>

<tr valign="top">
	<th scope="row">
		<label for="<?php echo esc_attr( $id ); ?>"><?php echo esc_attr( $name ); ?></label>
	</th>
	<td class="forminp forminp-upload-image">
		<div id="<?php echo esc_attr( $id ); ?>_preview" style="float:left;margin-right:10px;"><img src="<?php echo esc_html( $image ); ?>" width="60px" height="60px" /></div>
		<div style="line-height:60px;">
			<input type="hidden" id="<?php echo esc_attr( $id ); ?>" class="yith_wcbr_upload_image_id" name="<?php echo esc_attr( $id ); ?>" value="<?php echo esc_attr( $image_id ); ?>" />
			<button type="button" id="<?php echo esc_attr( $id ); ?>_upload" class="yith_wcbr_upload_image_button button"><?php esc_html_e( 'Upload/Add image', 'yith-woocommerce-brands-add-on' ); ?></button>
			<button type="button" id="<?php echo esc_attr( $id ); ?>_remove" class="yith_wcbr_remove_image_button button"><?php esc_html_e( 'Remove image', 'yith-woocommerce-brands-add-on' ); ?></button>
		</div>
	</td>
</tr>
