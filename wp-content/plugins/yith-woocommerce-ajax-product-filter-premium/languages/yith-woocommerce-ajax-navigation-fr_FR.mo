��    K      t  e   �      `  #   a     �     �  Y   �            
   4     ?     P     j     r          �     �     �  
   �  '   �     �     �     �     �  1        H  6   Q     �  2   �  2   �     �     	     	     	  0   %	     V	     [	     j	     m	     y	  
   �	     �	  %   �	  +   �	     �	     
     
     +
     =
     C
  
   K
  K   V
  L   �
     �
            s   *     �     �     �     �     �     �       
             &     ,     2  !   ?  (   a  '   �  z   �     -     E     W  $   \  �  �  5   /     e     �  Y   �     �        
        %  !   @     b     k     �     �     �     �     �  1   �               $     +  9   J     �  4   �  	   �  	   �  	   �     �     �  
        &  =   2     p     v     �     �     �     �     �     �     �  	   �     �     �     �       	   !     +     >     Q     m     �     �  �   �  +   P  +   |  '   �     �  
   �     �     �     �     
     %     +     2  C   G     �     �  u   �          3     O     S           B   7          G       D   3                5          J   F   2   =   <      0   :             '   /      ;   $          8              H   #   +              K                  A         -   %      .                    ?       "          E   )   &   ,   1   	          6   @           !   C   >       9   
   (   *                  4           I                    "See all categories/tags" link text Add new range Admin: Section titleTag List Admin: user noteNote: tags with no products assigned will not be showed in the front end All (hierarchical) All (no hierarchical) Attribute: Background color Background color on hover BiColor Border color Border color on hover Brand Button Label CSS custom class Categories Choose how to sort WooCommerce products Closed Color Count Display (default All): Display on sale and in stock WooCommerce products Dropdown Filter the list of products without reloading the page Filters: For multicolor: I.E. white and red T-ShirtColor 1 For multicolor: I.E. white and red T-ShirtColor 2 Hide Selected Hide product count Label Labels Leave it empty to use the default text available List No tags found. OR Only Parent Opened Parent tag Price Range Price filter option: price ends toTo Price filter option: price starts fromFrom Product sortingSort by Product sortingStock/On sale Query Type: Reset All Filters Round See all Select all Select if you want to show round color box or square color boxColor Style: Select this if you want to show the widget as open or closedDropdown style: Show "In Stock" filter Show "On Sale" filter Show Selected Show a price filter widget with a list of preset price ranges that users can use to better narrow down the products Show only "In Stock" products Show only "On Sale" products Show widget dropdown Square Tag Tag name Term Text color Text color on hover Title Type: Unselect all Use custom style for reset button [FRONTEND] On sale filter labelIn stock [FRONTEND] On sale filter labelOn sale [Plugin Name]YITH WooCommerce Ajax Product Filter is enabled but not effective. It requires WooCommerce in order to work. current categories text current tags text here refer to: product pricePrice Filter Project-Id-Version: YITH WooCommerce Ajax Product Filter
Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/yith-woocommerce-ajax-product-filter-premium
POT-Creation-Date: 2021-05-18 10:07:49+00:00
PO-Revision-Date: 2021-03-30 20:26:41+0000
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Generator: GlotPress/3.0.0-alpha.2
 Texte du lien "Voir toutes les catégories / balises" Ajouter une nouvelle gamme Liste de balises Remarque: les balises sans produits attribués ne seront pas affichées dans le front-end Tout (hiérarchique) Tout (pas de hiérarchie) Attribut : Couleur de l'arrière plan Couleur d'arrière-plan au survol Bicolore Couleur de la bordure Couleur de la bordure au survol Marque Étiquette du bouton Classe personnalisée CSS Catégories Choisissez comment trier les produits WooCommerce Fermé Couleur Nombre Affichage (tout par défaut) : Afficher les produits en solde et en stock de WooCommerce Menu déroulant Filtrer la liste des produits sans recharger la page Filtres : Couleur 1 Couleur 2 Masquer la sélection Masquer le nombre de produits Étiquette Étiquettes Laissez-le vide pour utiliser le texte par défaut disponible Liste Aucune balise trouvée. OU Parent seulement Ouvert Étiquette parent Gamme de prix à De  Trier par Stock/En solde Type de requête : Réinitialiser tous les filtres Rond Voir tout Tout sélectionner Style de couleur : Style de liste déroulante: Afficher le filtre "En stock" Afficher le filtre "En solde" Afficher la sélection Afficher un widget de filtre de prix avec une gamme de prix prédéfinies que les utilisateurs peuvent utiliser pour mieux affiner les produits Afficher uniquement les produits "En stock" Afficher uniquement les produits "En solde" Afficher la liste déroulante du widget Carré Étiquette Nom de la balise Terme Couleur du texte Couleur du texte au survol Titre Type : Tout déselectionner Utiliser un style personnalisé pour le bouton de réinitialisation En stock En promo Le filtre de produit YITH WooCommerce Ajax est activé mais pas efficace. Il nécessite WooCommerce pour fonctionner. texte des catégories actuelles texte des balises actuelles ici Filtre de prix 