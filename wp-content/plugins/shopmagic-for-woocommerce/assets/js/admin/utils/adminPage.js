export function isAutomationListPage() {
  const currentPageParams = new URLSearchParams(window.location.search);
  return (currentPageParams.get('post_type') === 'shopmagic_automation')
    && (currentPageParams.get('page') === null)
    && (window.location.pathname.includes('edit.php'));
}

export function isAutomationEditPage() {
  return document.getElementById('_shopmagic_edit_page') !== null;
}
