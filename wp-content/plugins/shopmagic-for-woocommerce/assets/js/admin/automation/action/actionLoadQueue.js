/**
 * When the action is changed on frontend, trigger UI update.
 */
export default class ActionLoadQueue {
  loadLock = false;

  actionLoadQueue = [];

  /**
   * @param {Event} e
   */
  actionChange = (e) => {
    e.stopPropagation();
    const currentId = jQuery(e.target).parent().find('.action_number').text() - 1;

    this.putInQueue(currentId, e.target);
    this.checkQueue();
  }

  /**
   * @param {number} currentId
   * @param {HTMLElement} control
   */
  putInQueue(currentId, control) {
    if (currentId < 0) return;
    document.querySelector(`#action-area-${currentId} .spinner`).classList.add('is-active');
    document.querySelector(`#action-area-${currentId} .error-icon`).classList.remove('error-icon-visible');

    this.actionLoadQueue.push({
      id: currentId,
      obj: control,
    });
  }

  checkQueue() {
    if (this.loadLock === false && this.actionLoadQueue.length > 0) {
      const descriptor = this.actionLoadQueue.shift();
      this.actionLoad(descriptor.id, descriptor.obj);
    }
  }

  /**
   *
   * @param {number} currentId
   * @param {HTMLElement} control
   * @returns {Promise<void>}
   */
  async actionLoad(currentId, control) {
    const self = control;
    if (jQuery(self).val().length) {
      this.loadLock = true;
      try {
        const response = await fetch(ShopMagic.ajaxurl, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
          },
          body: new URLSearchParams({
            action: 'shopmagic_load_action_params',
            action_slug: jQuery(self).val(),
            action_id: currentId,
            post: jQuery('#post_ID').val(),
            editor_initialized: window.SM_EditorInitialized === true,
            paramProcessNonce: ShopMagic.paramProcessNonce,
          }),
        });

        if (response.ok) {
          const params = await response.json();
          jQuery(`#action-config-area-${currentId}`).html(params.action_box);// .tinymce_textareas();
        }
      } catch (e) {
        document.querySelector(`#action-area-${currentId} .error-icon`).classList.add('error-icon-visible');
      } finally {
        document.querySelector(`#action-area-${currentId} .spinner`).classList.remove('is-active');
        this.loadLock = false;
        this.checkQueue();
      }
    }
  }
}
