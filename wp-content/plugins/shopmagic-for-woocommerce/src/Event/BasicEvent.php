<?php

namespace WPDesk\ShopMagic\Event;

use Psr\Container\ContainerInterface;
use WPDesk\ShopMagic\Automation\Automation;
use WPDesk\ShopMagic\DataSharing\DataLayer;
use WPDesk\ShopMagic\Filter\FilterLogic;

/**
 * ShopMagic Events Base class.
 *
 * @package WPDesk\ShopMagic\Event
 */
abstract class BasicEvent implements Event {

	/** @var Automation */
	protected $automation;

	/** @var FilterLogic */
	protected $filter;

	/** @var ContainerInterface */
	protected $fields_data;

	public function __clone() {
		$this->automation  = null;
		$this->filter      = null;
		$this->fields_data = null;
	}

	public function update_fields_data( ContainerInterface $data ) {
		$this->fields_data = $data;
	}

	public function get_provided_data_domains() {
		return [];
	}

	public function get_fields() {
		return [];
	}

	public function get_provided_data() {
		return [];
	}

	public function set_automation( Automation $automation ) {
		$this->automation = $automation;
	}

	public function set_filter_logic( FilterLogic $filter ) {
		$this->filter = $filter;
	}

	/**
	 * Run registered actions from automation
	 *
	 * @since 1.0.0
	 */
	protected function run_actions() {
		$this->filter->set_provided_data( ( new DataLayer( $this, [ $this->automation ] ) )->get_provided_data() );
		if ( apply_filters( 'shopmagic/core/event/filter_passed', $this->filter->passed(), $this ) ) {
			$this->automation->event_fired( $this );
		}
	}

	/**
	 * Returns the description of the current Event
	 *
	 * @since   1.0.4
	 */
	public function get_description() {
		return __( 'No description provided for this event.', 'shopmagic-for-woocommerce' );
	}

	public function jsonSerialize() {
		return [];
	}

	public function set_from_json( array $serializedJson ) { // phpcs:ignore WordPress.NamingConventions.ValidVariableName.VariableNotSnakeCase
		// nothing to do here...
	}
}
