<?php

namespace WPDesk\ShopMagic\Automation;

use ShopMagicVendor\WPDesk\Persistence\Adapter\ArrayContainer;
use WPDesk\ShopMagic\Action\Action;
use WPDesk\ShopMagic\Action\ActionFactory2;
use WPDesk\ShopMagic\ActionExecution\ExecutionStrategyFactory;
use WPDesk\ShopMagic\AutomationOutcome\OutcomeReposistory;
use WPDesk\ShopMagic\Event\Event;
use WPDesk\ShopMagic\Event\EventFactory2;
use WPDesk\ShopMagic\Event\NullEvent;
use WPDesk\ShopMagic\Filter\FilterFactory2;
use WPDesk\ShopMagic\Filter\FilterLogic;

/**
 * @package WPDesk\ShopMagic\Automation
 */
final class Automation implements \JsonSerializable {

	/** @var int */
	private $id;

	/** @var Event|null */
	private $event;

	/** @var AutomationPersistence */
	private $persistence;

	/** @var EventFactory2 */
	private $event_factory;

	/** @var FilterFactory2 */
	private $filter_factory;

	/** @var ActionFactory2 */
	private $action_factory;

	/** @var ExecutionStrategyFactory */
	private $execution_factory;

	public function __construct(
		$automation_id,
		EventFactory2 $event_factory,
		FilterFactory2 $filter_factory,
		ActionFactory2 $action_factory,
		ExecutionStrategyFactory $execution_factory
	) {
		$this->id                = (int) $automation_id;
		$this->event_factory     = $event_factory;
		$this->filter_factory    = $filter_factory;
		$this->action_factory    = $action_factory;
		$this->execution_factory = $execution_factory;
		$this->persistence       = new AutomationPersistence( $this->id );
	}

	public function get_name(): string {
		$post = get_post( $this->id );
		if ( ! is_null( $post ) ) {
			return $post->post_title;
		}
		return '';
	}

	/**
	 * @return void
	 */
	public function initialize() {
		/** @noinspection NullPointerExceptionInspection False positive in $automation->get_event. */
		$this->get_event()->initialize();
	}

	/**
	 * Has real event and at least one action.
	 */
	public function is_fully_configured(): bool {
		return ! $this->get_event() instanceof NullEvent && count( $this->get_actions() ) > 0;
	}

	public function get_id(): int {
		return $this->id;
	}

	/**
	 * @return Event
	 */
	public function get_event() {
		if ( $this->event === null ) {
			$filter = $this->create_filter();

			$this->event = $this->event_factory->create_event( $this->persistence->get_event_slug(), $this, $filter );
			$this->event->update_fields_data( new ArrayContainer( $this->persistence->get_event_data() ) );
		}

		return $this->event;
	}

	/**
	 * @return FilterLogic
	 */
	private function create_filter() {
		return $this->persistence->get_filters_as_group( $this->filter_factory );
	}

	/**
	 * Event should run this method when is fired.
	 *
	 * @param Event $event
	 * @return void
	 */
	public function event_fired( Event $event ) {
		global $wpdb;
		$outcome_repository = new OutcomeReposistory( $wpdb );
		foreach ( $this->get_actions() as $index => $action ) {
			do_action( 'shopmagic/core/automation/event_fired', $this, $event, $action );
			if ( apply_filters( 'shopmagic/core/automation/should_execute_action', true, $this, $event, $action ) ) {
				$unique_id = (string) $outcome_repository->prepare_for_outcome( $this, $event, $action, $index );
				$strategy  = $this->execution_factory->create_strategy( $this, $event, $action, $index );
				$strategy->execute( $this, $event, $action, $index, $unique_id );
			}
		}
	}

	/**
	 * @return Action[]
	 */
	public function get_actions(): array {
		$actions = [];
		foreach ( $this->persistence->get_actions_data() as $action_data ) {
			$actions[] = $this->action_factory->create_action(
				$action_data['_action'],
				new ArrayContainer( $action_data )
			);
		}

		return $actions;
	}

	public function has_action( int $index ): bool {
		return isset( $this->get_actions()[ $index ] );
	}

	public function get_action( int $index ): Action {
		return $this->get_actions()[ $index ];
	}

	/**
	 * @return int[]
	 */
	public function jsonSerialize(): array {
		return [
			'id' => $this->id,
		];
	}
}
