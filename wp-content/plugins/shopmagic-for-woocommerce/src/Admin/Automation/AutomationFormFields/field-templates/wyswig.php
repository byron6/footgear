<?php
/**
 * @var \ShopMagicVendor\WPDesk\Forms\Field $field
 * @var string $name_prefix
 * @var string $value
 */
?>
<?php wp_print_styles( 'media-views' ); ?>

<script>
	window.SM_EditorInitialized = true;
</script>

<tr class="shopmagic-field">
	<td class="shopmagic-label">
		<label for="message_text"><?php echo esc_html( $field->get_label() ); ?></label>

		<p class="content">
		<?php
		esc_html_e(
			'Copy and paste placeholders (including double brackets) from the metabox on the right to personalize.',
			'shopmagic-for-woocommerce'
		);
		?>
				</p>
	</td>

	<td class="shopmagic-input">
		<?php
		$editor_id       = uniqid( 'wyswig_' );
		$editor_settings = [
			'textarea_name' => esc_attr( $name_prefix ) . '[' . esc_attr( $field->get_name() ) . ']',
		];

		$allowed_tags = '<a><p><h1><h2><h3><h4><br><img><style><span><b><i><em><strong><div><ul><ol><li><hr><u>';
		wp_editor( strip_tags( $value, $allowed_tags ), $editor_id, $editor_settings );
		?>
		<script type="text/javascript">
			(function () {
				ShopMagic.wyswig.init('<?php echo esc_attr( $editor_id ); ?>');
			}());
		</script>

	</td>
</tr>
