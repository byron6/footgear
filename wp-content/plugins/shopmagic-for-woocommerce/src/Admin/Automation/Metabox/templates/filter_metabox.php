<?php
/**
 * @var string $nonce_action
 * @var string $nonce_name
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>

<div id="filters">
	<?php wp_nonce_field( $nonce_action, $nonce_name ); ?>
	<div id="filter-group-area"></div>
	<div id="filter-config-area" class="config-area shopmagic-tfoot">
		<div class="shopmagic-fr">
			<button id="add-filter-group" style="display: none" class="button button-primary button-large" type="button">
				<?php esc_html_e( '+ New Filter Group', 'shopmagic-for-woocommerce' ); ?>
			</button>
		</div>
	</div>
</div>
