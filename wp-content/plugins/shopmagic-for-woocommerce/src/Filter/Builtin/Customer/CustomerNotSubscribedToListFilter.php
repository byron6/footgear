<?php
declare(strict_types=1);

namespace WPDesk\ShopMagic\Filter\Builtin\Customer;

use WPDesk\ShopMagic\CommunicationList\CommunicationList;
use WPDesk\ShopMagic\CommunicationList\CommunicationListRepository;
use WPDesk\ShopMagic\Filter\Builtin\CustomerFilter;
use WPDesk\ShopMagic\Filter\ComparisionType\SelectManyToManyType;
use WPDesk\ShopMagic\Placeholder\Builtin\Customer\CustomerEmail;

final class CustomerNotSubscribedToListFilter extends CustomerFilter {

	public function get_name(): string {
		return __( 'Customer - Not Subscribed to List', 'shopmagic-for-woocommerce' );
	}

	public function passed(): bool {
		$customer_email_placeholder = new CustomerEmail();
		$customer_email_placeholder->set_provided_data( $this->provided_data );
		$lists_ids = CommunicationListRepository::get_email_optout_lists_ids( $customer_email_placeholder->value( [] ) );

		return $this->get_type()->passed(
			$this->fields_data->get( SelectManyToManyType::VALUE_KEY ),
			$this->fields_data->get( SelectManyToManyType::CONDITION_KEY ),
			$lists_ids
		);
	}

	protected function get_type(): SelectManyToManyType {
		return new SelectManyToManyType( CommunicationListRepository::get_lists_as_select_options() );
	}

}
