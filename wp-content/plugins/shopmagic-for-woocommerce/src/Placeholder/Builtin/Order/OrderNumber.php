<?php

namespace WPDesk\ShopMagic\Placeholder\Builtin\Order;

use WPDesk\ShopMagic\Placeholder\Builtin\WooCommerceOrderBasedPlaceholder;

final class OrderNumber extends WooCommerceOrderBasedPlaceholder {

	public function get_slug(): string {
		return parent::get_slug() . '.order_number';
	}

	/** @param string[] $parameters */
	public function value( array $parameters ): string {
		if ( $this->is_order_provided() ) {
			$order = $this->get_order();
			if ( $order instanceof \WC_Order ) {
				return $order->get_order_number();
			}
		}

		return '';
	}
}
