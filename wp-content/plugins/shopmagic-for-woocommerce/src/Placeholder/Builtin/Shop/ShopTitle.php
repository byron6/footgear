<?php


namespace WPDesk\ShopMagic\Placeholder\Builtin\Shop;

use WPDesk\ShopMagic\Placeholder\BasicPlaceholder;

final class ShopTitle extends BasicPlaceholder {

	public function get_slug(): string {
		return parent::get_slug() . '.title';
	}

	public function get_required_data_domains(): array {
		return array();
	}

	public function value( array $parameters ): string {
		return get_bloginfo( 'name' );
	}
}
