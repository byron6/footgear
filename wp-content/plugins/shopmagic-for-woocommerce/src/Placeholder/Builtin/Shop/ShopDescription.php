<?php


namespace WPDesk\ShopMagic\Placeholder\Builtin\Shop;

use WPDesk\ShopMagic\Placeholder\BasicPlaceholder;

final class ShopDescription extends BasicPlaceholder {

	public function get_slug(): string {
		return parent::get_slug() . '.tagline';
	}

	public function get_required_data_domains(): array {
		return array();
	}

	public function value( array $parameters ): string {
		return get_bloginfo( 'description' );
	}
}
