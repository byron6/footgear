<?php

namespace WPDesk\ShopMagic\Placeholder\Builtin\Customer;

use WPDesk\ShopMagic\CommunicationList\CommunicationListRepository;
use WPDesk\ShopMagic\FormField\Field\SelectField;
use WPDesk\ShopMagic\Frontend\ListsOnAccount;
use WPDesk\ShopMagic\Placeholder\Builtin\UserBasedPlaceholder;
use WPDesk\ShopMagic\Placeholder\Helper\PlaceholderUTMBuilder;

final class CustomerUnsubscribeUrl extends UserBasedPlaceholder {

	/** @var PlaceholderUTMBuilder */
	private $utm_builder;

	public function __construct() {
		$this->utm_builder = new PlaceholderUTMBuilder();
	}

	public function get_slug(): string {
		return parent::get_slug() . '.unsubscribe_url';
	}

	public function get_supported_parameters(): array {
		return array_merge(
			$this->utm_builder->get_utm_fields(),
			[
				( new SelectField() )
					->set_name( 'list' )
					->set_label( __( 'List', 'shopmagic-for-woocommerce' ) )
					->set_options( CommunicationListRepository::get_lists_as_select_options() )
					->set_placeholder( __( 'Select...', 'shopmagic-for-woocommerce' ) ),
			]
		);
	}

	public function get_description(): string {
		return $this->utm_builder->get_description();
	}

	public function value( array $parameters ): string {
		$email_placeholder = new CustomerEmail();
		$email_placeholder->set_provided_data( $this->provided_data );

		$list = $parameters['list'] ?? null;

		$unsubscribe_url = ListsOnAccount::get_unsubscribe_url( $email_placeholder->value( [] ), $list );

		return $this->utm_builder->append_utm_parameters_to_uri( $parameters, $unsubscribe_url );
	}
}
