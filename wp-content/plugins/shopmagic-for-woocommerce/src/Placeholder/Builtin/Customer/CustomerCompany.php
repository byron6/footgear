<?php


namespace WPDesk\ShopMagic\Placeholder\Builtin\Customer;

use WPDesk\ShopMagic\Placeholder\Builtin\UserBasedPlaceholder;

/**
 * @todo Create new Customer implementation - Company
 */
final class CustomerCompany extends UserBasedPlaceholder {

	public function get_slug(): string {
		return parent::get_slug() . '.company';
	}

	public function get_required_data_domains(): array {
		$data_domains   = parent::get_required_data_domains();
		$data_domains[] = \WC_Order::class;
		return $data_domains;
	}


	public function value( array $parameters ): string {
		if ( $this->is_order_provided() ) {
			return $this->get_order()->get_billing_company();
		}
		return '';
	}
}
