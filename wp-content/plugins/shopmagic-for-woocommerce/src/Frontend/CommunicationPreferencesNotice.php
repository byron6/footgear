<?php

declare(strict_types=1);

namespace WPDesk\ShopMagic\Frontend;

use ShopMagicVendor\WPDesk\Notice\Notice;
use ShopMagicVendor\WPDesk\Notice\PermanentDismissibleNotice;

/**
 * Show notice if user have deleted Communication Preferences page from WP.
 *
 * @package WPDesk\ShopMagic\Frontend
 */
final class CommunicationPreferencesNotice {

	const NOTICE_NAME = 'shopmagic_create_communication_page';

	/**
	 * @return void
	 */
	public function show_message() {
		new PermanentDismissibleNotice(
			$this->get_message(),
			self::NOTICE_NAME,
			Notice::NOTICE_TYPE_WARNING
		);
	}

	private function get_message(): string {
		// It seem that ShopMagic's **Communication Preferences** page is missing.
		// You and your customers won't be able to manage your Lists, and use placeholders like `{{ customer.unsubscribe_url }}`.
		// Add the page using the following link - Create page.
		return __( 'It seems that ShopMagic\'s', 'shopmagic-for-woocommerce' ) . ' ' .
			'<b>' . __( 'Communication Preferences', 'shopmagic-for-woocommerce' ) . '</b> ' . __( 'page is missing', 'shopmagic-for-woocommerce' ) . '. ' .
			__( 'You and your customers won\'t be able to manage your Lists, and use placeholders like', 'shopmagic-for-woocommerce' ) . ' ' .
			'<code>' . __( '{{ customer.unsubscribe_url }}', 'shopmagic-for-woocommerce' ) . '</code> ' .
			__( 'Add the page using the following link', 'shopmagic-for-woocommerce' ) . ' - ' .
			'<br>' .
			'<a href="' . $this->get_post_url() . '">' . esc_html__( 'Create page', 'shopmagic-for-woocommerce' ) . '</a>';
	}

	private function get_post_url(): string {
		global $pagenow;
		return add_query_arg(
			[
				'action'   => self::NOTICE_NAME,
				'nonce'    => wp_create_nonce( self::NOTICE_NAME ),
				'redirect' => $pagenow,
			],
			admin_url( 'admin-post.php' )
		);
	}

}
