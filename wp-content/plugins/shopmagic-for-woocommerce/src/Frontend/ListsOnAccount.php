<?php

namespace WPDesk\ShopMagic\Frontend;

use ShopMagicVendor\WPDesk\View\Renderer\SimplePhpRenderer;
use ShopMagicVendor\WPDesk\View\Resolver\DirResolver;
use WPDesk\ShopMagic\CommunicationList\CommunicationListRepository;
use WPDesk\ShopMagic\Optin\EmailOptRepository;

/**
 * Communication type info for customer. Optins/optouts.
 *
 * @package WPDesk\ShopMagic\Frontend
 */
final class ListsOnAccount {
	const ACCOUNT_SHORTCODE = 'shopmagic_communication_preferences';

	/** @param int|null $list_id */
	public static function get_unsubscribe_url( string $email, $list_id ): string {
		$hash = md5( $email . SECURE_AUTH_SALT );

		$link = get_permalink( get_option( CommunicationPreferencesPage::ACCOUNT_PAGE_ID_OPTION_KEY ) );

		if ( empty( $link ) || empty( $email ) ) {
			return '';
		}

		return $link . ( strpos( $link, '?' ) !== false ? '&' : '?' ) . http_build_query(
			[
				'email' => $email,
				'hash'  => $hash,
			]
		);
	}

	public static function validate_unsubscribe_hash( string $email, string $hash ): bool {
		return md5( $email . SECURE_AUTH_SALT ) === $hash;
	}

	/** @return void */
	public function hooks() {
		add_shortcode( self::ACCOUNT_SHORTCODE, [ $this, 'communication_preferences_shortcode' ] );
		if ( apply_filters( 'shopmagic/core/communication_type/account_page_show', true ) ) {
			add_filter( 'woocommerce_account_menu_items', [ $this, 'new_menu_items' ] );
			add_action( 'woocommerce_account_' . $this->get_slug() . '_endpoint', [ $this, 'nav_menu_content' ] );
			add_action( 'init', [ $this, 'add_rewrite_endpoint' ] );
			add_filter( 'query_vars', [ $this, 'add_query_vars' ], 0 );
		}
	}

	/**
	 * @return void
	 * @internal
	 */
	public function add_rewrite_endpoint() {
		add_rewrite_endpoint( $this->get_slug(), EP_PAGES );
	}

	/**
	 * @param string[] $vars
	 *
	 * @return string[]
	 *
	 * @internal
	 */
	public function add_query_vars( array $vars ): array {
		$vars[] = $this->get_slug();

		return $vars;
	}

	private function get_slug(): string {
		return apply_filters( 'shopmagic/core/communication_type/account_page_slug', 'communication-preferences' );
	}

	/**
	 * Insert the new endpoint into the My Account menu.
	 *
	 * @param string[] $items
	 *
	 * @return string[]
	 *
	 * @internal
	 */
	public function new_menu_items( array $items ): array {
		$logout_item = false;

		if ( isset( $items['customer-logout'] ) ) {
			$logout_item = $items['customer-logout'];
			unset( $items['customer-logout'] );
		}

		$items[ $this->get_slug() ] = $this->get_title();

		if ( $logout_item ) {
			$items['customer-logout'] = $logout_item;
		}

		return $items;
	}

	private function get_title(): string {
		return apply_filters( 'shopmagic/core/communication_type/account_page_title', __( 'Communication', 'shopmagic-for-woocommerce' ) );
	}

	/**
	 * @return void
	 * @internal WooCommerce communication preferences callback.
	 */
	public function nav_menu_content() {
		echo $this->communication_preferences_shortcode(); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
	}

	/** @internal This is a shortcode. Do not use outside the class. */
	public function communication_preferences_shortcode(): string {
		global $wpdb;

		$email = $this->get_email_for_shortcode();
		if ( empty( $email ) ) {
			return '';
		}
		// phpcs:disable WordPress.Security.NonceVerification
		if ( isset( $_POST['email'] ) ) {
			$sanitized_post = array_map( 'sanitize_text_field', $_POST );
			$this->save_opt_changes( $email, $sanitized_post );
			wc_print_notice( __( 'Your communication preferences have been updated.', 'shopmagic-for-woocommerce' ) );
		}

		$renderer = new FrontRenderer();
		$opt_repo = new EmailOptRepository( $wpdb );
		$ct_repo  = new CommunicationListRepository( $wpdb );
		$types    = $ct_repo->get_account_communication_types();

		return $renderer->render(
			'communication_preferences',
			[
				'email'    => $email,
				'hash'     => isset( $_REQUEST['hash'] ) ? sanitize_text_field( wp_unslash( $_REQUEST['hash'] ) ) : null,
				'types'    => $types,
				'renderer' => $renderer,
				'opt_ins'  => $opt_repo->find_by_email( $email ),
			]
		);
		// phpcs:enable
	}

	/**
	 * Returns email or redirects.
	 */
	private function get_email_for_shortcode(): string {
		if ( ! is_user_logged_in() ) {
			// phpcs:disable WordPress.Security.NonceVerification.Recommended
			$email = isset( $_REQUEST['email'] ) ? sanitize_email( wp_unslash( $_REQUEST['email'] ) ) : '';
			$hash  = isset( $_REQUEST['hash'] ) ? sanitize_text_field( wp_unslash( $_REQUEST['hash'] ) ) : '';
			if ( self::validate_unsubscribe_hash( $email, $hash ) ) {
				return $email;
			}
			// phpcs:enable

			return '';

		}

		return wp_get_current_user()->user_email;
	}

	/**
	 * @param string $email
	 * @param string[] $request
	 *
	 * @return void
	 */
	private function save_opt_changes( string $email, array $request ) {
		global $wpdb;
		$opt_repo = new EmailOptRepository( $wpdb );
		$ct_repo  = new CommunicationListRepository( $wpdb );
		$optins   = $opt_repo->find_by_email( $email );
		$types    = $ct_repo->get_account_communication_types();
		foreach ( $types as $type ) {
			if ( isset( $request['shopmagic_optin'][ $type->get_id() ] ) && $request['shopmagic_optin'][ $type->get_id() ] === 'yes' ) {
				if ( ! $optins->is_opted_in( $type->get_id() ) ) {
					$opt_repo->opt_in( $email, $type->get_id() );
				}
			} elseif ( ! $optins->is_opted_out( $type->get_id() ) ) {
				$opt_repo->opt_out( $email, $type->get_id() );
			}
		}
	}

}
