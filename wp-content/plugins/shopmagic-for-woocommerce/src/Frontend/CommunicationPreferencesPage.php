<?php

declare(strict_types=1);

namespace WPDesk\ShopMagic\Frontend;

/**
 * Makes sure that communication preferences page exists on WP instance.
 *
 * @package WPDesk\ShopMagic\Frontend
 */
final class CommunicationPreferencesPage {
	const ACCOUNT_PAGE_ID_OPTION_KEY = 'shopmagic_communication_account_page_id';

	/** @var CommunicationPreferencesNotice */
	private $notice;

	public function __construct( CommunicationPreferencesNotice $notice ) {
		$this->notice = $notice;
	}

	/** @return void */
	public function hooks() {
		add_action( 'admin_notices', [ $this, 'notice_on_non_existing_page' ] );
		add_action( 'admin_post_' . CommunicationPreferencesNotice::NOTICE_NAME, [ $this, 'maybe_create_communication_page_from_post' ] );
	}

	/** @return void */
	public function notice_on_non_existing_page() {
		if ( ! $this->has_communication_page() ) {
			$this->notice->show_message();
		}
	}

	/** @return void */
	public function maybe_create_communication_page() {
		if ( ! $this->has_communication_page() && did_action( 'wp_loaded' ) ) {
			$this->create_communication_page();
		}
	}

	/** @return void */
	public function maybe_create_communication_page_from_post() {
		$nonce = isset( $_REQUEST['nonce'] ) ? sanitize_text_field( wp_unslash( $_REQUEST['nonce'] ) ) : '';
		if ( current_user_can( 'publish_pages' ) &&
			wp_verify_nonce( $nonce, CommunicationPreferencesNotice::NOTICE_NAME ) ) {
			$this->maybe_create_communication_page();
			wp_safe_redirect( isset( $_REQUEST['redirect'] ) ? esc_url_raw( wp_unslash( $_REQUEST['redirect'] ) ) : admin_url() );
			exit;
		}
		wp_die( esc_html__( 'You are not allowed to access this area.', 'shopmagic-for-woocommerce' ) );
	}

	private function has_communication_page(): bool {
		$page_id = get_option( self::ACCOUNT_PAGE_ID_OPTION_KEY );
		return (bool) get_post( $page_id );
	}

	/** @return void */
	private function create_communication_page() {
		$page_id = wp_insert_post(
			[
				'post_name'      => __( 'communication-preferences', 'shopmagic-for-woocommerce' ),
				'post_title'     => __( 'Communication preferences', 'shopmagic-for-woocommerce' ),
				'post_content'   => '[' . ListsOnAccount::ACCOUNT_SHORTCODE . ']',
				'post_status'    => 'publish',
				'post_type'      => 'page',
				'post_author'    => 1,
				'comment_status' => 'closed',
			]
		);
		update_option( self::ACCOUNT_PAGE_ID_OPTION_KEY, $page_id );
	}

}
