<?php
/**
 * Single Product title
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/title.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see        https://docs.woocommerce.com/document/template-structure/
 * @package    WooCommerce\Templates
 * @version    1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

global $product;
$product_id = $product->get_id();

$stock_status = $product->get_stock_status();

$brandID = '';
$myvals = get_post_meta($product_id);
foreach($myvals as $key=>$val)
{
	if($key == '_primary_term_yith_product_brand'){
		$brandID = $val[0];
	}
}

$thumbnail_id = get_term_meta( $brandID, 'thumbnail_id', true );
$thumbnail_url = wp_get_attachment_image_src( $thumbnail_id );

echo do_shortcode('[yith_wcbr_product_brand show_logo="yes" show_title="no" product_id="'.$product_id.'"]');

the_title( '<h1 class="product_title entry-title">', '</h1>' );

$badges = '';
$new = false;

$newness_days = (int)get_option('yith-wcbm-badge-newer-than');
$created = strtotime( $product->get_date_created() );
if ( ( time() - ( 60 * 60 * 24 * $newness_days ) ) < $created ) {
	$new = true;
}

//Get Badges at product level
$myvals = get_post_meta($product_id, '_yith_wcbm_product_meta');
$myvals = $myvals[0]['id_badge'];

$badgeTitles = [];
if(isset($myvals)  && !empty($myvals)){
	foreach($myvals as $myval){
		$badgeTitles[] = get_the_title($myval);
	}
}

//New
if($new || in_array('New', $badgeTitles)){
	$badges .= '
	<div class="yith-wcbm-badge yith-wcbm-badge-1194 yith-wcbm-badge--on-product-'.$product_id.' yith-wcbm-badge--anchor-point-top-left yith-wcbm-badge-css yith-wcbm-css-badge-1194" data-position="{&quot;top&quot;:&quot;30&quot;,&quot;bottom&quot;:&quot;auto&quot;,&quot;left&quot;:&quot;-8&quot;,&quot;right&quot;:&quot;auto&quot;}">
			<div class="yith-wcbm-badge__wrap">
				<div class="yith-wcbm-css-s1"></div>
				<div class="yith-wcbm-css-s2"></div>
				<div class="yith-wcbm-css-text">
					<div class="yith-wcbm-badge-text">New In</div>
				</div>
			</div><!--yith-wcbm-badge__wrap-->
		</div>
	';
}

//Sale
if( $product->is_on_sale() || in_array('Sale', $badgeTitles) ) {
	$badges .= '
	<div class="yith-wcbm-badge yith-wcbm-badge-1193 yith-wcbm-badge--on-product-'.$product_id.' yith-wcbm-badge--anchor-point-top-left yith-wcbm-badge-css yith-wcbm-css-badge-1193" data-position="{&quot;top&quot;:0,&quot;bottom&quot;:&quot;auto&quot;,&quot;left&quot;:&quot;-8&quot;,&quot;right&quot;:&quot;auto&quot;}">
			<div class="yith-wcbm-badge__wrap">
				<div class="yith-wcbm-css-s1"></div>
				<div class="yith-wcbm-css-s2"></div>
				<div class="yith-wcbm-css-text">
					<div class="yith-wcbm-badge-text">Promo</div>
				</div>
			</div><!--yith-wcbm-badge__wrap-->
		</div>
	';
}

//Combo
if( has_term( array('combo'), 'product_cat', $product_id ) || in_array('Combo', $badgeTitles) ) {
	$badges .= '
	<div class="yith-wcbm-badge yith-wcbm-badge-1203 yith-wcbm-badge--on-product-'.$product_id.' yith-wcbm-badge--anchor-point-top-left yith-wcbm-badge-css yith-wcbm-css-badge-1203" data-position="{&quot;top&quot;:&quot;30&quot;,&quot;bottom&quot;:&quot;auto&quot;,&quot;left&quot;:&quot;-8&quot;,&quot;right&quot;:&quot;auto&quot;}">
			<div class="yith-wcbm-badge__wrap">
				<div class="yith-wcbm-css-s1"></div>
				<div class="yith-wcbm-css-s2"></div>
				<div class="yith-wcbm-css-text">
					<div class="yith-wcbm-badge-text">Combo</div>
				</div>
			</div><!--yith-wcbm-badge__wrap-->
		</div>
	';
}

//Low Stock or No Stock
if( $product->get_type() == 'simple' ){
	if($stock_status == 'instock'){
		if( ($product->get_stock_quantity() > 0 && $product->get_stock_quantity() < 3) || in_array('Low Stock', $badgeTitles) ){
			$badges .= '
			<div class="yith-wcbm-badge yith-wcbm-badge-1196 yith-wcbm-badge--on-product-'.$product_id.' yith-wcbm-badge--anchor-point-top-left yith-wcbm-badge-css yith-wcbm-css-badge-1196" data-position="{&quot;top&quot;:0,&quot;bottom&quot;:&quot;auto&quot;,&quot;left&quot;:&quot;auto&quot;,&quot;right&quot;:&quot;-7&quot;}">
				<div class="yith-wcbm-badge__wrap">
					<div class="yith-wcbm-css-s1"></div>
					<div class="yith-wcbm-css-s2"></div>
					<div class="yith-wcbm-css-text">
						<div class="yith-wcbm-badge-text">Low Stock</div>
					</div>
				</div><!--yith-wcbm-badge__wrap-->
			</div>
			';
		}
	}else if($stock_status == 'outofstock'){
		$badges .= '
		<div class="yith-wcbm-badge yith-wcbm-badge-1199 yith-wcbm-badge--on-product-'.$product_id.' yith-wcbm-badge--anchor-point-top-left yith-wcbm-badge-css yith-wcbm-css-badge-1199" data-position="{&quot;top&quot;:&quot;30&quot;,&quot;bottom&quot;:&quot;auto&quot;,&quot;left&quot;:&quot;-8&quot;,&quot;right&quot;:&quot;auto&quot;}">
				<div class="yith-wcbm-badge__wrap">
					<div class="yith-wcbm-css-s1"></div>
					<div class="yith-wcbm-css-s2"></div>
					<div class="yith-wcbm-css-text">
						<div class="yith-wcbm-badge-text">Instore Only</div>
					</div>
				</div><!--yith-wcbm-badge__wrap-->
			</div>
		';
	}
}else if( $product->get_type() == 'variable' ){

	$variations = $product->get_available_variations();
	$stock = array();
	if($stock_status == 'instock'){
		foreach($variations as $variation){
			if( isset( $variation['max_qty'] ) ){
				if( ($variation['max_qty'] > 0 && $variation['max_qty'] >= 3) ){
					$stock[] = 'high';
				}else if( ($variation['max_qty'] > 0 && $variation['max_qty'] < 3) || in_array('Low Stock', $badgeTitles) ){
					$stock[] = 'low';
				}
			}
		}
	}else if($stock_status == 'outofstock'){
		$stock[] = 'out';
	}
	
	//echo '<pre>'.print_r($stock_status, true).'</pre>';
	
	if(!empty($stock)){
		if(in_array('low', $stock) && !in_array('high', $stock)){
			$badges .= '
			<div class="yith-wcbm-badge yith-wcbm-badge-1196 yith-wcbm-badge--on-product-'.$product_id.' yith-wcbm-badge--anchor-point-top-left yith-wcbm-badge-css yith-wcbm-css-badge-1196" data-position="{&quot;top&quot;:0,&quot;bottom&quot;:&quot;auto&quot;,&quot;left&quot;:&quot;auto&quot;,&quot;right&quot;:&quot;-7&quot;}">
				<div class="yith-wcbm-badge__wrap">
					<div class="yith-wcbm-css-s1"></div>
					<div class="yith-wcbm-css-s2"></div>
					<div class="yith-wcbm-css-text">
						<div class="yith-wcbm-badge-text">Low Stock</div>
					</div>
				</div><!--yith-wcbm-badge__wrap-->
			</div>
			';	
		}else if(in_array('out', $stock)){
			$badges .= '
			<div class="yith-wcbm-badge yith-wcbm-badge-1199 yith-wcbm-badge--on-product-'.$product_id.' yith-wcbm-badge--anchor-point-top-left yith-wcbm-badge-css yith-wcbm-css-badge-1199" data-position="{&quot;top&quot;:&quot;30&quot;,&quot;bottom&quot;:&quot;auto&quot;,&quot;left&quot;:&quot;-8&quot;,&quot;right&quot;:&quot;auto&quot;}">
					<div class="yith-wcbm-badge__wrap">
						<div class="yith-wcbm-css-s1"></div>
						<div class="yith-wcbm-css-s2"></div>
						<div class="yith-wcbm-css-text">
							<div class="yith-wcbm-badge-text">Instore Only</div>
						</div>
					</div><!--yith-wcbm-badge__wrap-->
				</div>
			';	
		}
	}
}

//Daily Deal
if( $product->is_featured() || in_array('Daily Deal', $badgeTitles) ) {
	$badges .= '
	<div class="yith-wcbm-badge yith-wcbm-badge-1198 yith-wcbm-badge--on-product-'.$product_id.' yith-wcbm-badge--anchor-point-top-left yith-wcbm-badge-css yith-wcbm-css-badge-1198" data-position="{&quot;top&quot;:&quot;auto&quot;,&quot;bottom&quot;:0,&quot;left&quot;:&quot;ayto&quot;,&quot;right&quot;:0}">
			<div class="yith-wcbm-badge__wrap">
				<div class="yith-wcbm-css-s1"></div>
				<div class="yith-wcbm-css-s2"></div>
				<div class="yith-wcbm-css-text">
					<div class="yith-wcbm-badge-text">Daily Deal</div>
				</div>
			</div><!--yith-wcbm-badge__wrap-->
		</div>
	';
}

//Exclusive to Footgear
if(in_array('Exclusive to Footgear', $badgeTitles)){
	$badges .= '
	<div class="yith-wcbm-badge yith-wcbm-badge-1201 yith-wcbm-badge--on-product-'.$product_id.' yith-wcbm-badge--anchor-point-top-left yith-wcbm-badge-css yith-wcbm-css-badge-1201" data-position="{&quot;top&quot;:&quot;30&quot;,&quot;bottom&quot;:&quot;auto&quot;,&quot;left&quot;:&quot;-8&quot;,&quot;right&quot;:&quot;auto&quot;}">
			<div class="yith-wcbm-badge__wrap">
				<div class="yith-wcbm-css-s1"></div>
				<div class="yith-wcbm-css-s2"></div>
				<div class="yith-wcbm-css-text">
					<div class="yith-wcbm-badge-text">Exclusive to FG</div>
				</div>
			</div><!--yith-wcbm-badge__wrap-->
		</div>
	';
}

echo '
<div class="single-prod-badges">'.$badges.'</div>
';
