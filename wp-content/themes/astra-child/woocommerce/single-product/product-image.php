<?php
/**
 * Single Product Image
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-image.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.5.1
 */

defined( 'ABSPATH' ) || exit;

// Note: `wc_get_gallery_image_html` was added in WC 3.3.2 and did not exist prior. This check protects against theme overrides being used on older versions of WC.
if ( ! function_exists( 'wc_get_gallery_image_html' ) ) {
	return;
}

global $product;

$prodID = $product->get_id();
$prodColor = '';
$prodSize = '';
$sizeDefault = '';
$colors = array();
$sizes = array();

if( $product->is_type('variable') ){
	$variations = $product->get_available_variations();
	
	$default_attributes = $product->get_default_attributes();
	
	foreach ( $variations as $variation ){
		$color = $variation['attributes']['attribute_pa_color'];
		$size = $variation['attributes']['attribute_pa_size'];
		if(!in_array($color, $colors)){
			$colors[] = $color;
		}
		if(!in_array($size, $sizes)){
			$sizes[] = $size;
		}
	}
	
	if(!empty($default_attributes)){
		if(isset($default_attributes['pa_color'])){
			$prodColor = $default_attributes['pa_color'];
		}
		if(isset($default_attributes['pa_size'])){
			$prodSize = $default_attributes['pa_size'];
		}
	}else{
		if($prodColor == ''){
			$prodColor = $colors[0];
		}
		if($prodSize == ''){
			$prodSize = $sizes[0];
		}
	}
	
	$sizeDefault = $sizes[0];
}

$columns           = apply_filters( 'woocommerce_product_thumbnails_columns', 4 );
$post_thumbnail_id = $product->get_image_id();
$wrapper_classes   = apply_filters(
	'woocommerce_single_product_image_gallery_classes',
	array(
		'woocommerce-product-gallery',
		'woocommerce-product-gallery--' . ( $post_thumbnail_id ? 'with-images' : 'without-images' ),
		'woocommerce-product-gallery--columns-' . absint( $columns ),
		'images',
	)
);

?>
<?php //echo do_shortcode('[astra_breadcrumb]');?>
<div class="<?php echo esc_attr( implode( ' ', array_map( 'sanitize_html_class', $wrapper_classes ) ) ); ?> woo-variation-gallery-wrapper" data-columns="<?php echo esc_attr( $columns ); ?>" style="opacity: 0; transition: opacity .25s ease-in-out;">
	<div class="woo-variation-gallery-container preload-style-<?php echo trim( get_option( 'woo_variation_gallery_preload_style', 'blur' ) ) ?>">
        <figure class="woocommerce-product-gallery__wrapper">
            <?php
			$output = generate_prod_gallery($prodID,$prodColor,$prodSize,$sizeDefault);
			echo $output[0]; 
			?>
        </figure>
    </div>
</div>
