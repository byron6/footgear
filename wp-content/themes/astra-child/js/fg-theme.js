jQuery.noConflict()(function ($) {
	$("#toggle").click(function() {
		// update content visibility and save as variable
	  var t = $(".store-trading-details");
				t.each(function() {}).on("click", function() {
					var e = $(this).parent().find(".trading-hours");
					e.hasClass("collapsed") ? (e.removeClass("collapsed"),
					$(this).html("Hide")) : (e.addClass("collapsed"),
					$(this).html("Trading hours")),
					$(".store-locator-province-container").trigger("heightChange")
				})
	});
	});
	
	jQuery(document).ready(function ($) {
		
		if($('.woocommerce-checkout').length){
			console.log('Found Postal');
			var postcode = $('input[name="billing_postcode"]').val();
			if(postcode && postcode != ''){
				console.log('Post Code: ' + postcode);
				$('body').trigger('update_checkout');
			}
		}
		
		if(jQuery('.fg-archive-left').length) {
			var stickyTop = $('.fg-archive-left').offset().top;
			
			$(window).scroll(function() {
				var windowTop = $(window).scrollTop();
				
				if (stickyTop < windowTop && $('.fg-archive-left').height() + $('.fg-archive-left').offset().top - $('#wcpf-filter-31278').height() > windowTop) {
					$('.fg-archive-left').css('position', 'fixed');
					$('.fg-archive-left').css('left', '0px');
					$('.fg-archive-left').css('top', '20px');
					$('.fg-archive-left').css('height', '100vh');
					$('.fg-archive-left').css('max-height', '100%');
					$('.fg-archive-right.shrink').css('left', '10%');
					
					$('.fg-archive-filter-btn').css('position', 'fixed');
					$('.fg-archive-filter-btn').css('right', '20px');
					$('.fg-archive-filter-btn').css('top', '40px');
					$('.fg-archive-filter-btn').css('left', 'unset');
					
				} else {
					$('.fg-archive-left').css('position', 'relative');
					$('.fg-archive-left').css('left', '0px');
					$('.fg-archive-left').css('top', 'unset');
					$('.fg-archive-left').css('height', '50vh');
					$('.fg-archive-left').css('max-height', '50%');
					$('.fg-archive-right.shrink').css('left', '0');
					
					$('.fg-archive-filter-btn').css('position', 'relative');
					$('.fg-archive-filter-btn').css('left', '0px');
					$('.fg-archive-filter-btn').css('top', 'unset');
					$('.fg-archive-filter-btn').css('right', 'unset');
					
				}
			});
		}
		
		$('.fg-archive-filter-btn').on('click',function(){
			if($('.fg-archive-right').hasClass('shrink')) {
				$('.fg-archive-left').removeClass('shrink');
				$('.fg-archive-right').removeClass('shrink');
				$('.fg-archive-right').css('left', '0');
			  }
			else {
				$('.fg-archive-left').addClass('shrink');
				$('.fg-archive-right').addClass('shrink');
				$('.fg-archive-right').css('left', '0');
			}
		  });
		  
		  $('#filters-display-mobile').on('click',function(){
			if($('.mobile-archive-filters').css('display') == 'none') {
				$('.mobile-archive-filters').css('display', 'block');
			  }
			else {
			  $('.mobile-archive-filters').css('display', 'none');
			}
		  });
		
		$('.fg-mobile-search .elementor-icon').on('click',function(){
			if($('.fg-search-cont-mobile').css('display') == 'none') {
				$('.fg-search-cont-mobile').css('display', 'block');
			  }
			else {
			  $('.fg-search-cont-mobile').css('display', 'none');
			}
		  });
		
		$('#fg-static-nav h5').on('click',function(){
			if($('.menu-static-page-nav-container').css('display') == 'none') {
				$('.menu-static-page-nav-container').css('display', 'block')
			  }
			else {
			  $('.menu-static-page-nav-container').css('display', 'none')
			}
		  });
		
		$(document).on('click',  '.fg-pop-reg-btn' , function(e) {
			elementorProFrontend.modules.popup.closePopup( { id: 26626 }, event); // Close Login Popup
			elementorProFrontend.modules.popup.showPopup( { id: 26632 }, event);
		});
		
		$(document).on('click',  '.fg-pop-login-btn' , function(e) {
			elementorProFrontend.modules.popup.closePopup( { id: 26632 }, event); // Close Registration Popup
			elementorProFrontend.modules.popup.showPopup( { id: 26626 }, event);
		});
		
		$(document).on('click',  '#fg-cart-account, #fg-cart-login' , function(e) {
			elementorProFrontend.modules.popup.closePopup( { id: 26632 }, event); // Close Registration Popup
			elementorProFrontend.modules.popup.closePopup( { id: 26626 }, event); // Close Login Popup
		});
		
		if($('.reviews-toggle').length){
			var acc = $('.reviews-toggle');
			acc.on("click", function() {
				this.classList.toggle("active");
				var panel = this.nextElementSibling;
				if (panel.style.maxHeight) {
				  panel.style.maxHeight = null;
				} else {
				  panel.style.maxHeight = panel.scrollHeight + "px";
				} 
			});
		}
		
		$('.fg-wcwl-add').on("click", function() {
			var id = $(this).data('id');
			jQuery.ajax({
				url:fggal_ajax_object.ajax_url,
				data:{ 
				  action: 'fg_variation_pop',
				  prodID: id
				},
				beforeSend:function(){
					//$('.fg-prod-quick.variations').addClass('loading-gallery');
				},
				success:function(data){
					//var ouput = JSON.parse(data);
					$('.fg-prod-quick.variations').html(data);
					
					//$('.fg-prod-quick.variations').removeClass('loading-gallery');
				}
			}); 
		});
		
		$(document).on('click',  '.fg-quick-cont .size-cont input, .fg-quick-cont .size-cont input' , function(e) {
			var id = $(this).attr('id').split('-');
			var color = '';
			var size = '';
			id = id[0];
			
			$('#fg-yith-'+ id + ' .my-wish').css('display', 'none');
			$('#fg-yith-'+ id + ' .my-wish-loading').css('display', 'block');
			 
			$('input[name="'+ id +'-fg-wl-size"]').each(function( index ) {
				if($(this).is(':checked')) {
					size = $(this).val();
				}
			});
			$('input[name="'+ id +'-fg-wl-color"]').each(function( index ) {
				if($(this).is(':checked')) {
					color = $(this).val();
				}
			});
			
			jQuery.ajax({
				url:fggal_ajax_object.ajax_url,
				data:{ 
				  action: 'fg_variation_pop_select',
				  prodID: id,
				  prodColor: color,
				  prodSize: size
				},
				success:function(data){
					$('#fg-yith-'+ id + ' .my-wish').html(data);
					var word = $('#fg-yith-'+ id + ' .my-wish a').attr('href');
					console.log(word);
					if (word.indexOf("remove") >= 0){
						$('#fg-yith-'+ id + ' .my-wish').css('display', 'block');
						$('#fg-yith-'+ id + ' .my-wish-loading').css('display', 'none');
					}else{
						$('#fg-yith-'+ id + ' .my-wish a').click();
						setTimeout(
						function() 
						{
							$('#fg-yith-'+ id + ' .my-wish').css('display', 'block');
							$('#fg-yith-'+ id + ' .my-wish-loading').css('display', 'none');
						}, 2500);
					}
				}
			});
			
		});
		
		$(document).on('click',  '.btn-color' , function(e) {
			var btnID = $( this ).data('btn-id');
			$("[data-fg-id="+btnID+"]").data('color') = $( this ).data('color');
		});
		
		$(document).on('click',  '.btn-size' , function(e) {
			var btnID = $( this ).data('btn-id');
			$("[data-fg-id="+btnID+"]").data('size') = $( this ).data('size');
		});
		
		if($('.astra-shop-filter-button').length){
			$('.astra-shop-filter-button').each(function( index ) {
				if(index > 0){
					$(this).html('<i class="fas fa-sliders-h"></i> FILTER');
				}
			});
		}
		
		$('.astra-shop-filter-button').on("click", function() {
			$('.fg-prod-col').addClass('shrink');
			$('.astra-off-canvas-sidebar-wrapper').css('background','transparent');
			$('.astra-shop-filter-button').css('display','none');
			setTimeout(
			function() 
			{
				$('.ast-off-canvas-overlay').css('overflow','auto');
			}, 2000);
			
		});
		
		$('.filter-item').on("click", function() {
			setTimeout(
			function() 
			{
				$('.fg-prod-col').addClass('shrink');
				$('.astra-off-canvas-sidebar-wrapper').css('background','transparent');
				$('.astra-shop-filter-button').css('display','none');
				$('.ast-off-canvas-overlay').css('overflow','auto');
			}, 3500);
			
		});
		
		$('.ast-shop-filter-close').on("click", function() {
			$('.fg-prod-col').removeClass('shrink');
			$('.astra-shop-filter-button').css('display','inline-block');
		});
		
		if($('#cat-explore').length){
			var boxHeight = $('#cat-explore')[0].scrollHeight;
			
			$('#cat-explore').hover(function(){
			  $(this).css("height", boxHeight);
			  }, function(){
			  $(this).css("height", "200");
			});
		}
	
		// WPSL Toggle Accordion
		$(document).ready(function () {
	
			$(".accordion_head").click(function () {
				if ($('.accordion_body').is(':visible')) {
					$(".accordion_body").slideUp(300);
					$(".plusminus").text('+');
				}
				if ($(this).next(".accordion_body").is(':visible')) {
					$(this).next(".accordion_body").slideUp(300);
					$(this).children(".plusminus").text('+');
				} else {
					$(this).next(".accordion_body").slideDown(300);
					$(this).children(".plusminus").text('-');
				}
			});
		});
	
		//WPSL Cat Accordian
		
		var acc = document.getElementsByClassName("accordion");
		var i;
		
		for (i = 0; i < acc.length; i++) {
		  acc[i].addEventListener("click", function() {
			this.classList.toggle("active");
			var panel = this.nextElementSibling;
			if (panel.style.maxHeight) {
			  panel.style.maxHeight = null;
			} else {
			  panel.style.maxHeight = panel.scrollHeight + "px";
			} 
		  });
		}
		
		$('input.variation_id').change(function () {
			if ('' != $('input.variation_id').val()) {
				
				var var_id = $('input.variation_id').val();
				$.ajax({
					url: document.location.origin + '/footgear/wp-admin/admin-ajax.php',
					//url: baseUrl + 'wp-admin/admin-ajax.php',
					data: { 'action': 'ts_before_after_variable', 'vari_id': var_id },
					success: function (data) {
						console.log(data);
						$(".woocommerce-variation-price").append(data);
					},
					error: function (errorThrown) {
						console.log(errorThrown);
					}
				});
			}
		});
		
		$(document).ready(function(){
		  $('#fg_tablet_filter_button .elementor-button').on('click', function(){
			  $(this).html() == "Hide Filters" ? $(this).html('Show Filters') : $(this).html('Hide Filters');
		  });
		});
	
	});