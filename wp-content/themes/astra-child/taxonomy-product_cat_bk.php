<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Astra
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header(); ?>

<?php if ( astra_page_layout() == 'left-sidebar' ) : ?>

	<?php get_sidebar(); ?>

<?php endif ?>

	<div id="primary" <?php astra_primary_class(); ?>>

		<?php astra_primary_content_top(); ?>

		<?php astra_archive_header(); ?>
        
        <h2>Testing Archive.php</h2>
        
        <?php
		if(isset($_GET['filter_brand']) && $_GET['filter_brand'] == 'all'){
			$category = get_queried_object();
			$terms = get_terms( array(
				'taxonomy' => 'pa_brand',
				'hide_empty' => false,
			) );
			foreach($terms as $term) {
				echo '<a href="/product-category/' . $category->slug . '/?filter_band=' . $term->slug . '">' . $term->name . '</a><br>';
			}
		}
		?>

		<?php astra_content_loop(); ?>

		<?php astra_pagination(); ?>

		<?php astra_primary_content_bottom(); ?>

	</div><!-- #primary -->

<?php if ( astra_page_layout() == 'right-sidebar' ) : ?>

	<?php get_sidebar(); ?>

<?php endif ?>

<?php get_footer(); ?>
