<?php
/**
 * Astra Child Theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Astra Child
 * @since 1.0.0
 */

/**
 * Define Constants
 */
define( 'CHILD_THEME_ASTRA_CHILD_VERSION', '1.0.0' );

function wpdocs_dequeue_script() {
    wp_dequeue_script( 'rex-variation-swatches-for-woocommerce' );
	wp_dequeue_script( 'rexvs-frontend' );
}
add_action( 'wp_print_scripts', 'wpdocs_dequeue_script', 100 );

/**
 * Enqueue styles
 */
function child_enqueue_styles() {

	wp_enqueue_style( 'astra-child-theme-css', get_stylesheet_directory_uri() . '/style.css', array('astra-theme-css'), CHILD_THEME_ASTRA_CHILD_VERSION, 'all' );
	
	wp_register_script('fg_jquery', get_stylesheet_directory_uri(). '/js/fg-theme.js' ,array('jquery'),'', true);
    wp_enqueue_script('fg_jquery');

}

add_action( 'wp_enqueue_scripts', 'child_enqueue_styles', 15 );

function fg_sl_js() {

	wp_register_script('fg_sl_jquery', get_stylesheet_directory_uri(). '/js/fg-sl-js.js' ,array('jquery'),'', true);
    wp_enqueue_script('fg_sl_jquery');

}

add_action( 'wp_enqueue_scripts', 'fg_sl_js');

@ini_set( 'upload_max_size' , '1256M' );
@ini_set( 'post_max_size', '1256M');
@ini_set( 'max_execution_time', '300' );

/*Custom Combo Pricing Option*/ 
function fg_option_group() {
	$currency = get_woocommerce_currency_symbol();
	
	echo '<div class="option_group">';
	
	woocommerce_wp_text_input( array(
		'id'                => 'fg_combo_price',
		'value'             => get_post_meta( get_the_ID(), 'fg_combo_price', true ),
		'label'             => 'Combo price ('.$currency.')',
		'desc_tip'    		=> true,
		'description'       => 'Combo price will overwrite regular price if quantity is more than 1.'
	) );
	
	echo '</div>';
}
add_action( 'woocommerce_product_options_general_product_data', 'fg_option_group' );

function fg_save_fields( $id, $post ){
 
	if( !empty( $_POST['fg_combo_price'] ) ) {
		update_post_meta( $id, 'fg_combo_price', $_POST['fg_combo_price'] );
	} else {
		delete_post_meta( $id, 'fg_combo_price' );
	}
 
}
add_action( 'woocommerce_process_product_meta', 'fg_save_fields', 10, 2 );

/*Custom Combo Pricing Option - Variations*/
function variation_settings_fields( $loop, $variation_data, $variation ) {
	$currency = get_woocommerce_currency_symbol();
	
	woocommerce_wp_text_input( 
		array( 
			'id'          => 'fg_combo_price[' . $variation->ID . ']', 
			'label'       => __( 'Combo price ('.$currency.')', 'woocommerce' ),
			'value'       => get_post_meta( $variation->ID, 'fg_combo_price', true ),
			'desc_tip'    => true,
			'description' => 'Combo price will overwrite regular price if quantity is more than 1.' 
		)
	);

}

function save_variation_settings_fields( $post_id ) {
	
	if( !empty( $_POST['fg_combo_price'][ $post_id ] ) ) {
		update_post_meta( $post_id, 'fg_combo_price', $_POST['fg_combo_price'][ $post_id ] );
	} else {
		delete_post_meta( $post_id, 'fg_combo_price' );
	}

}
//add_action( 'woocommerce_product_after_variable_attributes', 'variation_settings_fields', 10, 3 );
//add_action( 'woocommerce_save_product_variation', 'save_variation_settings_fields', 10, 2 );


/*Custom Combo Pricing Cart*/ 
function fg_recalculate_price( $cart_object ) {
	$decimals = wc_get_price_decimals();
	$sep = wc_get_price_decimal_separator();
	
	$categories   = array('combo');
    $has_category = false;
	
	$comboCnt = 0;
	
	if ( is_admin() && ! defined( 'DOING_AJAX' ) )
		return;

    foreach ( $cart_object->get_cart() as $cart_item ) {
        if ( has_term( $categories, 'product_cat', $cart_item['product_id'] ) ) {
            $comboCnt ++;
        }
    }
 
	foreach ( $cart_object->get_cart() as $hash => $value ) {
		
		if(isset($value['variation_id'])){
			$variation = wc_get_product($value['variation_id']);
			$product = wc_get_product( $variation->get_parent_id() );
			$id = $product->get_id();
			//$id = $value['variation_id']; testing
		}else{
			$id = $value['product_id'];
		}

		$combo_price = (float)get_post_meta($id, 'fg_combo_price', true);
		$combo_price = number_format($combo_price, $decimals, $sep, '');

		if( ($value['quantity'] > 1 && isset($combo_price) && $combo_price > 0) || (isset($combo_price) && $combo_price > 0 && $comboCnt > 1)) {
			if( $value['data']->get_regular_price() > $combo_price )
				$value['data']->set_price( $combo_price );

		}

	}
 
}
add_action( 'woocommerce_before_calculate_totals', 'fg_recalculate_price' );

/*Custom Combo Pricing Cart - Variations*/
function filter_cart_item_price( $price, $cart_item, $cart_item_key ) {
	
	$orig_price = $price;
	
	$cart = WC()->cart;
	
	$categories   = array('combo');
    $has_category = false;
	
	$comboCnt = 0;

    foreach ( WC()->cart->get_cart() as $cart_item_check ) {
        if ( has_term( $categories, 'product_cat', $cart_item_check['product_id'] ) ) {
            $comboCnt ++;
        }
    }
	
	$currency = get_woocommerce_currency_symbol();
	$decimals = wc_get_price_decimals();
	$sep = wc_get_price_decimal_separator();
	
	$gty = $cart_item['quantity'];
	$product = $cart_item['data'];
	$price = $product->get_price();
	$product_check = wc_get_product( $product->get_parent_id() );
	//$product_check = wc_get_product($product->get_id()); testing
	$combo_price = (float)$product_check->get_meta('fg_combo_price');
	
	if( ($gty > 1 && isset($combo_price) && $combo_price > 0) || (isset($combo_price) && $combo_price > 0 && $comboCnt > 1)) {
		$price = $combo_price;
	}
	
    $price = __( $currency.number_format($price, $decimals, $sep, ''), 'yourtheme' );
	
	if(current_user_can( 'administrator')){
		//$price .= ' - '.$product->get_parent_id();
	}
	
    return $price;
}
add_filter( 'woocommerce_cart_item_price', 'filter_cart_item_price', 10, 3 );

/*Price History Option*/ 
function fg_historical_price_funct() {
	$currency = get_woocommerce_currency_symbol();
	
	echo '<div class="option_group">';
	
	woocommerce_wp_text_input( array(
		'id'                => '_fg_historical_price',
		'value'             => get_post_meta( get_the_ID(), '_fg_historical_price', true ),
		'label'             => 'Historical Price ('.$currency.')',
		'desc_tip'    		=> true,
		'description'       => 'Historical Price for display of sale pricing on front-end.'
	) );
	
	echo '</div>';
}
add_action( 'woocommerce_product_options_general_product_data', 'fg_historical_price_funct' );

/*Price History Option - Variation*/ 
function fg_historical_price_variation_funct() {
	$currency = get_woocommerce_currency_symbol();
	
	echo '<div class="option_group">';
	
	woocommerce_wp_text_input( array(
		'id'                => 'fg_historical_price',
		'value'             => get_post_meta( get_the_ID(), 'fg_historical_price', true ),
		'label'             => 'Historical Price ('.$currency.')',
		'desc_tip'    		=> true,
		'description'       => 'Historical Price for display of sale pricing on front-end.'
	) );
	
	echo '</div>';
}
add_action( 'woocommerce_product_options_inventory_product_data', 'fg_historical_price_variation_funct' );

function fg_historical_price_save( $id, $post ){
 
	if( !empty( $_POST['fg_historical_price'] ) || $_POST['fg_historical_price'] != '' ) {
		update_post_meta( $id, 'fg_historical_price', $_POST['fg_historical_price'] );
	} else {
		delete_post_meta( $id, 'fg_historical_price' );
	}
	
	if( !empty( $_POST['_fg_historical_price'] ) || $_POST['_fg_historical_price'] != '' ) {
		update_post_meta( $id, '_fg_historical_price', $_POST['_fg_historical_price'] );
	} else {
		delete_post_meta( $id, '_fg_historical_price' );
	}
 
}
add_action( 'woocommerce_process_product_meta', 'fg_historical_price_save', 10, 2 );

/*Sale Price Confirmation*/ 
function fg_hidden_sale_select() {
	
	echo '<div class="option_group">';
	
	// Custom Product Checkbox Field
    woocommerce_wp_checkbox( array(
        'id'        => '_fg_historical_sale_check',
        'label'     => __('Historical Price (On Sale).', 'woocommerce'),
        'desc_tip'  => false
    ));
	
	echo '</div>';
}
add_action( 'woocommerce_product_options_general_product_data', 'fg_hidden_sale_select' );

/*Sale Price Confirmation - Variation*/ 
function fg_hidden_sale_select_variation() {
	
	echo '<div class="option_group">';
	
	// Custom Product Checkbox Field
    woocommerce_wp_checkbox( array(
        'id'        => 'fg_historical_sale_check',
        'label'     => __('Historical Price (On Sale).', 'woocommerce'),
        'desc_tip'  => false
    ));
	
	echo '</div>';
}
add_action( 'woocommerce_product_options_inventory_product_data', 'fg_hidden_sale_select_variation' );

/*Sale Price Confirmation - Save*/
function fg_hidden_sale_save( $id, $post ){
 	
	$woocommerce_checkbox = isset( $_POST['_fg_historical_sale_check'] ) ? 'yes' : 'no';
	update_post_meta( $id, '_fg_historical_sale_check', $woocommerce_checkbox );
	
	$woocommerce_checkbox_variation = isset( $_POST['fg_historical_sale_check'] ) ? 'yes' : 'no';
	update_post_meta( $id, 'fg_historical_sale_check', $woocommerce_checkbox_variation );
 
}
add_action( 'woocommerce_process_product_meta', 'fg_hidden_sale_save', 10, 2 );

/*Add Product to Combo Cat if Combo Price and Sale Cat if Sale*/
function fg_sync_on_product_save( $product_id ) {
	
	$historicalPrice = 0;
	$historicalCheck = 'no';
	$product = wc_get_product($product_id);
	
	if( $product->is_type( 'simple' ) ){
		$historicalPrice = get_post_meta( $product_id, '_fg_historical_price', true );
		$historicalCheck = get_post_meta( $product_id, '_fg_historical_sale_check', true );
	}elseif( $product->is_type( 'variable' ) ){
		$historicalPrice = get_post_meta( $product_id, 'fg_historical_price', true );
		$historicalCheck = get_post_meta( $product_id, 'fg_historical_sale_check', true );
	}
	
	$variations = fg_get_variations($product);
	$combo_ids = array();
	$combo = false;
	$catCombo = term_exists( 'Combo', 'product_cat' );
	$catSale = term_exists( 'Sale', 'product_cat' );
	$catDailyDeails = term_exists( 'Promo', 'product_cat' );
	$newCats = array();
	
	if( $product->is_on_sale() ) {
		$newCats[] = $catDailyDeails['term_id'];
	}
	
	if($variations){
		$combo_ids[] = $product_id;
		/* testing
		foreach($variations as $variant){
			$combo_ids[] = $variant['variation_id'];
		}
		*/
	}else{
		$combo_ids[] = $product_id;
	}
	
	foreach($combo_ids as $id){
		$combo_price = (float)get_post_meta( $id, 'fg_combo_price', true );
		
		if(isset($combo_price) && $combo_price > 0){
			if ( $catCombo !== 0 && $catCombo !== null ) {
				$newCats[] = $catCombo['term_id'];
				/*
				if (!in_array($catDailyDeails['term_id'], $newCats)){
					$newCats[] = $catDailyDeails['term_id'];	
				}
				*/
				$combo = true;
				break;
			}
		}
	}
	
	if(!empty($newCats)){
		wp_set_post_terms( $product_id, $newCats, 'product_cat', true );
	}
	
	if(!$combo){
		if ( $catCombo !== 0 && $catCombo !== null ) {
			if( has_term( array('combo'), 'product_cat', $product_id ) ) {
				wp_remove_object_terms( $product_id, 'Combo', 'product_cat' );
			}
		}
	}
	
	if(!$product->is_on_sale() ) {
		if ( $catDailyDeails !== 0 && $catDailyDeails !== null ) {
			if( has_term( array('promo'), 'product_cat', $product_id ) ) {
				wp_remove_object_terms( $product_id, 'Promo', 'product_cat' );
			}
		}
	}
	
	if($catSale){
		if ( $historicalCheck == 'no') {
			wp_remove_object_terms( $product_id, 'Sale', 'product_cat' );
		}else if( $historicalCheck == 'yes' && (float)$historicalPrice > 0) {
			wp_set_post_terms( $product_id, array($catSale['term_id']), 'product_cat', true );
		}
	}
	
}
//add_action( 'woocommerce_update_product', 'fg_sync_on_product_save', 10, 1 );

/*Custom Price Text*/
function fg_custom_price_txt($price_html, $product) {
	//Exclude Admin Pages
    if ( is_admin() ) return $price_html;
	//Exclude Products with zero Prices
	if ( '' === $product->get_price() ) return $price_html;
	
	$currency = get_woocommerce_currency_symbol();
	$decimals = wc_get_price_decimals();
	$sep = wc_get_price_decimal_separator();
	
	$historical_txt = '';
	
	//$price_html .= '<pre>'.print_r($product, true).'</pre>';
	if(current_user_can( 'administrator')){
		//$price_html .= '<pre>'.print_r(get_the_ID(), true).'</pre>';
	}
	
	if( $product->is_type( 'simple' ) ){
		
		$historical_price = number_format((float)$product->get_meta('_fg_historical_price'), $decimals, $sep, '');
		$historical_check = $product->get_meta('_fg_historical_sale_check');
		
		if(isset($historical_price) && $historical_price > 0 && $historical_check == 'yes'){
		
			$historical_discount = number_format(((float)$historical_price - (float)$product->get_regular_price()), $decimals, $sep, '');
			
			if($historical_discount > 0){
				$historical_txt = '
				<br><span class="price-lbl">SAVE</span> <ins><span class="woocommerce-Price-amount amount"><bdi style="color:#f99900;"><span class="woocommerce-Price-currencySymbol">'.$currency.'</span>'.$historical_discount.'</bdi></span></ins>';
			}
		}
		//Display Saving Ammount
		if( $product->is_on_sale() ) {
			$regular_price = $product->get_regular_price();
			$sale_price = number_format((float)$product->get_sale_price(), $decimals, $sep, '');
			$combo_price = number_format((float)$product->get_meta('fg_combo_price'), $decimals, $sep, '');
			$discount = number_format(((float)$regular_price - (float)$sale_price), $decimals, $sep, '');

			$price_html .= '
			<ins><span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">'.$currency.'</span>'.$sale_price.'</bdi></span></ins><br><span class="price-lbl">SAVE</span> <ins><span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">'.$currency.'</span>'.$discount.'</bdi></span></ins>';
		}
		
		//Display Combo Prices
		if(isset($combo_price) && $combo_price > 0){
			$combo_price = number_format(((float)$combo_price * 2), $decimals, $sep, '');
			$price_html .= '
			<br><span class="price-lbl" style="color:#c02114; line-height: 40px;">2 FOR </span><ins><span class="woocommerce-Price-amount amount"><bdi style="color:#c02114;"><span class="woocommerce-Price-currencySymbol">'.$currency.'</span>'.$combo_price.'</bdi></span></ins>';
		}
		
	}elseif( $product->is_type( 'variable' ) ){
		$variations = fg_get_variations($product);
		$combo_prices = array();
		
		//$price_html .= '<pre>'.print_r(get_post_meta( $product->id, 'fg_combo_price', true ), true).'</pre>';
		
		foreach($variations as $variant){
			/*$combo_variant_price = number_format((float)get_post_meta( $variant['variation_id'], 'fg_combo_price', true ), $decimals, $sep, ''); testing*/
			$combo_variant_price = number_format((float)get_post_meta( $product->id, 'fg_combo_price', true ), $decimals, $sep, '');
			
			if(isset($combo_variant_price) && $combo_variant_price > 0){
				$combo_prices[] = $combo_variant_price;
			}
		}
		$discount = 0;
	  
		// Regular price min and max
		$min_regular_price = $product->get_variation_regular_price( 'min' );
		$max_regular_price = $product->get_variation_regular_price( 'max' );
		
		// Sale price min and max
		$min_sale_price = $product->get_variation_sale_price( 'min' );
		$max_sale_price = $product->get_variation_sale_price( 'max' );
		
		$min_discount = (float)$min_regular_price - (float)$min_sale_price;
		$max_discount = (float)$max_regular_price - (float)$max_sale_price;
		$discount = $max_discount;
		
		if($min_discount > $max_discount){
			$discount = $min_discount;
		}
		
		$discount = number_format($discount, $decimals, $sep, '');
		
		$historical_price = number_format((float)$product->get_meta('fg_historical_price'), $decimals, $sep, '');
		$historical_check = $product->get_meta('fg_historical_sale_check');
		
		if(isset($historical_price) && $historical_price > 0 && $historical_check == 'yes'){
		
			$historical_discount = number_format(((float)$historical_price - (float)$max_regular_price), $decimals, $sep, '');
			
			if($historical_discount > 0){
				$historical_txt = '
				<br><span class="price-lbl">SAVE</span> <ins><span class="woocommerce-Price-amount amount"><bdi style="color:#f99900;"><span class="woocommerce-Price-currencySymbol">'.$currency.'</span>'.$historical_discount.'</bdi></span></ins>';
			}
		}
		
		//Display Saving Ammount
		if($discount > 0){
			$percentage = round(($discount / (float)$max_regular_price) * 100);
			
			$price_html .= '
			<br><span class="price-lbl" style="color:#c02114;">SAVE</span> <ins><span class="woocommerce-Price-amount amount"><bdi style="color:#c02114;">'.$percentage.'%</bdi></span></ins>';
		}
		
		//Display Combo Prices
		if(!empty($combo_prices)){
			sort($combo_prices);
			$min_combo_price = number_format((float)$combo_prices[0], $decimals, $sep, '');
			$max_combo_price = number_format((float)$combo_prices[count($combo_prices)-1], $decimals, $sep, '');
			
			$max_combo_price = number_format(((float)$max_combo_price * 2), $decimals, $sep, '');
			$min_combo_price = number_format(((float)$min_combo_price * 2), $decimals, $sep, '');
				
			if(count($combo_prices) > 1){
				$price_html .= '
			<br><span class="price-lbl" style="color:#c02114; line-height: 40px;">2 FOR </span><span class="woocommerce-Price-amount amount"><bdi style="color:#c02114;"><span class="woocommerce-Price-currencySymbol">'.$currency.'</span>'.$max_combo_price.'</bdi></span>';
			}else{
				$price_html .= '
			<br><span class="price-lbl" style="color:#c02114; line-height: 40px;">2 FOR </span><span class="woocommerce-Price-amount amount"><bdi style="color:#c02114;"><span class="woocommerce-Price-currencySymbol">'.$currency.'</span>'.$min_combo_price.'</bdi></span>';
			}
		}
	  
	}
	
	return $price_html.''.$historical_txt; 
}
add_filter( 'woocommerce_get_price_html', 'fg_custom_price_txt', 99, 2 );

/*Custom Price Text - Variation Selection*/
function fg_custom_ajax_price_txt(){
	$currency = get_woocommerce_currency_symbol();
	if ( isset($_REQUEST) ) {
		$variation_id = sanitize_text_field($_REQUEST['vari_id']);
		$variable_product = wc_get_product($variation_id);
		$regular_price = $variable_product->get_regular_price();
		$sale_price = $variable_product->get_sale_price();

		$combo_price = (float)$variable_product->get_meta('fg_combo_price');
		$combo_txt = '';
		if(isset($combo_price) && $combo_price > 0){
				$combo_txt .= '<br><br><span class="save-label">COMBO Price</span><br><bdi><span class="woocommerce-Price-currencySymbol">'.$currency.'</span>'.$combo_price.'</bdi>';
		}
		
		$discount = $regular_price - $sale_price;
		
		if( !empty($sale_price) ) {
			
			$datastr = '<bdi><span class="woocommerce-Price-currencySymbol">'.$currency.'</span>'.$regular_price.'</bdi><br><span class="save-label">SAVE</span><br><bdi><span class="woocommerce-Price-currencySymbol">'.$currency.'</span>'.$discount.'</bdi>';

		}
		
		echo $datastr.$combo_txt;
	}
}
//add_action("wp_ajax_ts_before_after_variable", "fg_custom_ajax_price_txt");

//add ACF rule
function acf_location_rule_values_Post( $choices ) {
    $choices['product_variation'] = 'Product Variation';
    //print_r($choices);
    return $choices;
}
add_filter('acf/location/rule_values/post_type', 'acf_location_rule_values_Post');

/*Function to check and retrieve product variations*/
function fg_get_variations($product){
	$variations = false;
	if( $product->is_type('variable') ){
		$variations = $product->get_available_variations();
	}
	return $variations;
}

//Main Menu Sub Links
function fg_tax_funct($atts){
	
	$protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
	//$url = $protocol . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
	$url = get_home_url().'/';
	$html = $class = '';
	$fullwidth = false;
	$url_prefix = get_permalink( wc_get_page_id( 'shop' ) );
	
	$options = shortcode_atts( array(
		'cat' => 0,
		'tax' => 'yith_product_brand',
		'title' => 'Brands',
		'layout' => 1
	), $atts );
	
	$tax = $options['tax'];
	$cat = $options['cat'];
	$title = $options['title'];
	$layout = $options['layout'];
	
	if($layout == 2){
		$class = ' fullwidth';
		$fullwidth = true;
	}
	
	if($tax == 'yith_product_brand'){
		$parent = 0;
	}else{
		$parent = $cat;
	}
	
	$terms_args = array(
		'taxonomy' => $tax,
		'hide_empty' => false
	);
	
	if($title == "Clothing" || $title == "Footwear" || $title == "Accessories"){
		$terms_args['child_of'] = $parent;
	}else{
		$terms_args['parent'] = $parent;
	}
	
	$terms = get_terms($terms_args);
	
	//$html .= '<pre>'.print_r($terms, true).'</pre>';
	
	$cat_options = get_term_by( 'id', $cat, 'product_cat' );
	$url_prefix = $url.'category/'.$cat_options->slug.'/';
	if($fullwidth){
		$shop_url = get_permalink( wc_get_page_id( 'shop' ) );
		$url_prefix = $shop_url;
	}
	$html .= '
	<div class="menu-list-cont'.$class.'">';
	
	if(!$fullwidth){
	$cat_link = get_category_link( $cat );
	$html .= '
	<div class="menu-list-head">'.$title.'<span><a href="'.$cat_link.'">View All</a></span></div>';
	}
	
	$html .= '
	<ul>
	';
	
	foreach($terms as $term){
		//$html .= '<pre>'.print_r($term, true).'</pre>';
		$cat_link = get_category_link( $cat );
		$thumbnail_id = get_term_meta( $term->term_id, 'thumbnail_id', true );
		$thumbnail_url = wp_get_attachment_image_src( $thumbnail_id );
		
		$thumbnail = get_option('yith_wcbr_logo_default');
		$thumbnail = wp_get_attachment_image_src( $thumbnail );
		$thumbnail = $thumbnail[0];
		
		$tax = str_replace('pa_', '',$tax);
		if($fullwidth && isset($thumbnail_url)){
			if($tax == 'yith_product_brand'){
				if($thumbnail_url == ''){
					$thumbnail_url = $thumbnail;
				}else{
					$thumbnail_url = $thumbnail_url[0];
				}
				$html .= '<li><a href="'.get_term_link($term->term_id).'"><img name="'.$term->name.'" src="'.$thumbnail_url.'" alt="'.$term->name.'" /></a></li>';
			}else{
				if($term->slug == "men" || $term->slug == "women" || $term->slug == "kids" || $term->slug == "accessories"){
					$html .= '<li><a href="'.$cat_link.'?orderby=date"><img name="'.$term->name.'" src="'.$thumbnail_url[0].'" alt="'.$term->name.'" /></a></li>';
				}
			}
		}else{
			//http://localhost/footgear/shop/category/gender/women/?yith_wcan=1&product_cat=promo+women
			if($tax == 'product_cat'){
				if($term->slug != 'daily-deals'){
					$html .= '<li><a href="'.get_term_link($term->term_id).'">'.$term->name.'</a></li>';
				}
			}else{
				$mycat = get_term( $cat, 'product_cat' );
				$html .= '<li><a href="'.$cat_link.'?option-brand='.$term->slug.'">'.$term->name.'</a></li>';
			}
		}
	}
	
	$html .= '</ul>';
	
	if($fullwidth && isset($thumbnail_url)){
		if($tax == 'yith_product_brand'){
			$html .= '<a class="elementor-cta__button elementor-button elementor-size-sm fg-brands-button" href="'.$url.'shop/">Shop All Brands</a>';
		}
	}
	
	$html .= '</div>';
	
	return $html;
}
add_shortcode('fg-tax', 'fg_tax_funct');

//Brands Page
function fg_brands_funct($atts){
	
	$html = '<div id="brands-cont">';
	
	$terms = get_terms( array( 
		'taxonomy' => 'yith_product_brand',
		'parent'   => 0,
		'hide_empty' => false
	) );
	
	foreach($terms as $term){
	
		//$html .= '<pre>'.print_r(get_term_link($term->term_id), true).'</pre>';
		
		$taxID = $term->term_id;
		$thumbnail_url_color = get_field('brand_colour_logo', $taxID);
		
		$thumbnail_id = get_term_meta( $taxID, 'thumbnail_id', true );
		$thumbnail_url = wp_get_attachment_image_src( $thumbnail_id );
		$thumbnail_url = $thumbnail_url[0];
		
		$thumbnail = get_option('yith_wcbr_logo_default');
		$thumbnail = wp_get_attachment_image_src( $thumbnail );
		$thumbnail = $thumbnail[0];
		
		//$html .= '<pre>'.print_r($thumbnail_url, true).'</pre>';
		
		if(isset($thumbnail_url_color) && $thumbnail_url_color != ''){
			$thumbnail = $thumbnail_url_color;
		}else if(isset($thumbnail_url) && $thumbnail_url != ''){
			$thumbnail = $thumbnail_url;
		}
		
		$html .= '<div class="flex-brand-item"><a href="'.get_term_link($term->term_id).'"><img name="'.$term->name.'" src="'.$thumbnail.'" alt="'.$term->name.'" /></a></div>';
	}
	
	$html .= '</div>';
	
	echo $html;
	
}
add_shortcode('fg-brands', 'fg_brands_funct');

/*Custom WPSL Template*/
function custom_templates( $templates ) {

    $templates[] = array (
        'id'   => 'custom',
        'name' => 'Custom template',
        'path' => get_stylesheet_directory() . '/' . 'wpsl-templates/custom.php',
    );

    return $templates;
}
add_filter( 'wpsl_templates', 'custom_templates' );

//Footgear Ribbon - Ad Slider
function fg_cat_dropdown_funct($atts){
	
	$html = '';
	
	$options = shortcode_atts( array(
		'cat' => 15,
		'attr' => 'brand',
		'title' => 'Brands',
		'layout' => 1
	), $atts );
	
	$attr = $options['attr'];
	$cat = $options['cat'];
	$title = $options['title'];
	$layout = $options['layout'];
	
	$cats = get_terms( array( 
		'taxonomy' => 'wpsl_store_category',
		'orderby' => 'name',
        'order' => 'ASC',
		'hide_empty' => true,
		'parent'   => 0
	) );
	
	$wpsl_cats = array();
	
	foreach($cats as $cat){
		$args = array(
		'post_type' => 'wpsl_stores',
		'post_status' => 'publish',
		'orderby' => 'title',
        'order' => 'ASC',
		'post_count' => -1,
		'nopaging' => true,
		'tax_query' => array(
			array(
			'taxonomy' => 'wpsl_store_category',
			'field' => 'term_id',
			'terms' => $cat->term_id
			 )
		  )
		);
		$stores = new WP_Query( $args );
		
		$lat = get_field('fg-wpsl-cat-lat', $cat);
		$long = get_field('fg-wpsl-cat-long', $cat);
		
		$wpsl_cats[] = array(
			'id' => $cat->term_id,
			'lat' => $lat,
			'long' => $long,
			'name' => $cat->name,
			'count' => $cat->count,
			'stores' => $stores,
		);
	}
	
	$html .= '
	<div class="fg-Store-cats">
	';
	
	foreach($wpsl_cats as $wpsl_cat){
	
		$html .= '
		<div class="accordion_container">
  <div class="accordion_head" lat="'.$wpsl_cat['lat'].'" long="'.$wpsl_cat['long'].'">'.$wpsl_cat['name'].' [ '.$wpsl_cat['count'].' ]<span class="plusminus">+</span></div>
  <div class="accordion_body" style="display: none;">
  ';
    foreach($wpsl_cat['stores']->posts as $store){
    $mydata = $store->wpsl_hours;
	$monday = $mydata['monday'][0];
	$tuesday = $mydata['tuesday'][0];
	$wednesday = $mydata['wednesday'][0];
	$thursday = $mydata['thursday'][0];
	$friday = $mydata['friday'][0];
	$saturday = $mydata['saturday'][0];
	$sunday = $mydata['sunday'][0];
	
				$html .= '<div class="store">
					<p class="storetitle"><a href="'.get_permalink($store->ID).'">'.$store->post_title.'</a></p>
					<p class="storeaddress">'.$store->wpsl_address.', '.$store->wpsl_address2.', '.$store->wpsl_city.', '.$store->wpsl_zip.'</p>
					<p class="storephone">0'.$store->wpsl_phone.'</p>
					<button class="accordion">Trading Hours</button>
					<div class="panel">
						<div class="inner-panel">
							<p>Monday: '.$monday.'</p>
							<p>Tuesday: '.$tuesday.'</p>
							<p>Wednesday: '.$wednesday.'</p>
							<p>Thursday: '.$thursday.'</p>
							<p>Friday: '.$friday.'</p>
							<p>Saturday: '.$saturday.'</p>
							<p>Sunday: '.$sunday.'</p>
						</div>  
					</div>  
					</div>
				';
			}
		
			$html .= '
			</div>
		</div>
		
		';
	}
	
	$html .= '
	</div>
	';
	
	return $html;
}
add_shortcode('fg-cat-dropdown', 'fg_cat_dropdown_funct');

/* CART AND CHECKOUT MESSAGE */

add_action('woocommerce_before_cart_table', 'footgear_cart_message');

function footgear_cart_message( ) {
 echo '<div class="footgear_checkout_message"><h4 style="font-size: 14px;color: #ff0000;">Free delivery for orders above R650</h4></div><div class="footgear_checkout_message_sa" style="margin-bottom:0px;"><h4 style="font-size: 14px;color: #ff0000;">Shipping Only In South Africa</h4></div><div class="footgear_checkout_message_sa"><h4 style="font-size: 14px;color: #ff0000;">Items are reserved for 30 minutes</h4></div>';
}

add_action( 'woocommerce_before_checkout_form', 'footgear_checkout_message', 10 );

function footgear_checkout_message( ) {
 echo '<div class="footgear_checkout_message"><h4 style="font-size: 14px;color: #ff0000;">Free delivery for orders above R650</h4></div><div class="footgear_checkout_message_sa"><h4 style="font-size: 14px;color: #ff0000;">Shipping Only In South Africa</h4></div><div class="footgear_checkout_message_sa"><h4 style="font-size: 14px;color: #ff0000;">Items are reserved for 30 minutes</h4></div>';
}


/* Shop Side Button */
add_action('woocommerce_before_main_content', 'shop_side_button', 5);

function shop_side_button() {
	if(!is_product()){
    echo '<div class="shop_side_button" style="position: fixed;left: 0;top: 50%;"><button class="button astra-shop-filter-button fg-filter-btn" data-selector="astra-off-canvas-sidebar-wrapper" style="border-top-left-radius: 0px !important;border-bottom-left-radius: 0px !important;"><i class="fas fa-sliders-h"></i></button></div>';
	}
}

/* Rename Sorting Options */ 
add_filter( 'woocommerce_catalog_orderby', 'misha_change_sorting_options_order', 5 );
 
function misha_change_sorting_options_order( $options ){
 
	$options = array(
 
		'menu_order' => __( 'Default sorting', 'woocommerce' ), 
		'date'       => __( 'Sort by newest', 'woocommerce' ),
		'popularity' => __( 'Sort by popularity', 'woocommerce' ),
		'rating'     => 'Sort by average rating',
		'price'      => __( 'Sort by price: low to high', 'woocommerce' ), 
		'price-desc' => __( 'Sort by price: high to low', 'woocommerce' ),
 
	);
 
	return $options;
 
}



add_filter( 'woocommerce_catalog_orderby', 'misha_add_custom_sorting_options' );
add_filter( 'woocommerce_default_catalog_orderby_options', 'misha_add_custom_sorting_options' );
function misha_add_custom_sorting_options( $options ){
 
	$options['deals'] = 'Sort by deals';
 
	return $options;
 
}

add_filter( 'woocommerce_get_catalog_ordering_args', 'misha_custom_product_sorting' );
 
function misha_custom_product_sorting( $args ) {
 

	// Show products in stock first
	$orderby_value = isset( $_GET['orderby'] ) ? wc_clean( $_GET['orderby'] ) : apply_filters( 'woocommerce_default_catalog_orderby', get_option( 'woocommerce_default_catalog_orderby' ) );
	if(  'deals' == $orderby_value) {
		$args['meta_query'] = array(
			'relation' => 'AND',
			'sale_simple' => array(
				'key' => '_sale_price',
				'compare' => 'EXISTS',
			),
			'sale_variation' => array(
				'key' => '_min_variation_sale_price',
				'compare' => 'EXISTS',
			),
			'combo' => array(
				'key' => 'fg_combo_price',
				'compare' => 'EXISTS',
			), 
		);
		
		$args['tax_query'] = array(
            'dailydeals' => array(
                'taxonomy' => 'product_cat',
                'field'    => 'slug',
                'terms'    => array('daily-deals'),
            )
        );
        $args['tax_query'] = array(
            'combocat' => array(
                'taxonomy' => 'product_cat',
                'field'    => 'slug',
                'terms'    => array('combo'),
            )
        );
		$args['orderby'] = array(
			//'dailydeals' => 'ASC',
			'combocat' => 'ASC',
			'sale_simple' => 'ASC',
			'sale_variation' => 'ASC',
			'combo' => 'ASC',
		);
	}
 
	return $args;
 
}

//Login Form
function fg_login_form_funct() {
	global $post;
	$id = $post->ID;

   if ( is_admin() ) return;
   if ( is_user_logged_in() && $id == 2251 ) header("Location: ".get_permalink(get_option('woocommerce_myaccount_page_id'))); 
   ob_start();
   ?>
   <h2 class="login-head">Returning Customer</h2>
    
    <form class="woocommerce-form woocommerce-form-login login" method="post">
    
        <?php do_action( 'woocommerce_login_form_start' ); ?>
    
        <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
            <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="username" id="username" autocomplete="username" value="<?php echo ( ! empty( $_POST['username'] ) ) ? esc_attr( wp_unslash( $_POST['username'] ) ) : ''; ?>" placeholder="*Username/Email Address" /><?php // @codingStandardsIgnoreLine ?>
        </p>
        <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
            <input class="woocommerce-Input woocommerce-Input--text input-text" type="password" name="password" id="password" autocomplete="current-password" placeholder="*Password" />
        </p>
    
        <?php do_action( 'woocommerce_login_form' ); ?>
    
        <p class="form-row">
            <label class="woocommerce-form__label woocommerce-form__label-for-checkbox woocommerce-form-login__rememberme">
                <input class="woocommerce-form__input woocommerce-form__input-checkbox" name="rememberme" type="checkbox" id="rememberme" value="forever" /> <span><?php esc_html_e( 'Remember me', 'woocommerce' ); ?></span>
            </label>
         </p>
         <p class="form-row">
            <?php wp_nonce_field( 'woocommerce-login', 'woocommerce-login-nonce' ); ?>
            <button type="submit" class="woocommerce-button button woocommerce-form-login__submit" name="login" value="<?php esc_attr_e( 'Log in', 'woocommerce' ); ?>"><?php esc_html_e( 'Log in', 'woocommerce' ); ?></button> &nbsp; <a href="<?php echo esc_url( wp_lostpassword_url() ); ?>"><?php esc_html_e( 'Forgot Password?', 'woocommerce' ); ?></a>
        </p>
    
        <?php do_action( 'woocommerce_login_form_end' ); ?>
    
    </form>
    <?php
   return ob_get_clean();
}
add_shortcode( 'fg_login_form', 'fg_login_form_funct' );

//Registration Form 
function fg_registration_form_funct() {
	global $post;
	$id = $post->ID;
	
   if ( is_admin() ) return;
   if ( is_user_logged_in() && $id == 2253 ) header("Location: ".get_permalink(get_option('woocommerce_myaccount_page_id')));
   ob_start();
 
   do_action( 'woocommerce_before_customer_login_form' );
 
   ?>
  <form method="post" class="woocommerce-form woocommerce-form-register register" <?php do_action( 'woocommerce_register_form_tag' ); ?> >

        <?php do_action( 'woocommerce_register_form_start' ); ?>
        
        	<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                <input type="text" class="input-text" name="billing_first_name" id="reg_billing_first_name" value="<?php if ( ! empty( $_POST['billing_first_name'] ) ) esc_attr_e( $_POST['billing_first_name'] ); ?>" placeholder="*First Name"/>
            </p>
            
            <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                <input type="text" class="input-text" name="billing_last_name" id="reg_billing_last_name" value="<?php if ( ! empty( $_POST['billing_last_name'] ) ) esc_attr_e( $_POST['billing_last_name'] ); ?>"  placeholder="*Last Name"/>
            </p>
            
            <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                <input type="text" class="input-text" name="billing_mobile" id="reg_billing_mobile" value="<?php if ( ! empty( $_POST['billing_mobile'] ) ) esc_attr_e( $_POST['billing_mobile'] ); ?>" placeholder="*Mobile Number"/>
            </p>
            
            <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                <input type="email" class="woocommerce-Input woocommerce-Input--text input-text" name="email" id="reg_email" autocomplete="email" value="<?php echo ( ! empty( $_POST['email'] ) ) ? esc_attr( wp_unslash( $_POST['email'] ) ) : ''; ?>" placeholder="*Email"/><?php // @codingStandardsIgnoreLine ?>
            </p>
            
            <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                <input type="text" class="input-text" name="billing_dob" id="reg_billing_dob" value="<?php if ( ! empty( $_POST['billing_dob'] ) ) esc_attr_e( $_POST['billing_dob'] ); ?>" placeholder="*Date of Birth"/>
            </p>
            
        <?php if ( 'no' === get_option( 'woocommerce_registration_generate_username' ) ) : ?>

            <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="username" id="reg_username" autocomplete="username" value="<?php echo ( ! empty( $_POST['username'] ) ) ? esc_attr( wp_unslash( $_POST['username'] ) ) : ''; ?>" placeholder="*Username"/><?php // @codingStandardsIgnoreLine ?>
            </p>

        <?php endif; ?>

        <?php if ( 'no' === get_option( 'woocommerce_registration_generate_password' ) ) : ?>

            <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                <input type="password" class="woocommerce-Input woocommerce-Input--text input-text" name="password" id="reg_password" autocomplete="new-password" placeholder="*Password" />
            </p>
            
            <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                <input type="password" class="input-text" name="password2" id="reg_password2" value="<?php if ( ! empty( $_POST['password2'] ) ) echo esc_attr( $_POST['password2'] ); ?>" placeholder="*Confirm Password"/>
            </p>

        <?php else : ?>

            <p><?php esc_html_e( 'A password will be sent to your email address.', 'woocommerce' ); ?></p>

        <?php endif; ?>

        <?php do_action( 'woocommerce_register_form' ); ?>

        <p class="woocommerce-form-row form-row">
            <?php wp_nonce_field( 'woocommerce-register', 'woocommerce-register-nonce' ); ?>
            <button type="submit" class="woocommerce-Button woocommerce-button button woocommerce-form-register__submit" name="register" value="<?php esc_attr_e( 'Register', 'woocommerce' ); ?>"><?php esc_html_e( 'Register', 'woocommerce' ); ?></button>
        </p>

        <?php do_action( 'woocommerce_register_form_end' ); ?>

    </form>
 
   <?php
     
   return ob_get_clean();
}
add_shortcode( 'fg_registration_form', 'fg_registration_form_funct' );


//Custom Logout Redirect
function fg_logout_redirect(){
	wp_redirect( home_url( '/login/' ) );
	exit;
}
add_action('wp_logout','fg_logout_redirect');

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@ Custom Fields
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

//Create Custom Reg Fields
function fg_extra_register_fields() {?>
	<h1>Create an Account</h1>
    <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide fg-reg-intro">Join our squad for exclusive updates on the hottest deals, latest news and more!</p>
    <?php
 }
add_action( 'woocommerce_register_form_start', 'fg_extra_register_fields' );
 
//Validate Custom Reg Fields
function fg_validate_extra_register_fields( $username, $email, $validation_errors ) {
	if ( isset( $_POST['billing_first_name'] ) && empty( $_POST['billing_first_name'] ) ) {
		$validation_errors->add( 'billing_first_name_error', __( 'First name is required!', 'woocommerce' ) );
	}
	if ( isset( $_POST['billing_last_name'] ) && empty( $_POST['billing_last_name'] ) ) {
		$validation_errors->add( 'billing_last_name_error', __( 'Last name is required!', 'woocommerce' ) );
	}
	if ( isset( $_POST['billing_mobile'] ) && empty( $_POST['billing_mobile'] ) ) {
		$validation_errors->add( 'billing_mobile_error', __( 'Mobile Number is required!', 'woocommerce' ) );
	}
	if ( isset( $_POST['billing_dob'] ) && empty( $_POST['billing_dob'] ) ) {
		$validation_errors->add( 'billing_dob_error', __( 'Date of Birth is required!', 'woocommerce' ) );
	}
	if ( isset( $_POST['password2'] ) && empty( $_POST['password2'] ) ) {
		$validation_errors->add( 'billing_dob_error', __( 'Please confirm your Password!', 'woocommerce' ) );
	}
	
	if ( isset( $_POST['password'] ) && isset( $_POST['password2'] ) ) {
		if ( strcmp( $_POST['password'], $_POST['password2'] ) !== 0 ) {
			$validation_errors->add( 'password2_error', __( 'Passwords do not match!', 'woocommerce' ) );
		}
	}
	
	return $validation_errors;
}
add_action( 'woocommerce_register_post', 'fg_validate_extra_register_fields', 10, 3 );

//Save Custom Reg Fields
function reg_save_extra_register_fields( $customer_id ) {
	if ( isset( $_POST['billing_first_name'] ) ) {
		update_user_meta( $customer_id, 'first_name', sanitize_text_field( $_POST['billing_first_name'] ) );
		update_user_meta( $customer_id, 'billing_first_name', sanitize_text_field( $_POST['billing_first_name'] ) );
		update_user_meta( $customer_id, 'shipping_first_name', sanitize_text_field( $_POST['billing_first_name'] ) );
	}
	if ( isset( $_POST['billing_last_name'] ) ) {
		update_user_meta( $customer_id, 'last_name', sanitize_text_field( $_POST['billing_last_name'] ) );
		update_user_meta( $customer_id, 'billing_last_name', sanitize_text_field( $_POST['billing_last_name'] ) );
		update_user_meta( $customer_id, 'shipping_last_name', sanitize_text_field( $_POST['billing_last_name'] ) );
	}
	  
	if ( isset( $_POST['billing_mobile'] ) ) {
		update_user_meta( $customer_id, 'billing_mobile', sanitize_text_field( $_POST['billing_mobile'] ) );
	}
	if ( isset( $_POST['billing_dob'] ) ) {
		update_user_meta( $customer_id, 'billing_dob', sanitize_text_field( $_POST['billing_dob'] ) );
	}
	if ( isset( $_POST['billing_gender'] ) ) {
		update_user_meta( $customer_id, 'billing_gender', sanitize_text_field( $_POST['billing_gender'] ) );
	}
}
add_action( 'woocommerce_created_customer', 'reg_save_extra_register_fields' );

function mysite_custom_define() {
	$custom_meta_fields = array();
	$custom_meta_fields['billing_mobile'] = 'Mobile Number';
	$custom_meta_fields['billing_dob'] = 'Date of Birth';
	$custom_meta_fields['billing_gender'] = 'Gender';
	return $custom_meta_fields;
}
function mysite_show_extra_profile_fields($user) {
	print('<h3>Extra Profile Information</h3>');
	print('<table class="form-table">');
	$meta_number = 0;
	$custom_meta_fields = mysite_custom_define();
	foreach ($custom_meta_fields as $meta_field_name => $meta_disp_name) {
	$meta_number++;
	print('<tr>');
	print('<th><label for="' . $meta_field_name . '">' . $meta_disp_name . '</label></th>');
	print('<td>');
	
	if($meta_field_name == 'billing_gender'){
		$gender_options = '<option value>Male or Female</option>';
		$genders = array('male','female');
		$gender_selected = esc_attr( get_the_author_meta('billing_gender', $user->ID ) );
		foreach($genders as $gender)
		{
			if ($gender_selected == $gender){
				$gender_options .= '<option value="'.$gender.'" selected="selected" >'.$gender.'</option>';
			}else{
				$gender_options .= '<option value="'.$gender.'">'.$gender.'</option>';
			}
		}
		print('<select name="'.$meta_field_name.'" id="'.$meta_field_name.'">');
		print($gender_options);
		print('</select>');
		print('<span class="description"></span>');
	}else{
		print('<input type="text" name="' . $meta_field_name . '" id="' . $meta_field_name . '" value="' . esc_attr( get_the_author_meta($meta_field_name, $user->ID ) ) . '" class="regular-text" /><br />');
		print('<span class="description"></span>');
	}
	print('</td>');
	print('</tr>');
	}
	print('</table>');
}
function mysite_save_extra_profile_fields($user_id) {
	if (!current_user_can('edit_user', $user_id))
	return false;
	$meta_number = 0;
	$custom_meta_fields = mysite_custom_define();
	foreach ($custom_meta_fields as $meta_field_name => $meta_disp_name) {
	$meta_number++;
	update_user_meta( $user_id, $meta_field_name, $_POST[$meta_field_name] );
	}
}
add_action('show_user_profile', 'mysite_show_extra_profile_fields');
add_action('edit_user_profile', 'mysite_show_extra_profile_fields');
add_action('personal_options_update', 'mysite_save_extra_profile_fields');
add_action('edit_user_profile_update', 'mysite_save_extra_profile_fields');

//Cat Page Attributes
function fg_cat_tax_funct(){
	$html = '';
	
	$category = get_queried_object();
	$category_id = $category->term_id;
	
	$prod_categories = array($category_id);
	$product_args = array(
		'numberposts' => -1,
		'post_status' => 'publish',
		'post_type' => array('product', 'product_variation'),
		'orderby' => 'ID',
		'suppress_filters' => false,
		'order' => 'ASC',
		'offset' => 0
	);
	
	if (!empty($prod_categories)) {
		$product_args['tax_query'] = array(
			array(
				'taxonomy' => 'product_cat',
				'field' => 'id',
				'terms' => $prod_categories,
				'operator' => 'IN',
		));
	}
	
	$products = get_posts($product_args);
	
	switch ($category_id) {
	  case 27: //Men 27
		$menu_id = 226;
		break;
	  case 31: //Women 31
		$menu_id = 225;
		break;
	  case 28: //Kids 28
		$menu_id = 227;
		break;
	  default:
		$menu_id = 226;
	}
	
	$menu_args = array(
		'menu' => $menu_id,
		'menu_class' => 'arch-menu',
		'container' => 'nav',
		'container_class' => 'arch-menu-cont'
	);
	return wp_nav_menu($menu_args);
}
add_shortcode('fg-cat-tax', 'fg_cat_tax_funct');

// Quick View Button
function action_woocommerce_shop_loop_item_title() {
	$id = get_the_ID(); 
	echo '<div class="fg-quick-icon"><a class="fg-pop-trig" href="#link-popup" data-prod-id="'.$id.'"><i class="far fa-eye">Quick View</i></a></div>'; 
}; 
//add_action( 'woocommerce_shop_loop_item_title', 'action_woocommerce_shop_loop_item_title', 10); 

//Move Variation Price
function move_variation_price() {
    remove_action( 'woocommerce_single_variation', 'woocommerce_single_variation', 10 );
}
add_action( 'woocommerce_before_add_to_cart_form', 'move_variation_price' );

//Remove Upsells
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 );

//Singel Product View Widget
function wpb_widgets_init() {
 
    register_sidebar( array(
        'name'          => 'Single Product Page',
        'id'            => 'single_product_page',
        'before_widget' => '<section class="fg_single_view_widget">',
        'after_widget'  => '</section>',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>',
    ) );
 
}
add_action( 'widgets_init', 'wpb_widgets_init' );

//Recently Viewed Products
function custom_woocommerce_after_single_product(){ 
	if ( is_active_sidebar( 'single_product_page' ) ){
		//echo '<div id="single-view-widgets" role="complementary">'.dynamic_sidebar( 'single_product_page' ).'</div>';
		if(isset($_COOKIE['woocommerce_recently_viewed'])){
			$prodIDS = str_replace('|',',',$_COOKIE['woocommerce_recently_viewed']);
			echo '<section class="fg-recently-viewed">';
			echo '<h2>Recently Viewed</h2>';
			echo do_shortcode('[products ids="'.$prodIDS.'"  limit="4" columns="4" ]');
			echo '</section>';
		}
	}
}
add_action('woocommerce_after_single_product', 'custom_woocommerce_after_single_product');

// Change WooCommerce "Related products" text
function change_rp_text($translated, $text, $domain)
{
     if ($text === 'Related products' && $domain === 'woocommerce') {
         $translated = esc_html__('You may also like', $domain);
     }
	 
	 if ($text === 'You may be interested in&hellip;' && $domain === 'woocommerce') {
         $translated = esc_html__('You might also like', $domain);
     }
	 
	 if ($text === 'You may also like&hellip;' && $domain === 'woocommerce') {
         $translated = esc_html__('You might also like', $domain);
     }
	 
	 if (($text === 'SKU' || $text === 'SKU:') && $domain === 'woocommerce' && !is_admin()) {
         $translated = esc_html__('', $domain);
     }
	 
	 if (($text === 'Billing details') && $domain === 'woocommerce' && !is_admin()) {
         $translated = esc_html__('Shipping', $domain);
     }
	 
	 if (($text === "It seems we can't find what you're looking for.") && $domain === 'woocommerce' && !is_admin()) {
         $translated = esc_html__("No matching products. Please try again or view all products <a href='/shop/'>here</a>.", $domain);
     }
	 
     return $translated;
}
add_filter('gettext', 'change_rp_text', 10, 3);
add_filter('ngettext', 'change_rp_text', 10, 3);

function lw_hide_sale_flash()
{
return false;
}
add_filter('woocommerce_sale_flash', 'lw_hide_sale_flash');

/*
 * show product weights
 */
function rs_show_weights()
{
	global $product;
	
	$variations = $product->get_available_variations();
	$colors = array();
	$check = array();
	
	$wcImageID = get_option('woocommerce_placeholder_image');
	$wcThumb = wp_get_attachment_image_src($wcImageID);
	$prodThumb = wp_get_attachment_image_src( get_post_thumbnail_id( $product->id ), 'single-post-thumbnail' );
	
	$colors_cnt = 0;
	$checked = '';
	$image_txt = '';
	
	foreach ( $variations as $variation ){
		$color = $variation['attributes']['attribute_pa_color'];
		$gallery = $variation['variation_gallery_images'];
		
		if($gallery){
			$img = $gallery[0]['gallery_thumbnail_src'];
		}
		
		if(!isset($img)){
			if(isset($prodThumb) && !empty($prodThumb)){
				$img = $prodThumb[0];
			}else{
				$img = $wcThumb[0];
			}
		}
		
		if(!in_array($color, $check)){
			$colors[] = array(
				'color' => $color,
				'img' => $img
			);
			$check[] = $color;
		}
	}
	
	if(!empty($colors)){
		foreach($colors as $color) {
			$image_txt .= '<input type="radio" id="'.$product->id.'-'.$color['color'].'" name="'.$product->id.'-fg-wl-color" value="'.$color['color'].'">
  <label for="'.$product->id.'-'.$color->term_id.'"><img class="fg-arch-thumb" data-arch-thumb="'.$color['color'].'" name="'.$color['color'].'" src="'.$color['img'].'" alt="'.$color['color'].'" /></label>';
		}
	}else if(isset($prodThumb) && !empty($prodThumb)){
		$image_txt .= '<input type="radio" id="'.$product->id.'-0" name="'.$product->id.'-fg-wl-color" value="0">
  <label for="'.$product->id.'-0"><img class="fg-arch-thumb" data-arch-thumb="0" name="" src="'.$prodThumb[0].'" alt="" /></label>';
	}else{
		$image_txt .= '<input type="radio" id="'.$product->id.'-0" name="'.$product->id.'-fg-wl-color" value="0">
  <label for="'.$product->id.'-0"><img class="fg-arch-thumb" data-arch-thumb="0" name="" src="'.$wcThumb[0].'" alt="" /></label>';
	}
	
	echo '<div class="fg-swatch">'.$image_txt.'</div>';
}
//add_action( 'woocommerce_after_shop_loop_item', 'rs_show_weights', 9 );

/*
 * show product weights
 */
function rs_show_wish()
{
	global $product;
	//fg-wcwl-add
	echo '
	<div class="fg-wcwl-add" data-id="'.$product->id.'">
		<span>Add to wishlist</span>
	</div>
	';
}
//add_action( 'woocommerce_after_shop_loop_item', 'rs_show_wish', 20 );

//Ajax Function to populate variations pop select
function fg_variation_pop_select(){
	
	$prodID = $selected = $_REQUEST['prodID'];
	$prodColor = $_REQUEST['prodColor'];
	$prodSize = $_REQUEST['prodSize'];
	
	$product = wc_get_product($prodID);
	$variations = $product->get_available_variations();
	
	$attSizeName = $attSlug = '';
	
	foreach ( $variations as $variation ){
		foreach($variation['attributes'] as $index => $value){
			$attSizeName = $index;
		}
	}
	
	$attSlug = str_replace('attribute_', '',$attSizeName);
	
	if (function_exists('fg_find_variation')) {
		if($prodColor != ''){
			$atts_vals['attribute_pa_color'] = $prodColor;
		}
		
		if($prodSize != ''){
			$atts_vals[$attSizeName] = $prodSize;
		}
		
		if(!empty($atts_vals)){
			$selected = fg_find_variation($prodID, $atts_vals);
		}
	}
	
	echo do_shortcode('[yith_wcwl_add_to_wishlist product_id='.(int)$selected.']');

	die();
}
add_action('wp_ajax_nopriv_fg_variation_pop_select', 'fg_variation_pop_select');
add_action('wp_ajax_fg_variation_pop_select', 'fg_variation_pop_select');

//Ajax Function to populate variations pop
function fg_variation_pop_funct(){
	
	$prodID = $selected = $_REQUEST['prodID'];
	$atts_vals = array();
	
	$product = wc_get_product($prodID);
	$variations = $product->get_available_variations();
	
	$sizes = array();
	$sizesTXT = '';
	$sizeDefault = '';
	$colors = array();
	$sizeNames = array();
	$colorsTXT = '';
	$colorDefault = '';
	$attSizeName = $attSlug = '';
	
	foreach($variations as $variation){
		
		foreach($variation['attributes'] as $index => $value){
			$attSizeName = $index;
		}
		
		$attSlug = str_replace('attribute_', '',$attSizeName);
		
		$term = get_term_by('slug', $variation['attributes'][$attSizeName], $attSlug);
		
		//echo '<pre>'.print_r($variation, true).'</pre>';
		$varID = $variation['variation_id'];
		if(isset($variation['attributes'][$attSizeName])){
			if(!in_array($variation['attributes'][$attSizeName], $sizes)){
				$sizes[] = array(
					'size' => $variation['attributes'][$attSizeName],
					'id' => $varID,
					'name' => $term->name
				);
			}
		}
		
		if(isset($variation['attributes']['attribute_pa_color'])){
			if(!in_array($variation['attributes']['attribute_pa_color'], $colors)){
				$colors[] = $variation['attributes']['attribute_pa_color'];
			}
		}
	}
	
	if(!empty($sizes)){
		foreach ($sizes as $key => $row)
		{
			$termSize = str_replace('uk-', '', $row['size']);
			$sizeNames[$key] = (float)$termSize;
		}
	}
	array_multisort($sizeNames, SORT_ASC, $sizes);
	
	$cnt = 0;
	$checked = '';
	if(!empty($sizes)){

		$sizesTXT .= '<div class="size-cont"><h4>Select a size to add to your Wishlist</h4>';
		foreach($sizes as $size){
			$in_wishlist = false;
			$class = ' class="';
			if ( function_exists( 'YITH_WCWL' ) ) {
				$in_wishlist = YITH_WCWL()->is_product_in_wishlist( $size['id'] );
			}
			if($in_wishlist){
				$class .= 'in-wishlist';
			}
			
			$class .= '"';
			
			if($cnt == 0){
				$sizeDefault = $size['size'];
			}
			$sizesTXT .= '<input type="radio" id="'.$prodID.'-'.$size['size'].'" name="'.$prodID.'-fg-wl-size" value="'.$size['size'].'"'.$checked.'><label for="'.$prodID.'-'.$size['size'].'"'.$class.'>'.$size['name'].'</label>';
			$cnt++;
			$checked = '';
		}
		$sizesTXT .= '</div>';
	}
	
	$cnt = 0;
	$checked = '';
	if(!empty($colors)){
		$colorsTXT .= '<div class="color-cont"><h4>Select a color to add to your Wishlist</h4>';
		foreach($colors as $color){
			if($cnt == 0){
				$colorDefault = $color;
			}
			$colorsTXT .= '<input type="radio" id="'.$prodID.'-'.$color.'" name="'.$prodID.'-fg-wl-color" value="'.$color.'"'.$checked.'>
	  <label for="'.$prodID.'-'.$color.'">'.$color.'</label>';
			$cnt++;
			$checked = '';
		}
		$colorsTXT .= '</div>';
	}
	
	
	if (function_exists('fg_find_variation')) {
		if($colorDefault != ''){
			$atts_vals['attribute_pa_color'] = $colorDefault;
		}
		
		if($sizeDefault != ''){
			$atts_vals[$attSizeName] = $sizeDefault;
		}
		
		if(!empty($atts_vals)){
			$selected = fg_find_variation($prodID, $atts_vals);
		}
	}
	
	echo '
	<div class="fg-quick-cont">
		'.$colorsTXT.'
		'.$sizesTXT.'
		<div class="fg-yith-wishlist" id="fg-yith-'.$prodID.'">
			<div class="my-wish"></div>
			<div class="my-wish-loading"><span>Updating Wishlist</span></div>
		</div>
	</div>';
	
	//echo '<pre>'.print_r($variations, true).'</pre>';

	die();
}
add_action('wp_ajax_nopriv_fg_variation_pop', 'fg_variation_pop_funct');
add_action('wp_ajax_fg_variation_pop', 'fg_variation_pop_funct');

function filter_woocommerce_short_description( $post_post_excerpt ) {
	if(is_product()){
		global $product;
	
		$post_post_excerpt .= '<p class="prod-sku">'.$product->get_sku().'</p>';
		return $post_post_excerpt; 
	}
}
add_filter( 'woocommerce_short_description', 'filter_woocommerce_short_description', 10, 1 ); 

function fg_find_in_store_link(){
	
	global $product;
	
	$review_count = $product->get_review_count();
	
	echo '<p class="find-in-store-link"><a href="/store-locator/"><i class="fas fa-map-marker-alt"></i>Find in Store</a></p>';
	
	echo '<button class="reviews-toggle">Additional Information</button><div class="panel">';
	echo $product->list_attributes();
	echo '</div>';
	
	echo '<button class="reviews-toggle">Free Delivery &amp; Returns</button><div class="panel">';
	//echo woocommerce_get_template( 'single-product/tabs/description.php' );
	echo get_option( 'fgdelivery_info' );
	echo '</div>';
	
	echo '<button class="reviews-toggle">Reviews ('.$review_count.')</button><div class="panel">';
	comments_template();
	echo '</div>';

}
add_action('woocommerce_share', 'fg_find_in_store_link', 40);
 
function fg_remove_product_tabs($tabs){
	unset($tabs['description']);
	//unset($tabs['additional_information']);
	//unset($tabs['reviews']);
	$tabs['delivery'] = array(
		'title' 	=> __( 'Free Delivery & Returns', 'woocommerce' ),
		'priority' 	=> 50,
		'callback' 	=> 'fg_returns_delivery_content'
	);
	return $tabs;
}
add_filter( 'woocommerce_product_tabs', 'fg_remove_product_tabs', 98 );

function fg_returns_delivery_content() {
	the_field('fg_delivery_returns');
}

//Cross Sells Edits
remove_action( 'woocommerce_cart_collaterals', 'woocommerce_cross_sell_display');
add_action( 'woocommerce_after_cart', 'woocommerce_cross_sell_display');

function fg_before_cart_table_funct(){
	echo '<h2>Shipping</h2>';
}
//add_action('woocommerce_before_cart_table', 'fg_before_cart_table_funct');

//Breadcrumbs on Checkout Process
function fg_before_cart_funct(){
	fgBreadcrumbs('cart');
}
add_action('woocommerce_before_cart', 'fg_before_cart_funct');

function fg_before_checkout_form_funct(){
	fgBreadcrumbs('checkout');
}
add_action('woocommerce_before_checkout_form', 'fg_before_checkout_form_funct', 9);

function fg_before_thankyou_funct(){
	fgBreadcrumbs('confirm');
}
add_action('woocommerce_before_thankyou', 'fg_before_thankyou_funct', 9);

function fgBreadcrumbs($fgPage){
	
	$list = '';
	switch ($fgPage) {
		case 'cart':
			$list = '<li class="current-phase"><span>Step 1: My Cart</span></li><li><span>Step 2: Shipping and Payment</span></li><li><span>Step 3: Order Complete</span></li>';
			break;
		case 'checkout':
			$list = '<li><span>Step 1: My Cart</span></li><li class="current-phase"><span>Step 2: Shipping and Payment</span></li><li><span>Step 3: Order Complete</span></li>';
			break;
		case 'confirm':
			$list = '<li><span>Step 1: My Cart</span></li><li><span>Step 2: Shipping and Payment</span></li><li class="current-phase"><span>Step 3: Order Complete</span></li>';
			break;	
		default:
			$list = '';
	}
	
	echo '
	<div id="phases">
		<ul>
			'.$list.'
		</ul>
	</div>
	<h1 class="fg-checkout-title">'.get_the_title().'</h1>';
}

function fg_change_cross_sells_columns( $columns ) {
	return 1;
}
add_filter( 'woocommerce_cross_sells_columns', 'fg_change_cross_sells_columns' );

function fg_change_cross_sells_product_no( $columns ) {
	return 10;
}
add_filter( 'woocommerce_cross_sells_total', 'fg_change_cross_sells_product_no' );

//Cart Checkout Buttons
function woocommerce_button_proceed_to_checkout() {
	$checkout_url = WC()->cart->get_checkout_url();
	if(is_user_logged_in()){
		?>
		<a href="<?php echo $checkout_url; ?>" class="checkout-button button alt wc-forward"><?php _e( 'Checkout', 'woocommerce' ); ?></a>
		<?php
	}else{
		?>
		<!-- <button class="fg-cart login" id="fg-cart-login">Login</button><br> -->
        <!-- <button class="fg-cart account" id="fg-cart-account">Create an Account</button> -->
        <button class="fg-cart my-checkout" id="fg-cart-checkout">Checkout</button>
		<?php	
	}
}

function woo_custom_order_button_text() {
    return __( 'Proceed To Checkout', 'woocommerce' ); 
}
add_filter( 'woocommerce_order_button_text', 'woo_custom_order_button_text' ); 
 
function ts_product_image_on_checkout( $name, $cart_item, $cart_item_key ) {

    if ( ! is_checkout() ) {
        return $name;
    }

    $_product = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );

    $thumbnail = $_product->get_image();

    $image = '<div class="ts-product-image">'
                . $thumbnail .
            '</div>'; 

    return $image . $name;
}
add_filter( 'woocommerce_cart_item_name', 'ts_product_image_on_checkout', 10, 3 );
  
function ts_product_image_on_order_pay( $name, $item, $extra ) {

    if ( ! is_checkout() ) {
        return $name;
    }
      
    $product_id = $item->get_product_id();

    $_product = wc_get_product( $product_id );

    $thumbnail = $_product->get_image();

    $image = '<div class="ts-product-image">'
                . $thumbnail .
            '</div>'; 

    return $image . $name;
}
add_filter( 'woocommerce_order_item_name', 'ts_product_image_on_order_pay', 10, 3 );

//Search Icon
function my_yith_wcas_submit_label( $label ) { 
    return '<i class="fa fa-search"></i>' . $label; 
}
add_filter( 'yith_wcas_submit_as_input', '__return_false' );
add_filter( 'yith_wcas_submit_label', 'my_yith_wcas_submit_label' );

function fg_bredcrumbs_single(){
	echo do_shortcode('[astra_breadcrumb]');
}
add_action('woocommerce_before_single_product', 'fg_bredcrumbs_single');

//Main Menu Sub Links
function gender_menu_funct($atts){
	$options = shortcode_atts( array(
		'gender' => '',
	), $atts );
	
	$gender = $options['gender'];
	
	$title = explode('-', $gender);
	$titleCnt = count($title);
	
	switch ($title[$titleCnt-1]) {
	  case 'm':
		$title = 'men';
		break;
	  case 'k':
		$title = 'kids';
		break;
	  case 'w':
		$title = 'women';
		break;
	}
	
	if(term_exists( ucfirst($gender), 'product_cat' )){
		$html .= '
		<div class="genger-menu">
		<ul>
		<li><a href="/'.$gender.'/?product-category=promo">Deals</a></li>
		<li><a href="/'.$gender.'/footwear-'.$title.'/">Shoes</a></li>
		<li><a href="/'.$gender.'/?orderby=date&badgefilter=new">Just Released</a></li>
		<li><a href="/'.$gender.'/?orderby=popularity">Best Sellers</a></li>
		</ul>
		</div>
		';
	}
	
	return $html;
}
add_shortcode('gender-menu', 'gender_menu_funct');

//add_filter( 'woocommerce_dropdown_variation_attribute_options_html', 'override_color_variation_display');

function override_color_variation_display( $html ) {
    $html = 'Some override';

    return $html;
}

add_filter( 'woocommerce_dropdown_variation_attribute_options_html', 'show_stock_status_in_dropdown', 9999, 2);
function show_stock_status_in_dropdown( $html, $args ) {
    $options = $args['options']; 
    $product = $args['product']; 
    $attribute = $args['attribute']; 
    $name = $args['name'] ? $args['name'] : 'attribute_' . sanitize_title( $attribute ); 
    $id = $args['id'] ? $args['id'] : sanitize_title( $attribute ); 
    $class = $args['class']; 
    $show_option_none = $args['show_option_none'] ? true : false; 
    $show_option_none_text = $args['show_option_none'] ? $args['show_option_none'] : __( 'Choose an option', 'woocommerce' );  

  // Get all product variations
    $variations = $product->get_available_variations();

    if ( empty( $options ) && ! empty( $product ) && ! empty( $attribute ) ) { 
        $attributes = $product->get_variation_attributes(); 
        $options = $attributes[ $attribute ]; 
    } 

    $html = '<select id="' . esc_attr( $id ) . '" class="' . esc_attr( $class ) . '" name="' . esc_attr( $name ) . '" data-attribute_name="attribute_' . esc_attr( sanitize_title( $attribute ) ) . '" data-show_option_none="' . ( $show_option_none ? 'yes' : 'no' ) . '" data-custom="footgear">'; 
    $html .= '<option value="">' . esc_html( $show_option_none_text ) . '</option>'; 

    if ( ! empty( $options ) ) { 
        if ( $product && taxonomy_exists( $attribute ) ) {
          // Get terms if this is a taxonomy - ordered. We need the names too. 
          $terms = wc_get_product_terms( $product->get_id(), $attribute, array( 'fields' => 'all' ) );
		  
		  $selected = ''; 

          foreach ( $terms as $term ) { 
                if ( in_array( $term->slug, $options ) ) {
					
					foreach ($variations as $variation) {
                        if($variation['attributes'][$name] == esc_attr( $term->slug )) {
                            $stock = $variation['availability_html'];
							
							if(preg_match("~\b0 in stock\b~",$stock)){
								$count = ' data-in-stock="0"';
							}else if(preg_match("~\bOut of stock\b~",$stock)){
								$count = ' data-in-stock="0"';
							}else{
								if($selected != 'selected'){
									$selected = 'selected';
									$count = ' data-in-stock="1" '.$selected;
								}else{
									$count = ' data-in-stock="1"';
								}
							}
							
                        }
                    }
					
					//$html .= '<pre>'.print_r($stock, true).'</pre>';
					 
                    $html .= '<option value="' . esc_attr( $term->slug ) . '" ' . $count . '>' . esc_html( apply_filters( 'woocommerce_variation_option_name', $term->name ) ) . '</option>'; 
					
                } 
            }
        } else {
            foreach ( $options as $option ) {
                    foreach ($variations as $variation) {
                        if($variation['attributes'][$name] == $option) {
                            $stock = $variation['is_in_stock'];
                        }
                    }       
                if( $stock == 1) {
                    $stock_content = ' - (In Stock)';
                } else {
                    $stock_content = ' - (Out of Stock)';
                }
                 // This handles < 2.4.0 bw compatibility where text attributes were not sanitized. 
                $selected = sanitize_title( $args['selected'] ) === $args['selected'] ? selected( $args['selected'], sanitize_title( $option ), false ) : selected( $args['selected'], $option, false ); 

                $html .= '<option value="' . esc_attr( $option ) . '" ' . $selected . '>' . esc_html( $option  .  $stock_content ) . '</option>'; 

            }
        } 
    } 

    $html .= '</select>'; 

    return $html;
}

function bk_title_order_received( $title, $id ) {
	if ( is_order_received_page() && get_the_ID() === $id ) {
		$title = "Order Complete";
	}

	return $title;
}
add_filter( 'the_title', 'bk_title_order_received', 10, 2 );

/*Change Quick View LAbel to Icon*/
function astra_woo_add_quick_view_text_html_func( $button, $label, $product_id ) {

    global $product;
    $product_id = $product->get_id();

    $label = __( '<i class="far fa-eye"></i>', 'astra-addon' );
    $button = '
	<a href="#" class="ast-quick-view-text" data-product_id="' . esc_attr( $product_id ) . '">' . $label . '</a>
	<div class="fg-wcwl-add" data-id="'.esc_attr( $product_id ).'">
		<span><i class="far fa-heart"></i></span>
	</div>
	';
    
    return $button;
}
add_filter( 'astra_woo_add_quick_view_text_html', 'astra_woo_add_quick_view_text_html_func', 10, 3 );

//Remove tabs from Single Product Page in woocommerce
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );

//Free Delivery and Returns
function fg_wc_settings( $settings, $current_section ) {
	if ( $current_section == '' ) {
		$settings_slider = array();
		// Add Title to the Settings
		$settings[] = array( 'name' => __( 'Free Delivery &amp; Returns', 'text-domain' ), 'type' => 'title', 'desc' => __( 'Global settings for free delivery and returns.', 'text-domain' ), 'id' => 'fgsettings' );
		// Add second text field option
		$settings[] = array(
			'name'     => __( 'Tab Info', 'text-domain' ),
			'desc_tip' => __( 'Information that will go inside the product tab [Free Delivery &amp; Returns]', 'text-domain' ),
			'id'       => 'fgdelivery_info',
			'type'     => 'textarea'
		);
		
		$settings[] = array( 'type' => 'sectionend', 'id' => 'fgsettings' );
		return $settings;
		
	} else {
		return $settings;
	}
}
add_filter( 'woocommerce_get_settings_products', 'fg_wc_settings', 10, 2 );

//Main Menu Sub Links
function fg_archive_title_funct(){
	
	$title = 'Shop';

	if(is_product_category()){
		global $wp_query;
		$slug = explode('-', $wp_query->get_queried_object()->slug);
		$slugPart = array_reverse($slug);
		$title = '';
		$cnt = 1;
		$slugPartCnt = count($slugPart);
		
		$parent = $wp_query->get_queried_object()->parent;
		
		if(isset($parent) && $parent != '' && $slugPartCnt == 1){
			$parentTerm = get_term_by( 'id', $parent, 'product_cat' );
			$slugParent = explode('-', $parentTerm->slug);
			foreach($slugParent as $piece){
				$slugPart[] = $piece;
			}
			$slugPart = array_reverse($slugPart);
		}
		
		$slugPartCnt = count($slugPart);
		
		foreach ($slugPart as $part){
			$sep = '';
			if($slugPartCnt == ($cnt + 1) && $slugPartCnt != 1){
				$sep = ':';
			}
			$title .= $part.$sep.' ';
			$cnt++;
		}
		
		$title = rtrim($title, " ");
		
		switch ($title) {
		  case 'm':
			$title = 'MEN';
			break;
		  case 'k':
			$title = 'KIDS';
			break;
		  case 'w':
			$title = 'WOMEN';
			break;
		  default:
			$title = $title;
		}
	}
	
	return '<header class="woocommerce-products-header"><h1 class="woocommerce-products-header__title page-title">'.ucwords($title).'</h1></header>';

}
add_shortcode('fg-archive-title', 'fg_archive_title_funct');

/**
 * Change number or products per row to 3
 */
add_filter('loop_shop_columns', 'loop_columns', 999);
if (!function_exists('loop_columns')) {
	function loop_columns() {
		return 3; // 3 products per row
	}
}

add_action('woocommerce_before_shop_loop', 'fg_filter_btn', 21);
function fg_filter_btn(){
	echo '<button class="fg-archive-filter-btn"><i class="fas fa-sliders-h"></i></button>';
}

add_action( 'wp', 'redirect' );
function redirect() {
  if ( is_page('my-account') && !is_user_logged_in() ) {
	$mystring = $_SERVER['REQUEST_URI'];
	$findme   = '/my-account/lost-password/';
	$pos = strpos($mystring, $findme);
	
	if($pos === false){
	  wp_redirect( home_url('/login') );
	  die();
	}
  }
}

function action_woocommerce_checkout_update_order_review($array, $int)
{
    WC()->cart->calculate_shipping();
    return;
}
//add_action('woocommerce_checkout_update_order_review', 'action_woocommerce_checkout_update_order_review', 10, 2);




/* ~~~~~~~~~~~~~~~~~~~~~~~~~ */
/* Switch IMG urls on Local to use Live url */
/* ~~~~~~~~~~~~~~~~~~~~~~~~~ */
if($_SERVER['REMOTE_ADDR'] === '127.0.0.1')
{
    // Replace src paths
    add_filter('wp_get_attachment_url', function ($url)
    {
        if(file_exists($url))
        {
            return $url;
        }
        return str_replace(WP_HOME, 'https://www.footgear.co.za', $url);
    });

    // Replace srcset paths
    add_filter('wp_calculate_image_srcset', function($sources)
    {
        foreach($sources as &$source)
        {
            if(!file_exists($source['url']))
            {
                $source['url'] = str_replace(WP_HOME, 'https://www.footgear.co.za', $source['url']);
            }
        }
        return $sources;
    });
}



/* ~~~~~~~~~~~~~~~~~~~~~~~~~ */
/* Pre Get Posts - New Badge Filter */
/* ~~~~~~~~~~~~~~~~~~~~~~~~~ */
function fg_pre_get_posts_badge( $query ) {

    // do not modify queries in the admin
    if( is_admin() ) {
        return $query;
    }


    if( $query->is_main_query() && isset($_GET['badgefilter']) && $_GET['badgefilter'] == 'new' ) {

//        $query->set( 'meta_key', '_yith_wcbm_product_meta' );
//        $query->set( 'meta_query', array(
//            array(
//                'key' => '_yith_wcbm_product_meta',
//                'value' => '1194',
//                'compare' => 'LIKE',
//            )
//        ));

        $query->set('date_query', array(
            array(
                'after' => date('Y-m-d', strtotime('-21 days')),
                'inclusive' => true
            ),
        ));

    }


    // return
    return $query;
}
add_action('pre_get_posts', 'fg_pre_get_posts_badge', 99);

//Free Shipping Notice Function
function freeShippingNotice($cart_amount, $rate_label){
	 
	$totalOutstanding = round(650 - $cart_amount,2);
	
	if ($totalOutstanding > 0) {
		echo'
		<div class="cart-shipping-discount-wrapper">
			<div class="save-box-wrapper ">
				<div class="arrow-up"></div>
				<div class="save-box">
					<div class="discount-description">
						<div class="free-shipping">
							<div>Want Free Shipping? Spend "R'.$totalOutstanding.'" more</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		';
	}
}


/* ~~~~~~~~~~~~~~~~~~~~~~~~~ */
/* Script to change date format */
/* ~~~~~~~~~~~~~~~~~~~~~~~~~ */
add_action('wp_footer', function () {
	?>
   <script>
   jQuery( document ).ready( function( $ ){
		 setTimeout( function(){
			   $('.elementor-date-field').each(function(){ flatpickr( $(this)[0] ).set('dateFormat', 'm/d');});
		 }, 1000 );
   });
   </script>
   <?php
});

//Remove Comments for Store Locator
function my_prefix_comments_open( $open, $post_id ) {
    $post_type = get_post_type( $post_id );
    if ( $post_type == 'wpsl_stores' ) {
        return false;
    }
    return true;
}
add_filter( 'comments_open', 'my_prefix_comments_open', 10 , 2 );