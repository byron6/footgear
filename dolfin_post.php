<?php

$curl = curl_init();
      
curl_setopt_array($curl, array(
  CURLOPT_URL => 'https://ftgsemuatlab.mychain.co.za:8888/DolfinInterface.Webservices.FTG/DolfinMessagingInterface.svc',
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => '',
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => 'POST',
  CURLOPT_POSTFIELDS =>'<?xml version="1.0" encoding="UTF-8"?>
<SOAP-ENV:Envelope xmlns:tem="http://tempuri.org/" xmlns:SOAP-ENV="http://www.w3.org/2003/05/soap-envelope" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <SOAP-ENV:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">
    <wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">
      <wsse:UsernameToken>
        <wsse:Username>SEMANTICA</wsse:Username>
        <wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">k4tVwN7R34s8XMmx</wsse:Password>
      </wsse:UsernameToken>
    </wsse:Security>
    <wsa:Action>http://tempuri.org/IDolfinMessagingInterface/ActionDolfinTransaction</wsa:Action>
    <wsa:To>https://ftgsemuatlab.mychain.co.za:8888/DolfinInterface.Webservices.FTG/DolfinMessagingInterface.svc</wsa:To>
  </SOAP-ENV:Header>
<SOAP-ENV:Body>
<tem:ActionDolfinTransaction>
<!--Optional:-->
<tem:companyId>FTG</tem:companyId>
<!--Optional:-->
<tem:source>DolfinTranSourceWS</tem:source>
<!--Optional:-->
<tem:xmlDoc>
<![CDATA[<CashSale>
<Hdr>
<TranHdr>
<StoreNumber>9997</StoreNumber>
<Terminal>99</Terminal>
<TransactionNumber>0002</TransactionNumber>
<UserName>Web Sales</UserName>
<TranDate>2021-07-05</TranDate>
<TranTime>14:10:07</TranTime>
</TranHdr>
<TransactionType>CS</TransactionType>
<CashierID>Web Sales</CashierID>
<CSCustomerCellPhone>0632305889</CSCustomerCellPhone>
<CSCustomerEmail>marketingassist@footgear.co.za</CSCustomerEmail>
<CSCustomerName>Shawna</CSCustomerName>
<CSCustomerSurname>Langeveld</CSCustomerSurname>
</Hdr>
<Lines>
<SaleLineData>
<LineNumber>46374</LineNumber>
<SKUCode>VNML0021201080</SKUCode>
<SaleQty>1</SaleQty>
<SaleLineDirection>Sale</SaleLineDirection>
<TransactionSubType>NLI</TransactionSubType>
<NetAmtStoreInc>499.9500</NetAmtStoreInc>
<TaxAmtStore>65.210869565217</TaxAmtStore>
<DiscAmtStoreInc>0.0000</DiscAmtStoreInc>
<DiscountType>NA</DiscountType>
<DiscountReasonCode />
<ConditionalPromotionID>0</ConditionalPromotionID>
<UnitPriceStoreInc>799.9500</UnitPriceStoreInc>
<OverridePrice>799.9500</OverridePrice>
<DeliveryDetails>
<DeliveryType>Courier</DeliveryType>
<DeliveryStoreNumber />
<ExpectedDeliveryDate>2020-07-02</ExpectedDeliveryDate>
<ContactName>Shawna Langeveld</ContactName>
<ContactTelephone>0632305889</ContactTelephone>
<DeliveryContactName>Shawna Langeveld</DeliveryContactName>
<DeliveryContactTelephone>0632305889</DeliveryContactTelephone>
<DeliveryAddress1>39 Carl Cronje Drive</DeliveryAddress1>
<DeliveryAddress2>Tygervalley</DeliveryAddress2>
<DeliveryCity>Cape Town</DeliveryCity>
<DeliveryCountry>ZA</DeliveryCountry>
<DeliveryPostalCode>7530</DeliveryPostalCode>
<DeliveryNotes />
<DeliveryEmailAddress>39 Carl Cronje Drive Tygervalley</DeliveryEmailAddress>
<DeliveryCourier>Dawnwing</DeliveryCourier>
<DeliveryOption>8</DeliveryOption>
<DeliveryCounty>ZA</DeliveryCounty>
<DeliveryFirstName>Shawna</DeliveryFirstName>
<DeliverySurname>Langeveld</DeliverySurname>
<DeliveryTitle>NA</DeliveryTitle>
</DeliveryDetails>
</SaleLineData>
<SaleLineData>
<LineNumber>46379</LineNumber>
<SKUCode>VNML0021201070</SKUCode>
<SaleQty>1</SaleQty>
<SaleLineDirection>Sale</SaleLineDirection>
<TransactionSubType>NLI</TransactionSubType>
<NetAmtStoreInc>499.9500</NetAmtStoreInc>
<TaxAmtStore>65.210869565217</TaxAmtStore>
<DiscAmtStoreInc>0.0000</DiscAmtStoreInc>
<DiscountType>NA</DiscountType>
<DiscountReasonCode />
<ConditionalPromotionID>0</ConditionalPromotionID>
<UnitPriceStoreInc>799.9500</UnitPriceStoreInc>
<OverridePrice>799.9500</OverridePrice>
<DeliveryDetails>
<DeliveryType>Courier</DeliveryType>
<DeliveryStoreNumber />
<ExpectedDeliveryDate>2020-07-02</ExpectedDeliveryDate>
<ContactName>Shawna Langeveld</ContactName>
<ContactTelephone>0632305889</ContactTelephone>
<DeliveryContactName>Shawna Langeveld</DeliveryContactName>
<DeliveryContactTelephone>0632305889</DeliveryContactTelephone>
<DeliveryAddress1>39 Carl Cronje Drive</DeliveryAddress1>
<DeliveryAddress2>Tygervalley</DeliveryAddress2>
<DeliveryCity>Cape Town</DeliveryCity>
<DeliveryCountry>ZA</DeliveryCountry>
<DeliveryPostalCode>7530</DeliveryPostalCode>
<DeliveryNotes />
<DeliveryEmailAddress>39 Carl Cronje Drive Tygervalley</DeliveryEmailAddress>
<DeliveryCourier>Dawnwing</DeliveryCourier>
<DeliveryOption>8</DeliveryOption>
<DeliveryCounty>ZA</DeliveryCounty>
<DeliveryFirstName>Shawna</DeliveryFirstName>
<DeliverySurname>Langeveld</DeliverySurname>
<DeliveryTitle>NA</DeliveryTitle>
</DeliveryDetails>
</SaleLineData>
</Lines>
<Tenders>
<TenderData>
<TenderTypeNo>CreditCard</TenderTypeNo>
<TenderSubType>MAN</TenderSubType>
<Reference />
<TenderDirection>Tender</TenderDirection>
<TenderCurrency>RANDS</TenderCurrency>
<TenderAmountCurr>999.9</TenderAmountCurr>
<TenderAmountStore>999.9</TenderAmountStore>
</TenderData>
</Tenders>
<Totals>
<TranDetailTotal>
<LinesCount>2</LinesCount>
<LinesQty>2</LinesQty>
<LinesValueInc>999.9</LinesValueInc>
</TranDetailTotal>
</Totals>
</CashSale>
]]> </tem:xmlDoc>
</tem:ActionDolfinTransaction>
 </SOAP-ENV:Body>
</SOAP-ENV:Envelope>
',
  CURLOPT_HTTPHEADER => array(
    'Content-Type: application/soap+xml; charset=utf-8'
  ),
));

$response = curl_exec($curl);

curl_close($curl);
echo $response;
