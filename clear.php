<?php 
// Load WordPress.
require( 'wp-load.php' );

// Clear cache.
if ( function_exists( 'rocket_clean_domain' ) ) {
	rocket_clean_domain();
}

// Preload cache.
if ( function_exists( 'run_rocket_sitemap_preload' ) ) {
	run_rocket_sitemap_preload();
}